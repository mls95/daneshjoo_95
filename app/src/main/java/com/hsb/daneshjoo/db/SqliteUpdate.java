package com.hsb.daneshjoo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by hsb-pc on 1/19/2017.
 */
public class SqliteUpdate {

    DataBaseHelper dataBaseHelper;
    public SqliteUpdate(Context context){
        dataBaseHelper=new DataBaseHelper(context);
    }


    public void Update_term(int id,int term,int yeardate,int sumunit,int datestart,int dateend){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();


        cv.put(SqliteInstraction.Term_term,term);
        cv.put(SqliteInstraction.Term_year_date,yeardate);
        cv.put(SqliteInstraction.Term_sum_unit,sumunit);
        cv.put(SqliteInstraction.Term_date_start,datestart);
        cv.put(SqliteInstraction.Term_date_end,dateend);


        db.update(SqliteInstraction.TABLE_Term,cv,"id_term="+id,null);
        db.close();
    }

    public void Update_lesson(int id,String namelesson,int countunit,String nameteacher,int dayclass,String clockclass,int numberclass,int dateexam){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();


        cv.put(SqliteInstraction.Lesson_name_lesson,namelesson);
        cv.put(SqliteInstraction.Lesson_name_teacher,nameteacher);
        cv.put(SqliteInstraction.Lesson_count_unit,countunit);
        cv.put(SqliteInstraction.Lesson_day_class,dayclass);
        cv.put(SqliteInstraction.Lesson_clock_class,clockclass);
        cv.put(SqliteInstraction.Lesson_number_class,numberclass);
        cv.put(SqliteInstraction.Lesson_date_exam,dateexam);

        db.update(SqliteInstraction.TABLE_Lesson,cv,"id_lesson="+id,null);

        db.close();
    }


    public boolean Update_student(int idUser,String name,String family,String field,int level,int enteringYear,int age,int sex,String univercity){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();


        cv.put(SqliteInstraction.Student_name,name);
        cv.put(SqliteInstraction.Student_family,family);
        cv.put(SqliteInstraction.Student_field,field);
        cv.put(SqliteInstraction.Student_level,level);
        cv.put(SqliteInstraction.Student_entering_year,enteringYear);
        cv.put(SqliteInstraction.Student_age,age);
        cv.put(SqliteInstraction.Student_sex,sex);
        cv.put(SqliteInstraction.Student_univercity,univercity);

        db.update(SqliteInstraction.TABLE_Student,cv,"id_user="+idUser,null);

        db.close();

        return true;
    }

    public void Update_lessonreportcard(int id_lesson_report_card,float grade){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Lesson_Report_Card_grade,grade);

        db.update(SqliteInstraction.TABLE_Lesson_Report_Card,cv,"id_lesson_report_card="+id_lesson_report_card,null);

        db.close();
    }

    public void Update_reminder(int id,int idLesson,String titer,String note,int date,String clock){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();


        cv.put(SqliteInstraction.Reminder_id_lesson,idLesson);
        cv.put(SqliteInstraction.Reminder_titer,titer);
        cv.put(SqliteInstraction.Reminder_note,note);
        cv.put(SqliteInstraction.Reminder_date_reminder,date);

        cv.put(SqliteInstraction.Reminder_clock_reminder,clock);

        db.update(SqliteInstraction.TABLE_NoteReminder,cv,"id_reminder="+id,null);

        db.close();
    }

    public void Update_reminder(int id,int flag){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Reminder_id_reminder,id);
        cv.put(SqliteInstraction.Reminder_flag,flag);
        db.update(SqliteInstraction.TABLE_NoteReminder,cv,"id_reminder="+id,null);

        db.close();
    }

    public void Update_note(int id, int idLesson, String titer, String note) {
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();


        cv.put(SqliteInstraction.Note_id_lesson,idLesson);
        cv.put(SqliteInstraction.Note_titer,titer);
        cv.put(SqliteInstraction.Note_note,note);

        db.update(SqliteInstraction.TABLE_Note,cv,"id_note="+id,null);

        db.close();
    }

}
