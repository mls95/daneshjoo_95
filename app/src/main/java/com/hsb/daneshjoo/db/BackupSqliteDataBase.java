package com.hsb.daneshjoo.db;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

/**
 * Created by hsb-pc on 3/12/2017.
 */
public class BackupSqliteDataBase {
    String inFileName="/data/data/com.hsb.daneshjoo/databases/daneshjoo.db";
    boolean success =true;
    File file =null;

    public void Backup()  {
      /* file=new File(Environment.getExternalStorageDirectory()+"/aaaaa/daneshjoo2.db");

        if (file.exists()){
            success=true;
        }else{

            file.getParentFile().mkdirs();

        }

        if(success){
            Log.i("success",file.getPath());
            File dbFile=new File(inFileName);
            FileInputStream fileInputStream=new FileInputStream(dbFile);
            String outFileName=Environment.getExternalStorageDirectory()+"/aaaaa/daneshjoo2.db";
            OutputStream outputStream=new FileOutputStream(outFileName);


            byte[] buffer=new byte[1024];
            int length;
            while ((length=fileInputStream.read(buffer))>0){
                outputStream.write(buffer,0,length);
            }

            outputStream.flush();
            outputStream.close();
        }
*/

        try {

            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                Log.i("if", "sd can write");
                String currentDBPath = inFileName;
                String backupDBPath = "daneshjoo.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(Environment.getExternalStorageDirectory() + "/aaaaa/daneshjoo2.db");

                if (currentDB.exists()) {
                    Log.i("if", "current db exists");
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());

                    src.close();
                    dst.close();
                }
            }
        }catch (IOException ex){
            Log.i("IOException",ex.toString());
        }

    }

    }
