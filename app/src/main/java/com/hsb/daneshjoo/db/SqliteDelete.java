package com.hsb.daneshjoo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by hsb-pc on 1/19/2017.
 */
public class SqliteDelete {
    DataBaseHelper dataBaseHelper;

    public SqliteDelete(Context context) {
        this.dataBaseHelper=new DataBaseHelper(context);
    }


    public void Del_term(int idTerm){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Term +" where term='"+idTerm+"'";
        db.execSQL(sql);
        db.close();
    }

    public void Del_term_report_card(int idTerm){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Term_Report_Card +" where "+SqliteInstraction.Term_Report_Card_term+"='"+idTerm+"'";
        db.execSQL(sql);
        db.close();
    }

    public void Del_lesson_idTerm(int idTerm) {

        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Lesson +" where id_term='"+idTerm+"'";
        db.execSQL(sql);
        db.close();
    }

    public void Del_lesson(int idLesson) {

        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Lesson +" where id_lesson='"+idLesson+"'";
        db.execSQL(sql);
        db.close();
    }


    public void Del_lesson_report_card_id_term(int idTerm) {

        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Lesson_Report_Card +" where "+SqliteInstraction.Lesson_Report_Card_id_term+"='"+idTerm+"'";
        db.execSQL(sql);
        db.close();
    }


    public void Del_lesson_report_card(int idLesson) {

        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Lesson_Report_Card +" where "+SqliteInstraction.Lesson_Report_Card_id_lesson+"='"+idLesson+"'";
        db.execSQL(sql);
        db.close();
    }

    public void Del_reminder(int idReminder) {
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_NoteReminder +" where id_reminder='"+idReminder+"'";
        db.execSQL(sql);
        db.close();
    }

    public void Del_note(int idNote) {
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_Note +" where id_note='"+idNote+"'";
        db.execSQL(sql);
        db.close();
    }

    public void Del_appVersion() {
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        String sql="DELETE FROM "+SqliteInstraction.TABLE_App_Version ;
        db.execSQL(sql);
        db.close();
    }

    public void Del_AllData(){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();

        String sql_del_account="DELETE  FROM "+SqliteInstraction.TABLE_Account;
        String sql_del_profile="DELETE  FROM "+SqliteInstraction.TABLE_Student;
        String sql_del_term="DELETE  FROM "+SqliteInstraction.TABLE_Term;
        String sql_del_lesson="DELETE  FROM "+SqliteInstraction.TABLE_Lesson;
        String sql_del_reminder="DELETE  FROM "+SqliteInstraction.TABLE_NoteReminder;
        String sql_del_note="DELETE  FROM "+SqliteInstraction.TABLE_Note;
        String sql_del_lessonreport="DELETE  FROM "+SqliteInstraction.TABLE_Lesson_Report_Card;
        String sql_del_termreport="DELETE  FROM "+SqliteInstraction.TABLE_Term_Report_Card;
        String sql_del_app_version="DELETE  FROM "+SqliteInstraction.TABLE_App_Version;


        db.execSQL(sql_del_account);
        db.execSQL(sql_del_profile);
        db.execSQL(sql_del_term);
        db.execSQL(sql_del_lesson);
        db.execSQL(sql_del_reminder);
        db.execSQL(sql_del_note);
        db.execSQL(sql_del_lessonreport);
        db.execSQL(sql_del_termreport);
        db.execSQL(sql_del_app_version);

        db.close();


    }
}
