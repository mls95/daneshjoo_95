package com.hsb.daneshjoo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.UUID;

/**
 * Created by hsb-pc on 1/19/2017.
 */
public class SqliteInsert {

    DataBaseHelper dataBaseHelper;

    // Initialize class DataBaseHelper
    public SqliteInsert(Context context){
        dataBaseHelper=new DataBaseHelper(context);
    }

    public Boolean ADD_account(String username,String email,String password,Integer id_user){


        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Account_username,username);
        cv.put(SqliteInstraction.Account_email,email);
        cv.put(SqliteInstraction.Account_password,password);
        cv.put(SqliteInstraction.Account_id_user,id_user);

        db.insert(SqliteInstraction.TABLE_Account,null,cv);
        db.close();

        return true;
    }

    public void ADD_term(int idUser,int id_term,int term,int yeardate,int sumunit,int datestart,int dateend){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Term_id_user,idUser);
        cv.put(SqliteInstraction.Term_id_term,id_term);
        cv.put(SqliteInstraction.Term_term,term);
        cv.put(SqliteInstraction.Term_year_date,yeardate);
        cv.put(SqliteInstraction.Term_sum_unit,sumunit);
        cv.put(SqliteInstraction.Term_date_start,datestart);
        cv.put(SqliteInstraction.Term_date_end,dateend);


        db.insertWithOnConflict(SqliteInstraction.TABLE_Term,null,cv,SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public void ADD_lesson(int id_user,int id_term,String namelesson,Integer id_lesson,int countunit,String nameteacher,int dayclass,String clockclass,int numberclass,int dateexam){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();




        cv.put(SqliteInstraction.Lesson_id_user,id_user);
        cv.put(SqliteInstraction.Lesson_id_term,id_term);
        cv.put(SqliteInstraction.Lesson_id_lesson,id_lesson);
        cv.put(SqliteInstraction.Lesson_name_lesson,namelesson);
        cv.put(SqliteInstraction.Lesson_name_teacher,nameteacher);
        cv.put(SqliteInstraction.Lesson_count_unit,countunit);
        cv.put(SqliteInstraction.Lesson_day_class,dayclass);
        cv.put(SqliteInstraction.Lesson_clock_class,clockclass);
        cv.put(SqliteInstraction.Lesson_number_class,numberclass);
        cv.put(SqliteInstraction.Lesson_date_exam,dateexam);

        db.insertWithOnConflict(SqliteInstraction.TABLE_Lesson,null,cv,SQLiteDatabase.CONFLICT_REPLACE);

        db.close();
    }

    public boolean ADD_student(int idUser,String name,String family,String field,int level,int enteringYear,int age,int sex,String univercity){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Student_id_user,idUser);
        cv.put(SqliteInstraction.Student_name,name);
        cv.put(SqliteInstraction.Student_family,family);
        cv.put(SqliteInstraction.Student_field,field);
        cv.put(SqliteInstraction.Student_level,level);
        cv.put(SqliteInstraction.Student_entering_year,enteringYear);
        cv.put(SqliteInstraction.Student_age,age);
        cv.put(SqliteInstraction.Student_sex,sex);
        cv.put(SqliteInstraction.Student_univercity,univercity);

        db.insertWithOnConflict(SqliteInstraction.TABLE_Student,null,cv,SQLiteDatabase.CONFLICT_REPLACE);

        db.close();

        return true;
    }

    public void ADD_noteReminder(int idUser,int idLesson,int id_reminder,int flag,String titer,String note,int date,String clock,int idterm){
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Reminder_id_user,idUser);
        cv.put(SqliteInstraction.Reminder_id_lesson,idLesson);
        cv.put(SqliteInstraction.Reminder_id_reminder,id_reminder);
        cv.put(SqliteInstraction.Reminder_titer,titer);
        cv.put(SqliteInstraction.Reminder_note,note);
        cv.put(SqliteInstraction.Reminder_date_reminder,date);
        cv.put(SqliteInstraction.Reminder_flag,flag);
        cv.put(SqliteInstraction.Reminder_clock_reminder,clock);
        cv.put(SqliteInstraction.Reminder_id_term,idterm);


        db.insertWithOnConflict(SqliteInstraction.TABLE_NoteReminder,null,cv,SQLiteDatabase.CONFLICT_FAIL);

        db.close();
    }

    public void ADD_note(int idUser, int idLesson,int id_note, String titer, String note,int idterm) {
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Note_id_user,idUser);
        cv.put(SqliteInstraction.Note_id_lesson,idLesson);
        cv.put(SqliteInstraction.Note_id_note,id_note);
        cv.put(SqliteInstraction.Note_titer,titer);
        cv.put(SqliteInstraction.Note_note,note);
        cv.put(SqliteInstraction.Note_id_term,idterm);

        db.insertWithOnConflict(SqliteInstraction.TABLE_Note,null,cv,SQLiteDatabase.CONFLICT_ABORT);

        db.close();
    }

    public void ADD_app_version(Integer codeVersion,String dateRelease) {
        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.App_Version_Code_version,codeVersion);
        cv.put(SqliteInstraction.App_Version_date_release,dateRelease);

        db.insertWithOnConflict(SqliteInstraction.TABLE_App_Version,null,cv,SQLiteDatabase.CONFLICT_ABORT);

        db.close();
    }


    public void ADD_term_report_card(int id_user,int id_term_report_card,int id_term,int term){


        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Term_Report_Card_id_user,id_user);
        cv.put(SqliteInstraction.Term_Report_Card_id_term_report_card,id_term_report_card);
        cv.put(SqliteInstraction.Term_Report_Card_id_term,id_term);
        cv.put(SqliteInstraction.Term_Report_Card_term,term);

        db.insert(SqliteInstraction.TABLE_Term_Report_Card,null,cv);
        db.close();
    }


    public void ADD_lesson_report_card(int id_user,int id_term,int id_lesson_report_card,int id_lesson,float grade){


        SQLiteDatabase db=dataBaseHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(SqliteInstraction.Lesson_Report_Card_id_user,id_user);
        cv.put(SqliteInstraction.Lesson_Report_Card_id_term,id_term);
        cv.put(SqliteInstraction.Lesson_Report_Card_id_lesson_report_card,id_lesson_report_card);
        cv.put(SqliteInstraction.Lesson_Report_Card_id_lesson,id_lesson);
        cv.put(SqliteInstraction.Lesson_Report_Card_grade,grade);

        db.insert(SqliteInstraction.TABLE_Lesson_Report_Card,null,cv);
        db.close();
    }
}
