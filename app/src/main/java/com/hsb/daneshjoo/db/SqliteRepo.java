package com.hsb.daneshjoo.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb-pc on 1/19/2017.
 */
public class SqliteRepo {

    DataBaseHelper dataBaseHelper;
    public SqliteRepo(Context context){
        dataBaseHelper=new DataBaseHelper(context);
    }


    // get count  record
    public int Repo_account(){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql=" Select * from "+ SqliteInstraction.TABLE_Account;
        int num=0;

        Cursor cur=db.rawQuery(sql,null);

        if(cur.moveToFirst()){
            {
                num=cur.getInt(0);

            }while (cur.moveToNext());
        }

        db.close();

        if (num>0)
            return num;
        else
            return 0;
    }


    public Cursor Repo_account_UserName(){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql=" Select "+SqliteInstraction.Account_username+" from "+ SqliteInstraction.TABLE_Account;
        return db.rawQuery(sql,null);
    }


    // get exeist username
    public boolean Repo_account_username(String username){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="select id from " + SqliteInstraction.TABLE_Account + " where "+SqliteInstraction.Account_username+"='"+username+"'";
        Cursor cur=db.rawQuery(sql,null);
        int id=0;
        if (cur.moveToFirst()){
            {
                id=cur.getInt(0);
            }while (cur.moveToNext());
        }

        db.close();
        if (id>0)
            return false;
        else
            return true;
    }


    // get exeist id_user
    public Integer Repo_account_Iduser(){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="select id_user from " + SqliteInstraction.TABLE_Account ;
        Cursor cur=db.rawQuery(sql,null);
        int id_user=0;
        if (cur.moveToFirst()){
            {
                id_user=cur.getInt(0);
            }while (cur.moveToNext());
        }

        db.close();
        if (id_user>0)
            return id_user;
        else
            return 0;
    }

    public boolean Repo_login(String u_name,String pass){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="select * from "+SqliteInstraction.TABLE_Account+" where "+SqliteInstraction.Account_username+"='"+u_name+"' and "+SqliteInstraction.Account_password+"='"+pass+"'";
        Log.i("sql",sql);
        Cursor cur=db.rawQuery(sql,null);
        String username="";
        String password="";
        String id_user;
        String email;

        Log.i("sql",cur.toString());
        if(cur.moveToFirst()){
            {
                username=cur.getString(2);
                password=cur.getString(4);
                id_user=cur.getString(1);
                email=cur.getString(3);

            }while (cur.moveToNext());
        }
        Log.i("sql",username);
        Log.i("sql",password);
        if (username.equals(u_name) && password.equals(pass)){
            return  true;
        }

        return false;
    }

    // get all table term
    public Cursor Repo_term(){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Term + " order by " +SqliteInstraction.Term_term+" ASC";
        return db.rawQuery(sql,null);
    }





    public Cursor Repo_term(int id_term){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Term + " where "+SqliteInstraction.Term_id +"='"+id_term+"'";
        return db.rawQuery(sql,null);
    }



    public Cursor Repo_term_id(int term){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Term + " where "+SqliteInstraction.Term_term +"='"+term+"'";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_getterm(int idterm){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT "+SqliteInstraction.Term_term+" FROM "+ SqliteInstraction.TABLE_Term + " where "+SqliteInstraction.Term_id +"='"+idterm+"'";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_getterm(){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT "+SqliteInstraction.Term_term+" FROM "+ SqliteInstraction.TABLE_Term +" order by "+SqliteInstraction.Term_term +" ASC";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_getfullterm(int idterm){
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Term + " where "+SqliteInstraction.Term_term +"='"+idterm+"'order by "+SqliteInstraction.Term_term+" ASC ";
        return db.rawQuery(sql,null);
    }


    public Cursor Repo_lesson(int id_term) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Lesson +" where "+SqliteInstraction.Lesson_id_term+"='"+id_term+"' order by "+SqliteInstraction.Lesson_day_class+" ASC ";
        return db.rawQuery(sql,null);
    }


    public Cursor Repo_lesson() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Lesson +" order by "+SqliteInstraction.Lesson_day_class+" ASC";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_idlesson(int id_lesson) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Lesson +" where "+SqliteInstraction.Lesson_id_lesson+"='"+id_lesson+"' order by "+SqliteInstraction.Lesson_day_class+" ASC ";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_getNameLesson(int idLesson) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT  "+ SqliteInstraction.Lesson_name_lesson+"  FROM "+ SqliteInstraction.TABLE_Lesson +" where "+SqliteInstraction.Lesson_id_lesson +"='"+idLesson+"'";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_Idlesson(int id_lesson) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Lesson +" where "+SqliteInstraction.Lesson_id+"='"+id_lesson+"'";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_ListNameLesson() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT id,name_lesson FROM "+ SqliteInstraction.TABLE_Lesson ;
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_ListNameLesson(int idterm) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT id,name_lesson,id_lesson FROM "+ SqliteInstraction.TABLE_Lesson +" where "+SqliteInstraction.Term_id_term+"='"+idterm+"'";
        return db.rawQuery(sql,null);
    }

    public int getIdUser() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="select "+SqliteInstraction.Account_id_user+" from " + SqliteInstraction.TABLE_Account ;
        Cursor cur=db.rawQuery(sql,null);
        int id=0;
        if (cur.moveToFirst()){
            {
                id=cur.getInt(0);
            }while (cur.moveToNext());
        }
        Log.i("IIIIIDDDDDDD",String.valueOf(id));
        db.close();
        return  id;
    }

    public int getIdStudent() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="select "+SqliteInstraction.Student_id+" from " + SqliteInstraction.TABLE_Student ;
        Cursor cur=db.rawQuery(sql,null);
        int id=0;
        if (cur.moveToFirst()){
            {
                id=cur.getInt(0);
            }while (cur.moveToNext());
        }
        Log.i("IIIIIDDDDDDD",String.valueOf(id));
        db.close();
        return  id;
    }

    public Cursor Repo_reminder() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_NoteReminder;
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_reminderIdTerm(int idterm) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_NoteReminder +" where "+SqliteInstraction.Reminder_id_term+"='"+idterm+"'";;
        return db.rawQuery(sql,null);
    }


    public Cursor Repo_reminder(int idReminder) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_NoteReminder +" where "+SqliteInstraction.Reminder_id+"='"+idReminder+"'";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_Notereminder(int date) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_NoteReminder +" where "+SqliteInstraction.Reminder_date_reminder+"='"+date+"'";
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_note() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Note;
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_profile() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Student;
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_note(int idNote) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Note +" where "+SqliteInstraction.Note_id+"='"+idNote+"'";
        return db.rawQuery(sql,null);
    }


    public Cursor Repo_noteIdTerm(int idterm) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Note +" where "+SqliteInstraction.Note_id_term+"='"+idterm+"'";
        return db.rawQuery(sql,null);
    }

    private Cursor Repo_codeVersion() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_App_Version ;
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_termreportcard() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Term_Report_Card + "  order by "+SqliteInstraction.Term_Report_Card_term+" ASC " ;
        return db.rawQuery(sql,null);
    }


    public Cursor Repo_lessonreportcard() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Lesson_Report_Card ;
        return db.rawQuery(sql,null);
    }

    public Cursor Repo_lessonreportcard(int idterm) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT * FROM "+ SqliteInstraction.TABLE_Lesson_Report_Card  + " where "+SqliteInstraction.Lesson_Report_Card_id_term+"='"+idterm+"'";
        return db.rawQuery(sql,null);
    }


    private Cursor Repo_lessonreportcard_grade(int id_lesson_report_card) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT "+SqliteInstraction.Lesson_Report_Card_grade+" FROM "+ SqliteInstraction.TABLE_Lesson_Report_Card  + " where "+SqliteInstraction.Lesson_Report_Card_id_lesson_report_card+"='"+id_lesson_report_card+"'";
        return db.rawQuery(sql,null);
    }


    private Cursor Repo_lessonreportcard_grade_idterm(int id_term) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT "+SqliteInstraction.Lesson_Report_Card_grade+" FROM "+ SqliteInstraction.TABLE_Lesson_Report_Card  + " where "+SqliteInstraction.Lesson_Report_Card_id_term+"='"+id_term+"'";
        return db.rawQuery(sql,null);
    }

    private Cursor Repo_CountLesson() {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT Count("+SqliteInstraction.Lesson_id_lesson+") FROM "+ SqliteInstraction.TABLE_Lesson ;
        return db.rawQuery(sql,null);
    }


    private Cursor Repo_CountLesson(int idTerm) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT Sum("+SqliteInstraction.Lesson_count_unit+") FROM "+ SqliteInstraction.TABLE_Lesson  +" where "+SqliteInstraction.Lesson_id_term+"='"+idTerm+"'";
        return db.rawQuery(sql,null);
    }


    private Cursor Repo_NameLesson(int id_term) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT "+SqliteInstraction.Lesson_name_lesson+" FROM "+ SqliteInstraction.TABLE_Lesson   +" where "+SqliteInstraction.Lesson_id_term+"='"+id_term+"'";
        return db.rawQuery(sql,null);
    }

    private Cursor Repo_lessoncountunit(int idlesson) {
        SQLiteDatabase db=dataBaseHelper.getReadableDatabase();
        String sql="SELECT "+SqliteInstraction.Lesson_count_unit+" FROM "+ SqliteInstraction.TABLE_Lesson  +" where "+SqliteInstraction.Lesson_id_lesson+"='"+idlesson+"'" ;

        return db.rawQuery(sql,null);
    }

    public String getNameLesson(int idLesson){
        String name="";
        Cursor cur=Repo_getNameLesson(idLesson);


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    name=cur.getString(0);
                }
                while (cur.moveToNext()) ;
            }
        }

        Log.i("NAME",name);
        return name;
    }



    public int getCodeVersion(){
        int codeversion=0;
        Cursor cur=Repo_codeVersion();


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    codeversion=cur.getInt(1);
                }
                while (cur.moveToNext()) ;
            }
        }

        Log.i("NAME",String.valueOf(codeversion));
        return codeversion;
    }


    public int getCountLesson() {
        int countlesson = 0;
        Cursor cur = Repo_CountLesson();


        if (cur != null) {
            if (cur.getCount() > 0) {
                cur.moveToFirst();
                do {
                    countlesson = cur.getInt(0);
                }
                while (cur.moveToNext());
            }
        }
        return countlesson;
    }


    public int getSumUnit(int idTerm) {
        int countUnit = 0;
        Cursor cur = Repo_term_id(idTerm);


        if (cur != null) {
            if (cur.getCount() > 0) {
                cur.moveToFirst();
                do {
                    countUnit = cur.getInt(5);
                }
                while (cur.moveToNext());
            }
        }
        return countUnit;
    }


    public int getCountLesson(int idTerm){
        int countlesson=0;
        Cursor cur=Repo_CountLesson(idTerm);


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    countlesson=cur.getInt(0);
                    Log.i("countlesson",countlesson+"");
                }
                while (cur.moveToNext()) ;
            }
        }

        return countlesson;
    }



    public float getGrade(int id_lesson_report_card){
        float grade=0;
        Cursor cur=Repo_lessonreportcard_grade(id_lesson_report_card);


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    grade=cur.getInt(0);
                }
                while (cur.moveToNext()) ;
            }
        }


        return grade;
    }


    public List<String> getListTerm(){
        List<String> termlist=new ArrayList<>();
        Cursor cur=Repo_termreportcard();


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    termlist.add(String.valueOf(cur.getInt(4)));
                }
                while (cur.moveToNext()) ;
            }
        }


        return termlist;
    }

    public List<Float> getListGrade(int id_term){
        List<Float> gradelist=new ArrayList<>();

        Cursor cur=Repo_lessonreportcard_grade_idterm(id_term);


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    gradelist.add(cur.getFloat(0));
                }
                while (cur.moveToNext()) ;
            }
        }


        return gradelist;
    }

    public List<String> getListNameLesson(int id_term){
        List<String> gradelist=new ArrayList<>();

        Cursor cur=Repo_NameLesson(id_term);


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    gradelist.add(cur.getString(0));
                    Log.i("name lesson",cur.getString(0));
                }
                while (cur.moveToNext()) ;
            }
        }


        return gradelist;
    }

    public float getTotalgradelesson(int idterm){
        float grade=0;
        float sumgrade=0;
        Cursor cur=Repo_lessonreportcard(idterm);


        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{
                    grade=cur.getInt(5);
                    Log.i("grade ",String.valueOf(grade));
                    Cursor cursor=Repo_lessoncountunit(cur.getInt(4));
                    if (cursor.getCount()>0) {
                        cursor.moveToFirst();
                        do {

                            sumgrade=sumgrade+grade*cursor.getInt(0);
                            Log.i("sumgrade ",String.valueOf(cursor.getInt(0)));

                        }
                        while (cursor.moveToNext());
                    }

                }
                while (cur.moveToNext()) ;
            }
        }


        return sumgrade;
    }

}
