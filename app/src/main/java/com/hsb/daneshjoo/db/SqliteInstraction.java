package com.hsb.daneshjoo.db;

/**
 * Created by hsb-pc on 1/19/2017.
 */
public class SqliteInstraction {

    public static final String TABLE_Account="account";

    public static final String TABLE_Student="student";

    public static final String TABLE_Term="term";

    public static final String TABLE_Lesson="lesson";

    public static final String TABLE_NoteReminder="reminder";

    public static final String TABLE_Note="note";

    public static final String TABLE_Fields="fields";

    public static final String TABLE_App_Version="app_version";

    public static final String TABLE_Term_Report_Card="term_report_card";

    public static final String TABLE_Lesson_Report_Card="lesson_report_card";




    // Table account
    public static final String Account_id="id";
    public static final String Account_id_user="id_user";
    public static final String Account_username="user_name";
    public static final String Account_email="email";
    public static final String Account_password="password";
    public static final String TABLE_CREATE_account="CREATE TABLE IF NOT EXISTS " +TABLE_Account+
            "("+
                Account_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                Account_id_user +" INTEGER,"+
                Account_username+" TEXT,"+
                Account_email+" TEXT,"+
                Account_password+" TEXT"+
            ");";

    // Table student
    public static final String Student_id="id";
    public static final String Student_id_user="id_user";
    public static final String Student_name="name";
    public static final String Student_family="family";
    public static final String Student_field="field";
    public static final String Student_level="level";
    public static final String Student_entering_year="entering_year";
    public static final String Student_age="age";
    public static final String Student_sex="sex";
    public static final String Student_univercity="univercity";
    public static final String TABLE_CREATE_student="CREATE TABLE IF NOT EXISTS " +TABLE_Student+
            "("+
            Student_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Student_id_user +" INTEGER,"+
            Student_name+" TEXT,"+
            Student_family+" TEXT,"+
            Student_field+" TEXT,"+
            Student_level+" LEVEL,"+
            Student_entering_year+" INTEGER,"+
            Student_age+" INTEGER,"+
            Student_sex+" INTEGER,"+
            Student_univercity+" TEXT"+
            ");";

    // Table term
    public static final String Term_id="id";
    public static final String Term_id_user="id_user";/* یوزر کاربر */
    public static final String Term_id_term="id_term";/* ای دی ترم*/
    public static final String Term_term="term"; /* نیم سال اول یا نیم سال دوم */
    public static final String Term_year_date="year_date";
    public static final String Term_sum_unit="sum_unit";/* جمع واحد  این ترم */
    public static final String Term_date_start="date_start"; /* تاریخ شروع ترم */
    public static final String Term_date_end="date_end"; /*  تاریخ پایان ترم*/
    public static final String TABLE_CREATE_term="CREATE TABLE IF NOT EXISTS " +TABLE_Term+
            "("+
            Term_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Term_id_user +" INTEGER,"+
            Term_id_term +" INTEGER,"+
            Term_term+" INTEGER,"+
            Term_year_date+" INTEGER,"+
            Term_sum_unit+" INTEGER,"+
            Term_date_start+" INTEGER,"+
            Term_date_end+" INTEGER, "+
            "UNIQUE ("+Term_id+","+Term_term+","+Term_year_date+","+Term_sum_unit+","+Term_date_start+","+Term_date_end+")"+
            "ON CONFLICT REPLACE "+
            ");";

    // Table lesson
    public static final String Lesson_id="id";
    public static final String Lesson_id_user="id_user";
    public static final String Lesson_id_term="id_term";
    public static final String Lesson_id_lesson="id_lesson";
    public static final String Lesson_name_lesson="name_lesson";
    public static final String Lesson_count_unit="count_unit";
    public static final String Lesson_name_teacher="name_teacher";
    public static final String Lesson_day_class="day_class";
    public static final String Lesson_clock_class="clock_class";
    public static final String Lesson_number_class="number_class";
    public static final String Lesson_date_exam="date_exam";
    public static final String TABLE_CREATE_lesson="CREATE TABLE IF NOT EXISTS " +TABLE_Lesson +
            "("+
            Lesson_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Lesson_id_user +" INTEGER,"+
            Lesson_id_term +" INTEGER,"+
            Lesson_id_lesson +" INTEGER,"+
            Lesson_name_lesson+" TEXT,"+
            Lesson_count_unit+" INTEGER,"+
            Lesson_name_teacher+" TEXT,"+
            Lesson_day_class+" INTEGER,"+
            Lesson_clock_class+" TEXT,"+
            Lesson_number_class+" INTEGER,"+
            Lesson_date_exam+" INTEGER"+

            ");";

    // Table reminder
    public static final String Reminder_id="id";
    public static final String Reminder_id_user="id_user";
    public static final String Reminder_id_lesson="id_lesson";
    public static final String Reminder_id_reminder="id_reminder";
    public static final String Reminder_flag="flag";
    public static final String Reminder_titer="titer";
    public static final String Reminder_note="note";
    public static final String Reminder_date_reminder="date_reminder";
    public static final String Reminder_clock_reminder="clock_reminder";
    public static final String Reminder_id_term="id_term";
    public static final String TABLE_CREATE_reminder="CREATE TABLE IF NOT EXISTS " +TABLE_NoteReminder +
            "("+
            Reminder_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Reminder_id_user +" INTEGER,"+
            Reminder_id_lesson+" INTEGER,"+
            Reminder_id_reminder+" INTEGER,"+
            Reminder_flag+" INTeGER,"+
            Reminder_titer+" TEXT,"+
            Reminder_note+" TEXT,"+
            Reminder_date_reminder+" INTEGER,"+
            Reminder_clock_reminder+" TEXT, "+
            Reminder_id_term+" INTEGER "+
            ");";

    // Table note
    public static final String Note_id="id";
    public static final String Note_id_user="id_user";
    public static final String Note_id_lesson="id_lesson";
    public static final String Note_id_note="id_note";
    public static final String Note_titer="titer";
    public static final String Note_note="note";
    public static final String Note_id_term="id_term";
    public static final String TABLE_CREATE_note="CREATE TABLE IF NOT EXISTS " +TABLE_Note+
            "("+
            Note_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Note_id_user +" INTEGER,"+
            Note_id_lesson+" INTEGER,"+
            Note_id_note+" INTEGER,"+
            Note_titer+" TEXT,"+
            Note_note+" TEXT,"+
            Note_id_term+" INTEGER"+
            ");";


    // Table Fields
    public static final String Fields_id="id";
    public static final String Fields_name_field="name_field";
    public static final String TABLE_CREATE_App_version="CREATE TABLE IF NOT EXISTS " +TABLE_Fields +
            "("+
            Fields_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Fields_name_field +" TEXT "+

            ");";


    // Table App_Version
    public static final String App_Version_id="id";
    public static final String App_Version_Code_version="code_version";
    public static final String App_Version_date_release="date_release";
    public static final String TABLE_CREATE_fields="CREATE TABLE IF NOT EXISTS " +TABLE_App_Version +
            "("+
            App_Version_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            App_Version_Code_version +" INTEGER , "+
            App_Version_date_release +" TEXT  "+
            ");";


    // Table Term_Report_Card
    public static final String Term_Report_Card_id="id";
    public static final String Term_Report_Card_id_user="id_user";
    public static final String Term_Report_Card_id_term_report_card="id_term_report_card";
    public static final String Term_Report_Card_id_term="id_term";
    public static final String Term_Report_Card_term="term";
    public static final String TABLE_CREATE_term_report_card="CREATE TABLE IF NOT EXISTS " +TABLE_Term_Report_Card +
            "("+
            Term_Report_Card_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Term_Report_Card_id_user+" INTEGER , "+
            Term_Report_Card_id_term_report_card +" INTEGER , "+
            Term_Report_Card_id_term +" INTEGER ,  "+
            Term_Report_Card_term +" INTEGER  "+
            ");";


    // Table Lesson_Report_Card
    public static final String Lesson_Report_Card_id="id";
    public static final String Lesson_Report_Card_id_user="id_user";
    public static final String Lesson_Report_Card_id_term="id_term";
    public static final String Lesson_Report_Card_id_lesson_report_card="id_lesson_report_card";
    public static final String Lesson_Report_Card_id_lesson="id_lesson";
    public static final String Lesson_Report_Card_grade="grade";
    public static final String TABLE_CREATE_lesson_report_card="CREATE TABLE IF NOT EXISTS " +TABLE_Lesson_Report_Card +
            "("+
            Lesson_Report_Card_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Lesson_Report_Card_id_user +" INTEGER , "+
            Lesson_Report_Card_id_term +" INTEGER , "+
            Lesson_Report_Card_id_lesson_report_card +" INTEGER , "+
            Lesson_Report_Card_id_lesson +" INTEGER ,  "+
            Lesson_Report_Card_grade +" REAL   "+
            ");";
}
