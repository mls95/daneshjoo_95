package com.hsb.daneshjoo.db;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by hsb-pc on 3/12/2017.
 */
public class RestoreSqliteDataBase {
    String inFileName="/data/data/com.hsb.daneshjoo/databases/daneshjoo.db";

    Context context;
    public void Restore() throws IOException {
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();

        if (sd.canWrite()) {
            String currentDBPath = inFileName;
            String backupDBPath = "daneshjoo.db";
            File currentDB = new File(currentDBPath);
            if (currentDB.isFile()) {
                // currentDB.delete();
                Log.i("list", currentDB.getParentFile().list()[0]);
            }
            File backupDB = new File(Environment.getExternalStorageDirectory() + "/aaaaa/daneshjoo2.db");
            Log.i("exists ", String.valueOf(currentDB.exists()));
            if (currentDB.exists()) {
                FileChannel src = new FileInputStream(backupDB).getChannel();
                FileChannel dst = new FileOutputStream(currentDB).getChannel();
                dst.transferFrom(src, 0, src.size());

                src.close();
                dst.close();
            }


        }
    }
}
