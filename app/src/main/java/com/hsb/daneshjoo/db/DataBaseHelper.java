package com.hsb.daneshjoo.db;

import android.app.Instrumentation;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hsb-pc on 1/19/2017.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    public static final int DB_verion=1;
    public static final String DB_name="daneshjoo.db";


    public DataBaseHelper(Context context){
        super(context,DB_name,null,DB_verion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_account);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_student);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_fields);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_term);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_lesson);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_reminder);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_note);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_App_version);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_term_report_card);
        sqLiteDatabase.execSQL(SqliteInstraction.TABLE_CREATE_lesson_report_card);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
