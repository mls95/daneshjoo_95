package com.hsb.daneshjoo.data;

import android.app.Activity;
import android.database.Cursor;
import android.util.Log;

import com.hsb.daneshjoo.PersianDateDialog;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.home.HomeItem;
import com.hsb.daneshjoo.cards.lesson.LessonItem;
import com.hsb.daneshjoo.cards.lessonreportcard.LessonReportCardItem;
import com.hsb.daneshjoo.cards.note.NoteItem;
import com.hsb.daneshjoo.cards.reminder.Reminderitem;
import com.hsb.daneshjoo.cards.term.TermItem;
import com.hsb.daneshjoo.cards.termreportcard.TermReportCard;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 2/24/2017.
 *
 */
public class DbToArrayList {

    Activity a;
    SqliteRepo repo;
    PersianDateDialog persianDateDialog=new PersianDateDialog();
    KeySetting keySetting;
    utility utilit;
    public DbToArrayList() {
        super();
    }

    public DbToArrayList(Activity a){
        this.a=a;
        utilit=new utility(a);
        keySetting=new KeySetting(a.getApplicationContext());
       repo =new SqliteRepo(a.getApplicationContext());
    }


    public ArrayList<TermItem> getDataTerm() {
        ArrayList results = new ArrayList<TermItem>();
        SqliteRepo repo=new SqliteRepo(a.getApplicationContext());
        Cursor cur=repo.Repo_term();
        TermItem obj;
        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{

                    if (cur.getInt(4)==0)
                        obj = new TermItem(cur.getInt(0),cur.getInt(2),""+String.valueOf(cur.getInt(3)),a.getApplicationContext().getResources().getString(R.string.tv_termcardview_year)+ a.getApplicationContext().getResources().getStringArray(R.array.Term_Spinner)[0], a.getApplicationContext().getResources().getString(R.string.tv_termcardview_sumUnit)+ String.valueOf(cur.getInt(5)), a.getApplicationContext().getResources().getString(R.string.tv_termcardview_dateStart)+ persianDateDialog.setDate(String.valueOf(cur.getInt(6))), a.getApplicationContext().getResources().getString(R.string.tv_termcardview_dateEnd)+ persianDateDialog.setDate(String.valueOf(cur.getInt(7))));
                    else if (cur.getInt(4)==1)
                        obj = new TermItem(cur.getInt(0),cur.getInt(2),""+String.valueOf(cur.getInt(3)),a.getApplicationContext().getResources().getString(R.string.tv_termcardview_year)+ a.getApplicationContext().getResources().getStringArray(R.array.Term_Spinner)[1], a.getApplicationContext().getResources().getString(R.string.tv_termcardview_sumUnit)+ String.valueOf(cur.getInt(5)), a.getApplicationContext().getResources().getString(R.string.tv_termcardview_dateStart)+ persianDateDialog.setDate(String.valueOf(cur.getInt(6))),a.getApplicationContext().getResources().getString(R.string.tv_termcardview_dateEnd) + persianDateDialog.setDate(String.valueOf(cur.getInt(7))));
                    else
                        obj = new TermItem(cur.getInt(0),cur.getInt(2),""+String.valueOf(cur.getInt(3)),a.getApplicationContext().getResources().getString(R.string.tv_termcardview_year)+ a.getApplicationContext().getResources().getStringArray(R.array.Term_Spinner)[2], a.getApplicationContext().getResources().getString(R.string.tv_termcardview_sumUnit)+ String.valueOf(cur.getInt(5)), a.getApplicationContext().getResources().getString(R.string.tv_termcardview_dateStart)+ persianDateDialog.setDate(String.valueOf(cur.getInt(6))), a.getApplicationContext().getResources().getString(R.string.tv_termcardview_dateEnd) + persianDateDialog.setDate(String.valueOf(cur.getInt(7))));


                    results.add(i, obj);
                    i++;
                } while (cur.moveToNext()) ;
            }
        }

        return results;
    }

    public ArrayList<Reminderitem> getDataReminder() {
        ArrayList results = new ArrayList<TermItem>();
        SqliteRepo repo=new SqliteRepo(a);


        Cursor cur;
        if(keySetting.getTerm()==0)
            cur=repo.Repo_reminder();
        else
            cur=repo.Repo_reminderIdTerm(keySetting.getTerm());
        String h="";
        String m="";

        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{

                    h=String.valueOf(cur.getString(8)).substring(0,2);
                    m=String.valueOf(cur.getString(8)).substring(2,4);

                    Reminderitem obj = new Reminderitem(cur.getInt(0),cur.getInt(2),String.valueOf(cur.getInt(3)),repo.getNameLesson(cur.getInt(2)),cur.getString(5),persianDateDialog.setDate(String.valueOf(cur.getInt(7))),h+":"+m);
                    results.add(i, obj);
                    i++;
                } while (cur.moveToNext()) ;
            }
        }

        return results;
    }


    public ArrayList<LessonReportCardItem> getDataLessonReportCard(int idterm) {
        ArrayList results = new ArrayList<LessonReportCardItem>();
        SqliteRepo repo=new SqliteRepo(a);
        Cursor cur=repo.Repo_lessonreportcard(idterm);


        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{

                    Cursor cursor=repo.Repo_idlesson(cur.getInt(4));
                    if(cursor.getCount()>0){
                        cursor.moveToFirst();
                        do{
                            LessonReportCardItem obj = new LessonReportCardItem(cur.getInt(3),cur.getInt(4),cur.getInt(2),cursor.getInt(5),cursor.getString(4),cur.getFloat(5));
                            results.add(i, obj);
                        }while (cursor.moveToNext());
                    }

                    i++;
                } while (cur.moveToNext()) ;
            }
        }

        return results;
    }

    public ArrayList<TermReportCard> getDataTermReportCard() {
        ArrayList results = new ArrayList<TermReportCard>();
        SqliteRepo repo=new SqliteRepo(a);
        Cursor cur=repo.Repo_termreportcard();

        int total_semester;

        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{

                    Cursor cursor =repo.Repo_getfullterm(cur.getInt(4));
                    Cursor cursor1 =repo.Repo_lesson(cur.getInt(4));
                    total_semester=0;
                    // total semester in table Lesson
                    if(cursor1.getCount()>0){
                        cursor1.moveToFirst();
                        do{
                            total_semester =total_semester +cursor1.getInt(5);

                        }while (cursor1.moveToNext());
                    }


                    if(cursor.getCount()>0){
                        cursor.moveToFirst();
                        do{
                            TermReportCard obj = new TermReportCard(cursor.getInt(2),cur.getInt(2),cursor.getInt(3),total_semester,repo.getTotalgradelesson(cur.getInt(4)));
                            results.add(i, obj);
                        }while (cursor.moveToNext());
                    }




                    i++;
                } while (cur.moveToNext()) ;
            }
        }

        return results;
    }


    public ArrayList<NoteItem> getDataNote() {
        ArrayList results = new ArrayList<NoteItem>();
        SqliteRepo repo=new SqliteRepo(a.getApplicationContext());

        Cursor cur;
        if(keySetting.getTerm()==0)
            cur=repo.Repo_note();
        else
            cur=repo.Repo_noteIdTerm(keySetting.getTerm());


        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{


                    NoteItem obj = new NoteItem(cur.getInt(0),cur.getInt(2),String.valueOf(cur.getInt(3)),repo.getNameLesson(cur.getInt(2)),cur.getString(4),cur.getString(5));
                    results.add(i, obj);
                    i++;
                } while (cur.moveToNext()) ;
            }
        }

        return results;
    }


    public ArrayList<HomeItem> getDataHome() {
        ArrayList results = new ArrayList<HomeItem>();

        SqliteRepo repo=new SqliteRepo(a);
        Cursor cur;
        if(keySetting.getTerm()==0)
            cur=repo.Repo_lesson();
        else
            cur=repo.Repo_lesson(keySetting.getTerm());

        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{


                    HomeItem obj=new HomeItem(cur.getInt(0),cur.getInt(2),cur.getString(4),cur.getString(5),cur.getInt(7),utilit.getNameDay(cur.getInt(7)),"14:30","402",persianDateDialog.setDate(String.valueOf(cur.getInt(10))));
                    results.add(i, obj);
                    i++;
                } while (cur.moveToNext()) ;
            }
        }

        return results;
    }

    public ArrayList<LessonItem> getDataLesson() {
        ArrayList results = new ArrayList<LessonItem>();

        Cursor cur=repo.Repo_lesson(utility.getIdTerm());

        int i=0;
        if (cur !=null){
            if (cur.getCount()>0) {
                cur.moveToFirst();
                do{


                    LessonItem obj = new LessonItem(cur.getInt(0),a.getApplicationContext().getResources().getString(R.string.tv_lesson_cardview_nameLesson)+cur.getString(4),cur.getInt(3),a.getApplicationContext().getResources().getString(R.string.tv_lesson_cardview_Unit)+cur.getString(5),a.getApplicationContext().getResources().getString(R.string.tv_lesson_cardveiw_name_teacher)+cur.getString(6),a.getApplicationContext().getResources().getString(R.string.tv_lesson__cardview_dayClass)+utilit.getNameDay(cur.getInt(7)),"تاریخ امتحان : "+persianDateDialog.setDate(String.valueOf(cur.getInt(10))));
                    results.add(i, obj);
                    i++;
                }
                while (cur.moveToNext()) ;
            }
        }
        return results;
    }



}
