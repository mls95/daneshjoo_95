package com.hsb.daneshjoo.MenuActionBar;

/**
 * Created by hsb on 8/2/2017.
 */

public class SpinnerNavItem {
    private String title;
    private int icon;

    public SpinnerNavItem(String title, int icon){
        this.title = title;
        this.icon = icon;
    }

    public String getTitle(){
        return this.title;
    }

    public int getIcon(){
        return this.icon;
    }
}
