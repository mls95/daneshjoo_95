package com.hsb.daneshjoo.util;

import android.app.ProgressDialog;
import android.content.Context;

import com.hsb.daneshjoo.R;

import dmax.dialog.SpotsDialog;

/**
 * Created by hsb on 6/21/2017.
 */

public class LoadingProgress {
    Context c;
    ProgressDialog pd;
    SpotsDialog spd;

    public LoadingProgress(Context c) {
        this.c = c;
        //pd=new ProgressDialog(c, R.style.progressdialog);
         spd = new SpotsDialog(c,R.style.CustomStyleProgressDialog);

    }

    public void Show(){
       /* pd.setMessage("صبر کنید...");
        pd.setCancelable(false);
        pd.show();*/
       spd.show();
    }

    public void Close(){
       // pd.dismiss();
        spd.dismiss();
    }
}
