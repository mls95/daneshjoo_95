package com.hsb.daneshjoo.util;

import android.app.Application;

import com.anetwork.android.sdk.advertising.AnetworkAdvertising;
import com.hsb.daneshjoo.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class FontTypeFace  extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AnetworkAdvertising.initialize(this,"bb4d8c52-cd56-4482-a75f-4503d99ffbfd",false);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/segoeuisl.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}