package com.hsb.daneshjoo.utilities;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;

import static java.security.AccessController.getContext;

/**
 * Created by MoSiOne70 on 06/10/2017.
 */

public class HelpShowCaseView {

    Activity activity;


    public HelpShowCaseView(Activity activity) {
        this.activity = activity;

    }

    public void helpShowCase(View view,
                             String setTitle,
                             String setText,
                             String setBtnText,
                             int customStyle,
                             int atrLeft,
                             int atrTop,
                             int atrRight,
                             int atrBottom,
                             int atrMargin) {

        ViewTarget viewTarget = new ViewTarget(view);

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(atrLeft);
        lps.addRule(atrTop);
        lps.addRule(atrRight);
        lps.addRule(atrBottom);


        int margin = ((Number) (activity.getResources().getDisplayMetrics().density * atrMargin)).intValue();
        lps.setMargins(margin, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(activity)
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(setTitle)
                .setContentText(setText)
                .setStyle(customStyle)
                .build();
        sv.setButtonText(setBtnText);
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });
    }
}
