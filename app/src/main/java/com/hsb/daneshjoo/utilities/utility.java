package com.hsb.daneshjoo.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.SignUpActivity;
import com.hsb.daneshjoo.cards.lesson.LessonRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.note.NoteRecyclerViewAdpter;
import com.hsb.daneshjoo.cards.reminder.ReminderRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteDelete;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.services.DeleteApi;
import com.hsb.daneshjoo.services.DeleteApiWebService;
import com.hsb.daneshjoo.services.webservice.request.DeleteJson;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Created by hsb-pc on 1/23/2017.
 */
public class utility {

    Context c;

    public utility(Context c) {
        this.c = c;
    }

    public static int ID_TERM;
    public static int ID_LESSON_REPORT;
    public static int row_TERM;
    public static int Id = 0;
    public static String token_createstudent = "";
    public static String NameLesson = "";

    public static int getIdLessonReport() {
        return ID_LESSON_REPORT;
    }

    public static void setIdLessonReport(int idLessonReport) {
        ID_LESSON_REPORT = idLessonReport;
    }

    public static int getId() {
        return Id;
    }

    public static void setId(int id) {
        Id = id;
    }

    public static int getIdTerm() {
        return ID_TERM;
    }

    public static void setIdTerm(int idTerm) {
        ID_TERM = idTerm;
    }

    public static int getRow_TERM() {
        return row_TERM;
    }

    public static void setRow_TERM(int row_TERM) {
        utility.row_TERM = row_TERM;
    }

    public static void setToken_createstudent(String token) {
        token_createstudent = token;
    }

    public static String getToken_createstudent() {
        return token_createstudent;
    }

    public static String getNameLesson() {
        return NameLesson;
    }

    public static void setNameLesson(String nameLesson) {
        NameLesson = nameLesson;
    }


    public static String encrypt(String s){

        // convert to big integer
        BigInteger bigInt = new BigInteger(s.getBytes());


        return String.valueOf(bigInt.intValue());
    }
    public static String md5(String s) {
        try {


            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(s.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            System.out.println("Digest(in hex format):: " + sb.toString());

            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            System.out.println("Digest(in hex format):: " + hexString.toString());

            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static void dialog(String title, String message, Context c) {
        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(c);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(true);
        alert.setPositiveButton(c.getResources().getString(R.string.Dialog_Message_Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        android.app.AlertDialog alert11 = alert.create();
        alert11.show();
    }

    public static void dialog(String title, String message, String positiveButton, String negativeButton, final String token, final Context c, final Activity activity) {

        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(c);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(true);
        alert.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SqliteDelete sqliteDelete = new SqliteDelete(c);

                if (token == "delete-term") {
                    SqliteRepo sqliteRepo = new SqliteRepo(activity);
                    DeleteJson deleteJson = new DeleteJson(String.valueOf(sqliteRepo.getIdUser()), String.valueOf(utility.getIdTerm()));
                    DeleteApiWebService webService = new DeleteApiWebService(activity);
                    webService.RunApi(DeleteApi.API_DEL_TERM, deleteJson);

                } else if (token == "delete-lesson") {

                    SqliteRepo sqliteRepo = new SqliteRepo(activity);
                    DeleteJson deleteJson = new DeleteJson(String.valueOf(sqliteRepo.getIdUser()), String.valueOf(getId()));
                    DeleteApiWebService webService = new DeleteApiWebService(activity);
                    webService.RunApi(DeleteApi.API_DEL_LESSON, deleteJson);


                    DbToArrayList dbToArrayList = new DbToArrayList(activity);
                    RecyclerView mRecyclerView = (RecyclerView) activity.findViewById(R.id.lesson_recycler_view);
                    mRecyclerView.setHasFixedSize(true);
                    // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));
                    LessonRecyclerViewAdapter mAdapter = new LessonRecyclerViewAdapter(dbToArrayList.getDataLesson(), activity, activity);
                    mRecyclerView.setAdapter(mAdapter);

                } else if (token == "delete-reminder") {

                    SqliteRepo sqliteRepo = new SqliteRepo(activity);
                    DeleteJson deleteJson = new DeleteJson(String.valueOf(sqliteRepo.getIdUser()), String.valueOf(getId()));
                    DeleteApiWebService webService = new DeleteApiWebService(activity);
                    webService.RunApi(DeleteApi.API_DEL_REMINDER, deleteJson);


                    DbToArrayList dbToArrayList = new DbToArrayList(activity);
                    RecyclerView mRecyclerView = (RecyclerView) activity.findViewById(R.id.reminder_recycler_view);
                    mRecyclerView.setHasFixedSize(true);
                    // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));
                    ReminderRecyclerViewAdapter mAdapter = new ReminderRecyclerViewAdapter(dbToArrayList.getDataReminder(), activity, activity);
                    mRecyclerView.setAdapter(mAdapter);

                } else if (token == "delete-note") {

                    SqliteRepo sqliteRepo = new SqliteRepo(activity);
                    DeleteJson deleteJson = new DeleteJson(String.valueOf(sqliteRepo.getIdUser()), String.valueOf(getId()));
                    DeleteApiWebService webService = new DeleteApiWebService(activity);
                    webService.RunApi(DeleteApi.API_DEL_NOTE, deleteJson);


                    DbToArrayList dbToArrayList = new DbToArrayList(activity);
                    RecyclerView mRecyclerView = (RecyclerView) activity.findViewById(R.id.note_recycler_view);
                    mRecyclerView.setHasFixedSize(true);
                    // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    NoteRecyclerViewAdpter mAdapter = new NoteRecyclerViewAdpter(dbToArrayList.getDataNote(), activity, activity);
                    mRecyclerView.setAdapter(mAdapter);
                } else if (token == "token_logout") {
                    sqliteDelete.Del_AllData();

                    Intent intent = new Intent(activity, SignUpActivity.class);
                    activity.startActivity(intent);

                    activity.finish();
                }
            }
        });

        alert.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        android.app.AlertDialog alert11 = alert.create();
        alert11.show();
    }

    public String getNameDay(int numDay) {
        String day = "";
        switch (numDay) {
            case 0:
                day = c.getResources().getString(R.string.Saturday);
                break;
            case 1:
                day = c.getResources().getString(R.string.Sunday);
                break;
            case 2:
                day = c.getResources().getString(R.string.Monday);
                break;
            case 3:
                day = c.getResources().getString(R.string.Tuesday);
                break;
            case 4:
                day = c.getResources().getString(R.string.Wednesday);
                break;
            case 5:
                day = c.getResources().getString(R.string.Thursday);
                break;
            case 6:
                day = c.getResources().getString(R.string.Friday);
                break;
        }

        return day;

    }

}
