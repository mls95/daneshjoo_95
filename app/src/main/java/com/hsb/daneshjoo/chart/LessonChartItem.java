package com.hsb.daneshjoo.chart;

/**
 * Created by hsb on 8/18/2017.
 */

public class LessonChartItem {
    private String NameLesson;
    private double Grade;

    public LessonChartItem(String nameLesson, double grade) {
        NameLesson = nameLesson;
        Grade = grade;
    }


    public String getNameLesson() {
        return NameLesson;
    }

    public void setNameLesson(String nameLesson) {
        NameLesson = nameLesson;
    }

    public double getGrade() {
        return Grade;
    }

    public void setGrade(double grade) {
        Grade = grade;
    }
}
