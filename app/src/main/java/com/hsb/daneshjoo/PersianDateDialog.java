package com.hsb.daneshjoo;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alirezaafkar.sundatepicker.DatePicker;
import com.alirezaafkar.sundatepicker.components.DateItem;
import com.alirezaafkar.sundatepicker.interfaces.DateSetListener;
import com.hsb.daneshjoo.settings.KeySetting;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by hsb-pc on 2/19/2017.
 */
public class PersianDateDialog implements View.OnClickListener , DatePickerDialog.OnDateSetListener {

    Date mDate;
    String sdate="";
    FragmentActivity fragmentActivity;
    Activity a;
    TextView et;
    KeySetting keySetting;

    public PersianDateDialog(){}
    public PersianDateDialog(Activity activity) {
        this.a=activity;
        keySetting=new KeySetting(a);
        this.fragmentActivity = (FragmentActivity) activity;
    }

    public String getStringDate(TextView editText){

        et=editText;

        if (keySetting.getLanguage().equals("fa")) { // language is farsi , open datepicker persian
            DatePicker.Builder builder = new DatePicker
                    .Builder()
                    .theme(R.style.DialogTheme)
                    .future(true);
            mDate = new Date();
            builder.date(mDate.getDay(), mDate.getMonth(), mDate.getYear());
            builder.build(new DateSetListener() {
                @Override
                public void onDateSet(int i, @Nullable Calendar calendar, int i1, int i2, int i3) {
                    mDate.setDate(i1, i2, i3);

                    //textView
                    sdate = String.valueOf(i3) + String.valueOf(i2) + String.valueOf(i1);

                    et.setText(setDate(validation(i3, i2, i1)));
                    et.setTag(validation(i3, i2, i1));


                }
            }).show(fragmentActivity.getSupportFragmentManager(), "");
        }else{// language is english , open datepicker english


            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    PersianDateDialog.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );

            dpd.show(a.getFragmentManager(), "Datepickerdialog");
        }
        return this.sdate;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == com.alirezaafkar.sundatepicker.R.id.done) {

        }
    }

    public String validation(int year,int month,int day){

        String y=String.valueOf(year);
        String m="";
        String d="";


        switch (month){
            case 1:
                m="0"+String.valueOf(month);
                break;
            case 2:
                m="0"+String.valueOf(month);
                break;
            case 3:
                m="0"+String.valueOf(month);
                break;
            case 4:
                m="0"+String.valueOf(month);
                break;
            case 5:
                m="0"+String.valueOf(month);
                break;
            case 6:
                m="0"+String.valueOf(month);
                break;
            case 7:
                m="0"+String.valueOf(month);
                break;
            case 8:
                m="0"+String.valueOf(month);
                break;
            case 9:
                m="0"+String.valueOf(month);
                break;
            default:
                m=String.valueOf(month);

        }


        switch (day){
            case 1:
                d="0"+String.valueOf(day);
                break;
            case 2:
                d="0"+String.valueOf(day);
                break;
            case 3:
                d="0"+String.valueOf(day);
                break;
            case 4:
                d="0"+String.valueOf(day);
                break;
            case 5:
                d="0"+String.valueOf(day);
                break;
            case 6:
                d="0"+String.valueOf(day);
                break;
            case 7:
                d="0"+String.valueOf(day);
                break;
            case 8:
                d="0"+String.valueOf(day);
                break;
            case 9:
                d="0"+String.valueOf(day);
                break;
            default:
                d=String.valueOf(day);

        }



        return y+m+d;
    }

    public String setDate(String date){


        String y=date.substring(0,4);
        String m=date.substring(4,6);
        String d=date.substring(6,8);

        return y+"/"+m+"/"+d;
    }

    public class Date extends DateItem {
        public String getDate() {
            Calendar calendar = getCalendar();
            return String.format(Locale.US,
                    "%d/%d/%d",
                    getYear(), getMonth(), getDay(),
                    calendar.get(Calendar.YEAR),
                    +calendar.get(Calendar.MONTH) + 1,
                    +calendar.get(Calendar.DAY_OF_MONTH));
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        et.setText(setDate(validation(year,monthOfYear+1,dayOfMonth)));
        et.setTag(validation(year,monthOfYear,dayOfMonth));
    }


}
