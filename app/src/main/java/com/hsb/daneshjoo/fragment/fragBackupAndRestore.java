package com.hsb.daneshjoo.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.settings.BackupDialog;
import com.hsb.daneshjoo.settings.RestoreDialog;

/**
 * Created by hsb-pc on 3/12/2017.
 */
public class fragBackupAndRestore extends Fragment {

    Button btn_backup,btn_restore;
    View view;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_backup_restore,container,false);
        Initiliaze();
        return view;
    }

    private void Initiliaze(){
        btn_backup=(Button) view.findViewById(R.id.btn_fragment_backup_restore_backup);
        btn_backup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BackupDialog backupDialog=new BackupDialog(getContext());
                backupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                backupDialog.show();
            }
        });
        btn_restore=(Button) view.findViewById(R.id.btn_fragment_backup_restore_restore);
        btn_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestoreDialog restoreDialog=new RestoreDialog(getContext());
                restoreDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                restoreDialog.show();
            }
        });
    }

}
