package com.hsb.daneshjoo.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.activity.SignUpActivity;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.db.SqliteUpdate;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.ProfileJson;
import com.hsb.daneshjoo.services.webservice.request.TokenJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.services.webservice.response.NoteMessage;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 1/20/2017.
 */
public class fragCreateStudent extends android.support.v4.app.Fragment {
    Button btn_save;
    EditText et_name, et_family, et_age, et_university, et_field, et_enteringYear;
    Spinner sp_sex, sp_level;

    View view;
    List<String> list1;
    List<String> list;
    ArrayAdapter<String> adapter;
    LoadingProgress loadingProgress;
    SqliteRepo sqliteRepo;

    SqliteInsert insert;
    int _sex = 0;

    int _level = 0;
    int _enteringYear = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_createstudent, container, false);

        loadingProgress = new LoadingProgress(view.getContext());
        sqliteRepo = new SqliteRepo(view.getContext());

        Initilize();


        sp_level.setPrompt("مقطع تحصیلی");

        list = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.University_Degree)));


        sp_level.setAdapter(SetAdapter(list));
        sp_level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _level = Integer.valueOf(sp_level.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        list1 = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.Sex)));

        sp_sex.setAdapter(SetAdapter(list1));
        sp_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _sex = Integer.valueOf(sp_sex.getSelectedItemPosition());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if (utility.getToken_createstudent().equals("token_update")) {

            set_ValueUpdateView();
        }


        return view;
    }

    private void Initilize() {
        et_name = (EditText) view.findViewById(R.id.et_student_name);
        et_family = (EditText) view.findViewById(R.id.et_student_family);
        et_age = (EditText) view.findViewById(R.id.et_student_entering_age);
        et_university = (EditText) view.findViewById(R.id.et_student_university);
        et_field = (EditText) view.findViewById(R.id.et_student_field);
        et_enteringYear = (EditText) view.findViewById(R.id.et_student_entering_year);

        sp_sex = (Spinner) view.findViewById(R.id.sp_student_sex);
        sp_level = (Spinner) view.findViewById(R.id.sp_student_level);

        btn_save = (Button) view.findViewById(R.id.btn_student_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validation()) {

                    RunApiProfile();

                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onStop() {
        super.onStop();

    }

    private boolean Validation() {
        if (et_name.getText().toString().isEmpty() || et_name.getText().toString().length() <= 0 || et_name.getText().toString().length() > 32) {
            et_name.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_NameNotValid));
        } else if (et_family.getText().toString().isEmpty() || et_family.getText().length() <= 0 || et_family.getText().toString().length() > 32) {
            et_family.setError(getContext().getResources().getString(R.string.CreateStudent_Family));

        } else if (et_field.getText().toString().isEmpty() || et_field.getText().length() <= 0 || et_field.getText().toString().length() > 32) {
            et_field.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_FieldNotValid));

        } else if (et_enteringYear.getText().toString().isEmpty() || et_enteringYear.getText().length() < 4 || et_enteringYear.getText().toString().length() > 4) {
            et_enteringYear.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_YearNotValid));
        } else if (!integerValid(et_enteringYear.getText().toString())) {
            et_enteringYear.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_YearNotValid));
        } else if (et_age.getText().toString().isEmpty() || et_age.getText().length() <= 0 || et_age.getText().toString().length() > 2) {
            et_age.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_AgeNotValid));

        } else if (!integerValid(et_age.getText().toString())) {
            et_age.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_AgeNotValid));

        } else if (et_university.getText().toString().isEmpty() || et_university.getText().length() <= 0 || et_university.getText().toString().length() > 32) {
            et_university.setError(getContext().getResources().getString(R.string.FragCreateStudent_Error_UniversityNotValid));

        } else {
            return true;
        }
        return false;
    }

    private Boolean integerValid(String str) {
        try {
            int num = Integer.parseInt(str);
            // is an integer!
        } catch (NumberFormatException e) {
            // not an integer!
            return false;
        } finally {
            return true;
        }
    }

    private ArrayAdapter<String> SetAdapter(List<String> list) {
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }


    private void RunApiProfile() {


        /**
         * REST API NOTE
         *
         */
        //Check exeist username
        final SqliteRepo repo = new SqliteRepo(view.getContext().getApplicationContext());
        if (repo.Repo_account_Iduser() > 0) {
            insert = new SqliteInsert(view.getContext().getApplicationContext());
            NetStatus netStatus = new NetStatus(view.getContext().getApplicationContext());
            if (netStatus.checkInternetConnection()) {
                // internet connection

                try {

                    // build web service
                    loadingProgress.Show();
                    // generate json
                    ProfileJson profileJson = new ProfileJson(String.valueOf(repo.getIdUser()), et_name.getText().toString(), et_family.getText().toString(), et_field.getText().toString(), String.valueOf(_level), et_enteringYear.getText().toString(), et_age.getText().toString(), String.valueOf(_sex), et_university.getText().toString());
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();


                    StoreClient client = RestClient.getClient().create(StoreClient.class);
                    Call<Messageing> call;

                    if (utility.getToken_createstudent().equals("token_update")) {

                        call = client.posteditprofile(profileJson);
                    } else {
                        call = client.postinsertprofile(profileJson);
                    }

                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                            loadingProgress.Close();
                            if (response.code() == 200) {
                                if (Integer.valueOf(response.body().getSuccessCode().toString()) == 0) {
                                    utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin), view.getContext());
                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 1) {

                                    if (utility.getToken_createstudent().equals("token_update")) {
                                        SqliteUpdate sqliteUpdate = new SqliteUpdate(view.getContext());
                                        sqliteUpdate.Update_student(response.body().getToken(), et_name.getText().toString(), et_family.getText().toString(), et_field.getText().toString(), _level, Integer.valueOf(et_enteringYear.getText().toString()), Integer.valueOf(et_age.getText().toString()), _sex, et_university.getText().toString());
                                    } else {
                                        insert.ADD_student(response.body().getToken(), et_name.getText().toString(), et_family.getText().toString(), et_field.getText().toString(), _level, Integer.valueOf(et_enteringYear.getText().toString()), Integer.valueOf(et_age.getText().toString()), _sex, et_university.getText().toString());

                                        Intent i = new Intent(view.getContext().getApplicationContext(), MainActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }


                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 202) {
                                    utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragCreateStudent_Error_DataRepeat), view.getContext());
                                }
                            } else {
                                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD), getActivity());

                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();
                            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin), getActivity());
                        }
                    });


                } catch (Exception e) {
                    loadingProgress.Close();
                    e.printStackTrace();
                }

            } else {
                // not internet connection
                loadingProgress.Close();
                utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.NOT_INTERNET_CONNECTION), getActivity());

            }


        } else {
            loadingProgress.Close();
            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin), getActivity());
        }

    }


    private void set_ValueUpdateView() {
        Cursor cur = sqliteRepo.Repo_profile();
        if (cur.getCount() > 0) {
            cur.moveToFirst();
            do {

                try {
                    String n = cur.getString(2);
                    et_field.setText(cur.getString(4));
                    et_enteringYear.setText(cur.getString(6));
                    et_university.setText(cur.getString(9));
                    et_age.setText(cur.getString(7));
                    et_name.setText(n);
                    et_family.setText(cur.getString(3));
                    sp_level.setSelection(cur.getInt(5));
                    sp_sex.setSelection(cur.getInt(8));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } while (cur.moveToNext());

        }
    }
}
