package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.reminder.ReminderDialog;
import com.hsb.daneshjoo.cards.reminder.ReminderRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.termreportcard.TermReportCardRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.HelpShowCaseView;

/**
 * Created by hsb on 8/14/2017.
 */

public class fragReportCard extends Fragment {


    View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    FloatingActionButton fab_chart_term_reportcard;
    KeySetting keySetting;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_report_card, container, false);
        Initilize();


        //LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver,new IntentFilter("del_reminder"));
        keySetting = new KeySetting(getContext());

        if (keySetting.getActiveSCVFragReportCard() == 0) {
            helpFloating();
            keySetting.setActiveSCVFragReportCard(1);
        } else if (keySetting.getActiveSCVFragReportCard() == 1) {
            RefreshRecyClerView();
        }

        fab_chart_term_reportcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // stock chart
                android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) view.getContext()).getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.container_body, new fragChartTerm());
                fragmentTransaction.commit();
            }
        });


        return view;
    }

    private void Initilize() {
        fab_chart_term_reportcard = (FloatingActionButton) view.findViewById(R.id.fab_chart_term_report_card);
    }


    public void RefreshRecyClerView() {


        DbToArrayList dbToArrayList = new DbToArrayList((Activity) getContext());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.report_card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new TermReportCardRecyclerViewAdapter(dbToArrayList.getDataTermReportCard(), view.getContext(), getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            RefreshRecyClerView();
        }

    };

    public void helpFloating() {

        ViewTarget viewTarget = new ViewTarget(view.findViewById(R.id.fab_chart_term_report_card));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);

        int margin = ((Number) (getActivity().getResources().getDisplayMetrics().density * 80)).intValue();
        lps.setMargins(margin, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(getContext().getResources().getString(R.string.FragReportCardFab_ShowCase_title))
                .setContentText(getContext().getResources().getString(R.string.FragReportCardFab_ShowCase_desc))
                .setStyle(R.style.BlueShowcaseTheme)
                .build();
        sv.setButtonText(getContext().getResources().getString(R.string.FragReportCardFab_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {
                RefreshRecyClerView();
            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i("onDestroy","TRUE");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("onDestroy","TRUE");
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.i("onDestroy","TRUE");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("onDestroy","TRUE");
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i("onPause","TRUE");
    }
}
