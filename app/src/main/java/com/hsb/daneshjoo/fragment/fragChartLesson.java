package com.hsb.daneshjoo.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.chart.LessonChartItem;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb on 8/18/2017.
 */

public class fragChartLesson extends Fragment {

    View view;
    BarChart chart;
    //FloatingActionButton menu_chat;
    private List<Float> gradelist;
    SqliteRepo repo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lesson_chart,container,false);

        Initilize();
        setDataChart();
        return view;
    }


    private void Initilize(){
        //menu_chat=(FloatingActionButton) view.findViewById(R.id.menu_type_chart_lesson);
        chart= (BarChart) view.findViewById(R.id.chart_lesson);
         repo=new SqliteRepo(view.getContext());
    }


    private void setDataChart(){

        XAxis xAxis=new XAxis();
        xAxis.setValues(getXAxisValues());

        xAxis.setGridColor(ColorTemplate.getHoloBlue());
        BarData data = new BarData(xAxis.getValues(), getDataSet());
        chart.setData(data);
        chart.setDescription("چارت");
        chart.animateXY(2000, 2000);
        chart.invalidate();
    }






    private BarDataSet getDataSet() {


        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        List<Float> gradelist;

        gradelist= repo.getListGrade(utility.getIdTerm());
        for (int i=0;i<gradelist.size();i++){
            valueSet1.add(new BarEntry(gradelist.get(i),i));
        }
        BarDataSet barDataSet2 = new BarDataSet(valueSet1, getContext().getResources().getString(R.string.FragChartLesson));
        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
        return barDataSet2;
    }

    private List<String> getXAxisValues() {
        List<String> xAxis ;

        xAxis= repo.getListNameLesson(utility.getIdTerm());
        return xAxis;
    }
}
