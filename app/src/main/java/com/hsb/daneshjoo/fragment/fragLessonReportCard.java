package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.lessonreportcard.LessonReportCardRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.termreportcard.TermReportCardRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.HelpShowCaseView;
import com.hsb.daneshjoo.utilities.utility;

/**
 * Created by hsb on 8/14/2017.
 */

public class fragLessonReportCard extends Fragment {


    View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    FloatingActionButton fab_chart_lesson_reportcard;
    KeySetting keySetting;
    private static int flag=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lesson_report_card, container, false);

        RefreshRecyClerView();

        //LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver,new IntentFilter("del_reminder"));

        Initilize();

        fab_chart_lesson_reportcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // stock chart

                android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) view.getContext()).getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new fragChartLesson());
                fragmentTransaction.commit();

            }
        });

        return view;
    }

    private void Initilize() {
        fab_chart_lesson_reportcard = (FloatingActionButton) view.findViewById(R.id.fab_chart_lesson_report_card);
    }

    public void RefreshRecyClerView() {

        DbToArrayList dbToArrayList = new DbToArrayList((Activity) getContext());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.lesson_report_card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new LessonReportCardRecyclerViewAdapter(dbToArrayList.getDataLessonReportCard(utility.getIdTerm()), view.getContext(), getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            RefreshRecyClerView();
        }

    };



    public void helpLessonCardView() {


        ViewTarget viewTarget = new ViewTarget(view.findViewById(R.id.lesson_report_card_recycler_view));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        int margin = ((Number) (getActivity().getResources().getDisplayMetrics().density * 40)).intValue();
        lps.setMargins(margin, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(getContext().getResources().getString(R.string.FragLessonReportCard_ShowCase_title))
                .setContentText(getContext().getResources().getString(R.string.FragLessonReportCard_ShowCase_desc))
                .setStyle(R.style.GrayShowcaseTheme)
                .build();
        sv.setButtonText(getContext().getResources().getString(R.string.FragLessonReportCard_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });

    }
}
