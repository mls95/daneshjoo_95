package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.phone.PhoneInfo;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;
import com.hsb.daneshjoo.services.webservice.request.SignUpJson;

import java.math.BigInteger;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 1/20/2017.
 */
public class fragSignUp extends android.support.v4.app.Fragment {


    EditText et_username, et_email, et_password;
    Button btn_signup, btn_signin, btn_visible;
    View view;
    SqliteInsert insert;
    LoadingProgress loadingProgress;
    Context context;
    CheckBox ch_visible_password;

    PhoneInfo phoneInfo;

    public void setContext(Activity activity) {
        context = activity;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_signup, container, false);
        loadingProgress = new LoadingProgress(view.getContext());
        Intilize();

        phoneInfo=new PhoneInfo(getContext());


        // set on click button btn_signup
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {



                if (Validation()) {
                    //Check exeist username
                    SqliteRepo repo = new SqliteRepo(container.getContext().getApplicationContext());
                    if (repo.Repo_account_username(et_username.getText().toString())) {
                        insert = new SqliteInsert(container.getContext().getApplicationContext());
                        NetStatus netStatus = new NetStatus(container.getContext().getApplicationContext());
                        if (netStatus.checkInternetConnection()) {
                            // internet connection

                            try {

                                loadingProgress.Show();
                                // build web service

                                // generate json

                                UUID uuid = UUID.randomUUID();
                                final Integer id_phone_info = Math.abs(uuid.hashCode());

                                SignUpJson signUpJson = new SignUpJson(String.valueOf(id_phone_info),phoneInfo.Get_VersionAndroid(),phoneInfo.Get_NamePhone(),phoneInfo.Get_ModelPhone(),phoneInfo.Get_Manufacturer(),phoneInfo.Get_Domain(),utility.encrypt(et_username.getText().toString()),et_username.getText().toString(), et_email.getText().toString(), et_password.getText().toString());


                                StoreClient client = RestClient.getClient().create(StoreClient.class);
                                Call<Messageing> call = client.postcreateaccount(signUpJson);
                                call.enqueue(new Callback<Messageing>() {
                                    @Override
                                    public void onResponse(Call<Messageing> call, Response<Messageing> response) {


                                        loadingProgress.Close();
                                        if (response.code() == 200) {
                                            if (Integer.valueOf(response.body().getSuccessCode().toString()) == 0) {
                                                utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignUp_Message_CheckInformation), view.getContext());
                                            } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 1) {
                                                if (insert.ADD_account(et_username.getText().toString(), et_email.getText().toString(), et_password.getText().toString(), response.body().getToken())) {

                                                    utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragSignUp_Message_AccountCreated), view.getContext());

                                                    utility.setToken_createstudent("token_create");

                                                    android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                    transaction.replace(R.id.fragmentParentViewGroup, new fragCreateStudent());
                                                    //transaction.addToBackStack(null);
                                                    transaction.commit();
                                                } else {
                                                    utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignUp_Message_AccountNotCreated), view.getContext());
                                                }

                                            } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 201) {
                                                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragSignUp_Message_UsernameAlreadyRegistered), view.getContext());

                                            }
                                        } else {
                                            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD), view.getContext());

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Messageing> call, Throwable t) {
                                        // Log error here since request failed

                                        loadingProgress.Close();
                                        utility.dialog(view.getContext().getResources().getString(R.string.Message),view. getContext().getResources().getString(R.string.FragSignUp_Message_AccountNotCreated), view.getContext());
                                    }
                                });


                            } catch (Exception e) {
                                loadingProgress.Close();
                                e.printStackTrace();
                            }

                        } else {
                            // not internet connection
                            loadingProgress.Close();
                            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.NOT_INTERNET_CONNECTION), view.getContext());

                        }


                    } else {
                        loadingProgress.Close();
                        utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragSignUp_Message_UsernameAlreadyRegistered) , view.getContext());
                    }

                }
            }
        });

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit, R.anim.enter, R.anim.exit);
                transaction.replace(R.id.fragmentParentViewGroup, new fragSignin());
               // transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        ch_visible_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean et_pass_visibility) {
                if (et_pass_visibility == false) {
                    et_pass_visibility = true;

                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    et_pass_visibility = false;
                    et_password.setTransformationMethod(null);
                }
            }
        });


        return view;
    }

    private void Intilize() {
        et_username = (EditText) view.findViewById(R.id.et_signup_username);
        et_email = (EditText) view.findViewById(R.id.et_signup_email);
        et_password = (EditText) view.findViewById(R.id.et_signup_password);

        btn_signup = (Button) view.findViewById(R.id.btn_signup_signup);
        btn_signin = (Button) view.findViewById(R.id.btn_signup_login);
/*
        btn_visible=(Button) view.findViewById(R.id.btn_visible);
*/
        ch_visible_password = (CheckBox) view.findViewById(R.id.ch_visible_password_up);

    }


    private boolean Validation() {
        String userString = et_username.getText().toString();
        String passString = et_password.getText().toString();
        Pattern pattern = Pattern.compile("[A-Za-z0-9_]+");

        boolean valid = (userString != null) && pattern.matcher(userString).matches();

        if(!valid){
            et_username.setError(getContext().getResources().getString(R.string.FragSignUp_Message_UseCharacter));
        }
        else if(userString.charAt(0)=='0' || userString.charAt(0)==' ')
        {
            et_username.setError(getContext().getResources().getString(R.string.FragSignUp_Message_FirstCharacterIsInvalid));
        }
        else if (et_username.getText().toString().isEmpty() || et_username.getText().toString().length() <= 0) {
            et_username.setError(getContext().getResources().getString(R.string.FragSignUp_Message_UserNameNotValid));
        }
         else if (et_username.getText().toString().length() < 8 || et_username.getText().toString().length() > 32) {
            et_username.setError(getContext().getResources().getString(R.string.FragSignUp_Message_UsernameBetweenCharacters));
        }

        else if (!isEmailValid(et_email.getText().toString())) {
            //utility.dialog("پیام","ایمیل معتبر نیست",view.getContext());
            et_email.setError(getContext().getResources().getString(R.string.FragSignUp_Message_EmailNotValid));
        }

        else if (et_password.getText().toString().isEmpty() || et_password.getText().length() <= 0) {
            et_password.setError(getContext().getResources().getString(R.string.FragSignUp_Message_PasswordNotValid));
        }else if(passString.charAt(0)=='0' || passString.charAt(0)==' '){
            et_password.setError(getContext().getResources().getString(R.string.FragSignUp_Message_FirstCharacterIsInvalid));
        }
        else if (et_password.getText().length() < 8 || et_password.getText().length() <= 0) {
            et_password.setError(getContext().getResources().getString(R.string.FragSignUp_Message_PassBetweenCharacters));
        } else {
            return true;
        }
        return false;
    }

    public boolean isEmailValid(String email) {
        String regExpn = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);


        if (matcher.matches()) {
            return true;
        } else {

            return false;
        }
    }
}
