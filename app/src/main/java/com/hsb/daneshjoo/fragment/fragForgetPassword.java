package com.hsb.daneshjoo.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.ForgetPassword;
import com.hsb.daneshjoo.services.webservice.request.LoginJson;
import com.hsb.daneshjoo.services.webservice.response.LoginMessage;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 4/21/2017.
 */

public class fragForgetPassword extends android.support.v4.app.Fragment {

    EditText et_email;
    Button btn_back,btn_send;
    LoadingProgress loadingProgress;
    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgetpassword, container, false);
        loadingProgress=new LoadingProgress(view.getContext());
             Initilize();
        return view;
    }


    private void Initilize() {

        et_email = (EditText) view.findViewById(R.id.et_forgetpassword_email);
        btn_back = (Button) view.findViewById(R.id.btn_forgetpassword_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction transaction=getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.replace(R.id.fragmentParentViewGroup, new fragSignin());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btn_send = (Button) view.findViewById(R.id.btn_forgetpassword_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (Validation()){
                    SqliteRepo repo=new SqliteRepo(view.getContext());
                    if (Validation()) {
                        NetStatus netStatus=new NetStatus(view.getContext());
                        if (netStatus.checkInternetConnection()){
                            // internet connection

                            try {

                                // build web service


                                // progress view

                                loadingProgress.Show();


                                ForgetPassword forgetPassword=new ForgetPassword(et_email.getText().toString());
                                // generate json

                                Gson gson = new GsonBuilder()
                                        .setLenient()
                                        .create();


                                String json=gson.toJson(forgetPassword);
                                StoreClient client= RestClient.getClient().create(StoreClient.class);
                                Call<Messageing> call = client.postforgetpassword(forgetPassword);
                                call.enqueue(new Callback<Messageing>() {
                                    @Override
                                    public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                                        loadingProgress.Close();
                                        if (response.code()==200){
                                            if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                                utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragForgetPassword_Dialog_Message_NoEmail),view.getContext());
                                            }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){
                                                utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragForgetPassword_Dialog_Message_DataSentToEmail),view.getContext());
                                            }else if(Integer.valueOf(response.body().getSuccessCode().toString())==202){
                                                utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignUp_Message_EmailNotValid),view.getContext());

                                            }
                                        }else{
                                            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD),view.getContext());

                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<Messageing> call, Throwable t) {
                                        // Log error here since request failed
                                        loadingProgress.Close();
                                        utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin),view.getContext());
                                    }
                                });




                            }catch (Exception e){

                                e.printStackTrace();
                            }

                        }else{
                            // not internet connection
                            loadingProgress.Close();
                            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NOT_INTERNET_CONNECTION),view.getContext());

                        }


                    }

                }
            }
        });

    }


    private boolean Validation(){
        if(et_email.getText().toString().isEmpty() || et_email.getText().toString().length()<=0){
            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragForgetPassword_Dialog_Message_EnterEmail),view.getContext());
        }else if(!isEmailValid(et_email.getText().toString())){
            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragForgetPassword_Dialog_Message_FormatEmail),view.getContext());
        }        else{
            return true;
        }
        return false;
    }


    public boolean isEmailValid(String email)
    {
        String regExpn ="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);




        if(matcher.matches()) {
            return true;
        }else {
            return false;
        }
    }
}
