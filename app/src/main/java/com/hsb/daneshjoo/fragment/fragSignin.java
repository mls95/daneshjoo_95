package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.alirezaafkar.sundatepicker.interfaces.DateSetListener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.PersianDateDialog;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.activity.SignUpActivity;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.LoginJson;
import com.hsb.daneshjoo.services.webservice.request.SignUpJson;
import com.hsb.daneshjoo.services.webservice.response.LoginMessage;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;
import com.alirezaafkar.sundatepicker.*;

import java.io.IOException;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 1/20/2017.
 */
public class fragSignin extends android.support.v4.app.Fragment {
    View view;
    SqliteInsert insert;

    Button btn_signup,btn_signin,btn_forget;
    EditText et_username,et_password;
    LoadingProgress loadingProgress;
    CheckBox ch_visible_password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_signin,container,false);

        loadingProgress=new LoadingProgress(view.getContext());
        Intilize();




        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.replace(R.id.fragmentParentViewGroup, new fragSignUp());
                // transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        btn_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.replace(R.id.fragmentParentViewGroup, new fragForgetPassword());
                //transaction.addToBackStack(null);
                transaction.commit();


            }
        });


        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                SqliteRepo repo=new SqliteRepo(container.getContext().getApplicationContext());
                if (Validation()) {
                    NetStatus netStatus=new NetStatus(container.getContext().getApplicationContext());
                    if (netStatus.checkInternetConnection()){
                        // internet connection

                        try {

                            // build web service


                            // progress view

                            loadingProgress.Show();


                            // generate json
                            LoginJson loginJson=new LoginJson(et_username.getText().toString(),et_password.getText().toString());
                            Gson gson = new GsonBuilder()
                                    .setLenient()
                                    .create();




                            StoreClient client= RestClient.getClient().create(StoreClient.class);
                            Call<LoginMessage> call = client.postloginaccount(loginJson);
                            call.enqueue(new Callback<LoginMessage>() {
                                @Override
                                public void onResponse(Call<LoginMessage> call, Response<LoginMessage> response) {


                                    loadingProgress.Close();
                                    if (response.code()==200){

                                        if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                            utility.dialog(getContext().getResources().getString(R.string.Message) ,getContext().getResources().getString(R.string.FragSignin_Message_CheckUserNameAndPassword),view.getContext());
                                        }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){
                                            insert=new SqliteInsert(container.getContext().getApplicationContext());
                                            if(insert.ADD_account(response.body().getUserName(),response.body().getEmail(),response.body().getPassword(),response.body().getToken())) {
                                                if(!response.body().getProfileJson().getName().equals("")) {

                                                    android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                    transaction.replace(R.id.fragmentParentViewGroup, new fragLoadData());
                                                    //transaction.addToBackStack(null);
                                                    transaction.commit();


                                                }else{
                                                    // not profile
                                                    utility.setToken_createstudent("token_create");

                                                    android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) view.getContext()).getSupportFragmentManager();
                                                    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                    // fragmentTransaction.addToBackStack(null);
                                                    fragmentTransaction.replace(R.id.fragmentParentViewGroup,new fragCreateStudent());
                                                    fragmentTransaction.commit();
                                                }


                                            }else{
                                                utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin),view.getContext());
                                            }

                                        }else if(Integer.valueOf(response.body().getSuccessCode().toString())==202){
                                            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_UserNameOrPasswordIsNotCorrect),view.getContext());

                                        }
                                    }else{
                                        utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD),view.getContext());

                                    }


                                }

                                @Override
                                public void onFailure(Call<LoginMessage> call, Throwable t) {
                                    // Log error here since request failed
                                    loadingProgress.Close();
                                    utility.dialog(view.getContext().getResources().getString(R.string.Message),view.getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin),view.getContext());
                                }
                            });




                        }catch (Exception e){

                            e.printStackTrace();
                        }

                    }else{
                        // not internet connection
                        loadingProgress.Close();
                        utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.NOT_INTERNET_CONNECTION),view.getContext());

                    }

                }




            }
        });
        ch_visible_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean et_pass_visibility) {
                if (et_pass_visibility==false) {
                    et_pass_visibility=true;
                    //et_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    et_pass_visibility=false;
                    //et_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    et_password.setTransformationMethod(null);
                }
            }
        } );
        return view;
    }



    private boolean Validation(){
        if(et_username.getText().toString().isEmpty() || et_username.getText().toString().length()<=0){
            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_UserName),view.getContext());
        }else if(et_password.getText().toString().isEmpty() || et_password.getText().length()<=0){
            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_Password),view.getContext());
        }else{
            return true;
        }
        return false;
    }

    private void Intilize() {

        et_username=(EditText) view.findViewById(R.id.et_signin_username);
        et_password=(EditText) view.findViewById(R.id.et_signin_password);

        btn_signup = (Button) view.findViewById(R.id.btn_signin_signup);
        btn_signin = (Button) view.findViewById(R.id.btn_signin_signin);
        btn_forget = (Button) view.findViewById(R.id.btn_signin_forget);
        ch_visible_password=(CheckBox)view.findViewById(R.id.ch_visible_password_in);
    }




}