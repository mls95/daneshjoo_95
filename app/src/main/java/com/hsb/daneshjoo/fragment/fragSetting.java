package com.hsb.daneshjoo.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.SignUpActivity;
import com.hsb.daneshjoo.db.SqliteDelete;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.settings.LanguageDialog;
import com.hsb.daneshjoo.utilities.utility;

/**
 * Created by hsb-pc on 3/3/2017.
 */
public class
fragSetting extends Fragment {

    CheckBox ch_notif, ch_syncdata, ch_newversion, ch_class_phonevibration, ch_exam_phonevibration;
    TextView tv_language, tv_backupandrestore, tv_aboutme, tv_contactus, tv_logout, tv_editaccount;
    KeySetting keySetting;
    LinearLayout liner_SyncServer, liner_language, liner_newVersion,
            liner_phoneVibration, liner_SaveDataOffline, liner_VibraNotification,
            liner_BackupRestor, liner_ContactUs, liner_Ratting, liner_AboutMe,
            liner_editaccount, liner_logOut;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_setting, container, false);
        keySetting = new KeySetting(view.getContext().getApplicationContext());

        InitilaizeLinerLayout();
        Initilaize();
        InitilaizeCheckBox();

        return view;
    }

    private void InitilaizeLinerLayout() {
        liner_SyncServer = (LinearLayout) view.findViewById(R.id.liner_frag_setting_SyncServer);
        liner_language = (LinearLayout) view.findViewById(R.id.liner_frag_setting_Language);
        liner_newVersion = (LinearLayout) view.findViewById(R.id.liner_frag_setting_NewVersion);
        liner_phoneVibration = (LinearLayout) view.findViewById(R.id.liner_frag_setting_phoneVibration);
        liner_SaveDataOffline = (LinearLayout) view.findViewById(R.id.liner_frag_setting_SaveDataOffline);
        liner_VibraNotification = (LinearLayout) view.findViewById(R.id.liner_frag_setting_VibraNotification);
        liner_BackupRestor = (LinearLayout) view.findViewById(R.id.liner_frag_setting_BackupRestor);
        liner_ContactUs = (LinearLayout) view.findViewById(R.id.liner_frag_setting_ContactUs);
        liner_Ratting = (LinearLayout) view.findViewById(R.id.liner_frag_setting_Ratting);
        liner_AboutMe = (LinearLayout) view.findViewById(R.id.liner_frag_setting_AboutMe);
        liner_editaccount = (LinearLayout) view.findViewById(R.id.liner_frag_setting_editaccount);
        liner_logOut = (LinearLayout) view.findViewById(R.id.liner_frag_setting_Logout);
    }

    private void Initilaize() {
        tv_aboutme = (TextView) view.findViewById(R.id.tv_frag_setting_AboutMe);
        tv_logout = (TextView) view.findViewById(R.id.tv_frag_setting_Logout);
        liner_logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragSetting_Dialog_Message_LogOutAccount), getContext().getResources().getString(R.string.Dialog_Message_Yes), getContext().getResources().getString(R.string.Dialog_Message_No), "token_logout", view.getContext(), getActivity());

            }
        });
        liner_AboutMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = ((AppCompatActivity) getContext()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new fragAboutMe());
                fragmentTransaction.commit();
            }
        });
        tv_backupandrestore = (TextView) view.findViewById(R.id.tv_frag_setting_BackupRestor);
        liner_BackupRestor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = ((AppCompatActivity) getContext()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new fragBackupAndRestore());
                fragmentTransaction.commit();
            }
        });


        tv_language = (TextView) view.findViewById(R.id.tv_frag_setting_Languge);
        liner_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LanguageDialog languageDialog = new LanguageDialog(getContext());
                languageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                languageDialog.show();
            }
        });
        ch_notif = (CheckBox) view.findViewById(R.id.checkbox_frag_setting_VibraNotification);
        liner_VibraNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ch_notif.isChecked()) {
                    ch_notif.setChecked(true);
                    keySetting.setVibration_Notification(true);

                } else {
                    ch_notif.setChecked(false);
                    keySetting.setVibration_Notification(false);

                }
            }
        });

        ch_syncdata = (CheckBox) view.findViewById(R.id.checkbox_frag_setting_SyncServer);
        liner_SyncServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ch_syncdata.isChecked()) {
                    ch_syncdata.setChecked(true);
                    keySetting.setSync_App(true);

                } else {
                    ch_syncdata.setChecked(false);
                    keySetting.setSync_App(false);

                }
            }
        });

        ch_newversion = (CheckBox) view.findViewById(R.id.checkbox_frag_setting_NewVersion);
        liner_newVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ch_newversion.isChecked()) {
                    ch_newversion.setChecked(true);
                    keySetting.setNew_Version(true);

                } else {
                    ch_newversion.setChecked(false);
                    keySetting.setNew_Version(false);

                }
            }
        });

        tv_contactus = (TextView) view.findViewById(R.id.tv_frag_setting_ContactUs);
        liner_ContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "h.myapps", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, getContext().getResources().getString(R.string.FragSetting_Dialog_Message_Offers));
                intent.putExtra(Intent.EXTRA_TEXT, "");

                startActivity(Intent.createChooser(intent, "Send Email..."));
            }
        });


        ch_class_phonevibration = (CheckBox) view.findViewById(R.id.checkbox_frag_setting_class_phoneVibration);
        ch_class_phonevibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_class_phonevibration.setChecked(true);
                    keySetting.setClassPhoneVibration(true);

                } else {
                    ch_class_phonevibration.setChecked(false);
                    keySetting.setClassPhoneVibration(false);

                }
            }
        });


        ch_exam_phonevibration = (CheckBox) view.findViewById(R.id.checkbox_frag_setting_Exam_phoneVibration);
        ch_exam_phonevibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_exam_phonevibration.setChecked(true);
                    keySetting.setExamPhoneVibration(true);

                } else {
                    ch_exam_phonevibration.setChecked(false);
                    keySetting.setExamPhoneVibration(false);

                }
            }
        });

        tv_editaccount = (TextView) view.findViewById(R.id.tv_frag_setting_editaccount);
        liner_editaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                utility.setToken_createstudent("token_update");

                android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) view.getContext()).getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new fragCreateStudent());
                fragmentTransaction.commit();
            }
        });

    }


    private void InitilaizeCheckBox() {
        if (keySetting.getNew_Version()) {
            ch_newversion.setChecked(true);
        } else {
            ch_newversion.setChecked(false);
        }

        if (keySetting.getSync_App()) {
            ch_syncdata.setChecked(true);
        } else {
            ch_syncdata.setChecked(false);
        }

        if (keySetting.getVibration_Notification()) {
            ch_notif.setChecked(true);
        } else {
            ch_notif.setChecked(false);
        }

        if (keySetting.getExamPhoneVibration()) {
            ch_class_phonevibration.setChecked(true);
        } else {
            ch_class_phonevibration.setChecked(false);
        }

        if (keySetting.getClassPhoneVibration()) {
            ch_exam_phonevibration.setChecked(true);
        } else {
            ch_exam_phonevibration.setChecked(false);
        }

    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


}
