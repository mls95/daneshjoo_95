package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.term.TermDialog;
import com.hsb.daneshjoo.cards.term.TermRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.settings.KeySetting;

/**
 * Created by hsb-pc on 1/27/2017.
 */
public class fragTerm extends android.support.v4.app.Fragment {
    View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    FloatingActionButton fab_newterm;
    Context context;
    KeySetting keySetting;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_term, container, false);



        keySetting = new KeySetting(getContext());

        if (keySetting.getActiveSCVFragTerm() == 0) {
            helpFloating();
            keySetting.setActiveSCVFragTerm(1);
        } else if (keySetting.getActiveSCVFragTerm() == 1) {
            RefreshRecyClerView();

        }


        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver, new IntentFilter("del_term"));
        Initilize();


        fab_newterm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TermDialog cdd = new TermDialog(getActivity());
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cdd.show();

            }
        });


        return view;
    }

    private void Initilize() {
        fab_newterm = (FloatingActionButton) view.findViewById(R.id.fab_newterm);
    }

    public void RefreshRecyClerView() {
/*        if (flag == true) {
            helpTermCardView();
        }*/
        DbToArrayList dbToArrayList = new DbToArrayList((Activity) getContext());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.term_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new TermRecyclerViewAdapter(dbToArrayList.getDataTerm(), view.getContext(), getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            RefreshRecyClerView();
        }

    };

    public void helpFloating() {
        delay();

        ViewTarget viewTarget = new ViewTarget(view.findViewById(R.id.fab_newterm));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);
        int margin = ((Number) (getResources().getDisplayMetrics().density * 80)).intValue();
        lps.setMargins(margin, margin, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(getContext().getResources().getString(R.string.FragTerm_ShowCase_title))
                .setContentText(getContext().getResources().getString(R.string.FragTerm_ShowCase_desc))
                .setStyle(R.style.CustomShowcaseTheme2)
                .build();
        sv.setButtonText(getContext().getResources().getString(R.string.FragTerm_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {

            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

                RefreshRecyClerView();

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });
    }


    public void delay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 1500);
    }
}
