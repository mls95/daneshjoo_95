package com.hsb.daneshjoo.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.utilities.HelpShowCaseView;

/**
 * Created by hsb-pc on 3/13/2017.
 */
public class fragAboutMe extends Fragment {

    View view;
    TextView tvVersion,tv_gmail,tv_website;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_about, container, false);

        tvVersion = (TextView) view.findViewById(R.id.et_fragment_about_version);
        tv_gmail=(TextView) view.findViewById(R.id.et_fragment_about_gmail);
        tv_website=(TextView) view.findViewById(R.id.et_fragment_about_visitus);
        tv_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "h.myapps", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, getContext().getResources().getString(R.string.FragSetting_Dialog_Message_Offers));
                intent.putExtra(Intent.EXTRA_TEXT, "");

                startActivity(Intent.createChooser(intent, "Send Email..."));
            }
        });

        tv_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://smartstudent.skyf.ir/?i=1";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        PackageManager manager = getActivity().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getActivity().getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = info.versionName;
        tvVersion.setText(getResources().getString(R.string.App_Version) + " " + version);


        return view;

    }


    public void helpAbout() {

        ViewTarget viewTarget = new ViewTarget(view.findViewById(R.id.et_fragment_about_contactus));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);

        int margin = ((Number) (getActivity().getResources().getDisplayMetrics().density * 80)).intValue();
        lps.setMargins(margin, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(getContext().getResources().getString(R.string.FragReminder_ShowCase_title))
                .setContentText(getContext().getResources().getString(R.string.FragReminder_ShowCase_desc))
                .setStyle(R.style.MIDNIGHTShowcaseTheme)
                .build();
        sv.setButtonText(getContext().getResources().getString(R.string.FragReminder_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });
    }

}