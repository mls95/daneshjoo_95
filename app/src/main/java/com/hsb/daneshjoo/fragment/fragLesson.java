package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.lesson.LessonDialog;
import com.hsb.daneshjoo.cards.term.TermDialog;
import com.hsb.daneshjoo.cards.lesson.LessonItem;
import com.hsb.daneshjoo.cards.lesson.LessonRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.HelpShowCaseView;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 1/27/2017.
 */
public class fragLesson extends android.support.v4.app.Fragment {
    View view;
    int ID_TERM;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    FloatingActionButton fab_newterm;
    KeySetting keySetting;

    public void setID_TERM(int ID_TERM) {
        this.ID_TERM = ID_TERM;
    }

    public int getID_TERM() {
        return this.ID_TERM;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lesson, container, false);

        RefreshRecyClerView();

        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver, new IntentFilter("del_lesson"));
        keySetting = new KeySetting(getContext());

        if (keySetting.getActiveSCVFragLesson() == 0) {
            helpFloating();
            keySetting.setActiveSCVFragLesson(1);
        }

        Initilize();

        fab_newterm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonDialog cdd = new LessonDialog(getActivity(), getID_TERM());
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cdd.show();
            }
        });


        return view;
    }

    private void Initilize() {
        fab_newterm = (FloatingActionButton) view.findViewById(R.id.fab_newlesson);
    }


    public void RefreshRecyClerView() {

        DbToArrayList dbToArrayList = new DbToArrayList((Activity) getContext());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.lesson_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new LessonRecyclerViewAdapter(dbToArrayList.getDataLesson(), getActivity(), getActivity());
        mRecyclerView.setAdapter(mAdapter);

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            RefreshRecyClerView();
        }

    };

    public void helpFloating() {


        ViewTarget viewTarget = new ViewTarget(view.findViewById(R.id.fab_newlesson));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);


        int margin = ((Number) (getActivity().getResources().getDisplayMetrics().density * 80)).intValue();
        lps.setMargins(margin, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(getContext().getResources().getString(R.string.FragLesson_ShowCase_title))
                .setContentText(getContext().getResources().getString(R.string.FragLesson_ShowCase_desc))
                .setStyle(R.style.OrangeShowcaseTheme)
                .build();
        sv.setButtonText(getContext().getResources().getString(R.string.FragLesson_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });

    }

}
