package com.hsb.daneshjoo.fragment;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb on 8/18/2017.
 */

public class fragChartTerm extends Fragment {

    View view;
    BarChart chart;
    //FloatingActionButton menu_chat;
    SqliteRepo repo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_term_chart,container,false);

        Initilize();
        setDataChart();
        return view;
    }


    private void Initilize(){
       // menu_chat=(FloatingActionButton) view.findViewById(R.id.menu_type_chart_term);
        chart= (BarChart) view.findViewById(R.id.chart_term);
        repo=new SqliteRepo(view.getContext());
    }


    private void setDataChart(){

        BarData data = new BarData(getXAxisValues(), getDataSet());

        chart.setData(data);
        chart.setDuplicateParentStateEnabled(false);
        chart.setDescription(getContext().getResources().getString(R.string.FragChart));
        chart.animateXY(2000, 2000);
        chart.invalidate();



    }





    /*private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("JAN");
        xAxis.add("FEB");
        xAxis.add("MAR");
        xAxis.add("APR");
        xAxis.add("MAY");
        xAxis.add("JUN");
        return xAxis;
    }*/


    private int getSumUnit(int term){
        Cursor cursor1 =repo.Repo_lesson(term);
        int total_semester=0;
        // total semester in table Lesson
        if(cursor1.getCount()>0){
            cursor1.moveToFirst();
            do{
                total_semester =total_semester +cursor1.getInt(5);
            }while (cursor1.moveToNext());
        }
        return total_semester;
    }


    private BarDataSet getDataSet() {

        float avarage, sumGrade, sumUnit;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();

        List<String> termlist;

        termlist= repo.getListTerm();
        for (int i=0;i<termlist.size();i++){

            sumGrade = repo.getTotalgradelesson(Integer.valueOf(termlist.get(i)));
            sumUnit = (float) getSumUnit(Integer.valueOf(termlist.get(i)));
            avarage = sumGrade / sumUnit;

            avarage = (float) (Math.round(avarage * 100) / 100.0d);

            valueSet1.add(new BarEntry(avarage,i));
        }



        BarDataSet barDataSet2 = new BarDataSet(valueSet1, getContext().getResources().getString(R.string.FragChartTerm));

        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
        return barDataSet2;
    }

    private List<String> getXAxisValues() {
        List<String> xAxis ;
        xAxis= repo.getListTerm();

        return xAxis;
    }
}
