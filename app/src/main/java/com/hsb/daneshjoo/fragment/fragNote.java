package com.hsb.daneshjoo.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.codetroopers.betterpickers.Utils;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.lesson.LessonRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.note.NoteDialog;
import com.hsb.daneshjoo.cards.note.NoteItem;
import com.hsb.daneshjoo.cards.note.NoteRecyclerViewAdpter;
import com.hsb.daneshjoo.cards.reminder.ReminderDialog;
import com.hsb.daneshjoo.cards.reminder.ReminderRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.reminder.Reminderitem;
import com.hsb.daneshjoo.cards.setterm.SetTermDialog;
import com.hsb.daneshjoo.cards.term.TermItem;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.HelpShowCaseView;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 2/14/2017.
 */
public class fragNote extends Fragment {

    View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    FloatingActionButton fab_newnote;
    KeySetting keySetting;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_note, container, false);

        RefreshRecyClerView();

        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver, new IntentFilter("del_note"));

        keySetting = new KeySetting(getContext());

        if (keySetting.getActiveSCVFragNote() == 0) {
            helpFloating();
            keySetting.setActiveSCVFragNote(1);
        }

        Initilize();

        fab_newnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SqliteRepo sqliteRepo = new SqliteRepo(view.getContext());
                if (sqliteRepo.getCountLesson() > 0) {
                    NoteDialog noteDialog = new NoteDialog(getActivity());
                    noteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    noteDialog.show();
                } else {
                    utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.FragReminder_Dialog_Message_NoLesson), getActivity());
                }

            }
        });
        return view;
    }

    private void Initilize() {
        fab_newnote = (FloatingActionButton) view.findViewById(R.id.fab_newnote);
    }

    public void RefreshRecyClerView() {

        DbToArrayList dbToArrayList = new DbToArrayList((Activity) getContext());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.note_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new NoteRecyclerViewAdpter(dbToArrayList.getDataNote(), view.getContext(), getActivity());
        mRecyclerView.setAdapter(mAdapter);

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            RefreshRecyClerView();
        }

    };

    public void helpFloating() {

        ViewTarget viewTarget = new ViewTarget(view.findViewById(R.id.fab_newnote));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);


        int margin = ((Number) (getActivity().getResources().getDisplayMetrics().density * 40)).intValue();
        lps.setMargins(margin*2, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(getContext().getResources().getString(R.string.FragNote_ShowCase_title))
                .setContentText(getContext().getResources().getString(R.string.FragNote_ShowCase_desc))
                .setStyle(R.style.RedShowcaseTheme)
                .build();
        sv.setButtonText(getContext().getResources().getString(R.string.FragNote_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });

    }
}
