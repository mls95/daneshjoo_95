package com.hsb.daneshjoo.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;

import com.hsb.daneshjoo.services.webservice.request.TokenJson;

import com.hsb.daneshjoo.services.webservice.response.LessonMessage;
import com.hsb.daneshjoo.services.webservice.response.NoteMessage;
import com.hsb.daneshjoo.services.webservice.response.ProfileMessage;
import com.hsb.daneshjoo.services.webservice.response.ReminderMessage;
import com.hsb.daneshjoo.services.webservice.response.SyncDataMessage;
import com.hsb.daneshjoo.services.webservice.response.TermMessage;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 6/4/2017.
 */

public class fragLoadData extends Fragment {

    View view;
    Button btn_enter;
    Button btn_tryAgain;

    SqliteInsert insert;

    public static boolean _GetTerm =false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_load_data,container,false);
        Initilize();
        LoadDataOfServer();

        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver_OK,new IntentFilter("loadData_ok"));
        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver_FAILD,new IntentFilter("loadData_faild"));
        return view;
    }


    private void Initilize(){
        btn_enter=(Button) view.findViewById(R.id.btn_fragment_load_data_enterhome);
        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(view.getContext().getApplicationContext(), MainActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        btn_tryAgain=(Button) view.findViewById(R.id.btn_fragment_load_data_tryAgain);
        btn_tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    private void LoadDataOfServer(){


        SyncDataWithServer();

    }



    private boolean   SyncDataWithServer(){


        /**
         * RESTFUL API TERM
         */
        //Check exeist username
        final LoadingProgress loadingProgress=new LoadingProgress(view.getContext());
        SqliteRepo repo=new SqliteRepo(view.getContext().getApplicationContext());
        if (repo.Repo_account_Iduser() > 0){
            insert=new SqliteInsert(view.getContext().getApplicationContext());
            NetStatus netStatus=new NetStatus(view.getContext().getApplicationContext());
            if (netStatus.checkInternetConnection()){
                // internet connection

                try {


                    loadingProgress.Show();
                    // build web service

                    // generate json
                    TokenJson tokenJson=new TokenJson(repo.Repo_account_Iduser());
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();


                    StoreClient client= RestClient.getClient().create(StoreClient.class);
                    Call<SyncDataMessage> call = client.posttokendata(tokenJson);
                    call.enqueue(new Callback<SyncDataMessage>() {
                        @Override
                        public void onResponse(Call<SyncDataMessage> call, Response<SyncDataMessage> response) {

                            if (response.code()==200){
                                if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                    utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin),view.getContext());

                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){
                                    Integer id_user =0;
                                    id_user=Integer.valueOf(response.body().getToken().toString());
                                    if (Integer.valueOf(response.body().getToken())>0) {

                                        String name = "";
                                        String family = "";
                                        String field = "";
                                        Integer level = 0;
                                        Integer enteringyear = 0;
                                        Integer age = 0;
                                        Integer sex = 0;
                                        String univercityname = "";


                                        name = response.body().getNmae();
                                        family = response.body().getFamily();
                                        field = response.body().getField();
                                        level = Integer.valueOf(response.body().getLevel());
                                        enteringyear = Integer.valueOf(response.body().getEnteringYear());

                                        age = Integer.valueOf(response.body().getAge());
                                        sex = Integer.valueOf(response.body().getSex());
                                        univercityname = response.body().getUnivercityName();

                                        insert.ADD_student(id_user, name, family, field, level, enteringyear, age, sex, univercityname);
                                    }



                                    Integer id_term =0;
                                    Integer id_id_term =0;
                                    Integer sumunit =0;
                                    Integer datestart =0;
                                    Integer dateend =0;
                                    Integer dateyear =0;

                                    if(Integer.valueOf(response.body().getTerm().get(0).getIdTerm())>0) {
                                        for (int i = 0; i < response.body().getTerm().size(); i++) {


                                            id_term = Integer.valueOf(response.body().getTerm().get(i).getTerm());
                                            id_id_term = Integer.valueOf(response.body().getTerm().get(i).getIdTerm());
                                            sumunit = Integer.valueOf(response.body().getTerm().get(i).getSumUnit());
                                            datestart = Integer.valueOf(response.body().getTerm().get(i).getDateStart());
                                            dateend = Integer.valueOf(response.body().getTerm().get(i).getDateEnd());
                                            dateyear = Integer.valueOf(response.body().getTerm().get(i).getYearDate());
                                            insert.ADD_term(id_user,id_id_term, id_term, dateyear, sumunit, datestart, dateend);
                                        }

                                    }


                                    if(Integer.valueOf(response.body().getLesson().get(0).getIdTerm())>0) {
                                        Integer l_id_term = 0;
                                        Integer id_lesson = 0;
                                        String namelesson = "";
                                        Integer countunit = 0;
                                        String nameteacher = "";
                                        Integer dayclass = 0;
                                        String clockclass = "";
                                        Integer numberclass = 0;
                                        Integer dateexam = 0;


                                        for (int i = 0; i < response.body().getLesson().size(); i++) {


                                            l_id_term = Integer.valueOf(response.body().getLesson().get(i).getIdTerm());
                                            id_lesson = Integer.valueOf(response.body().getLesson().get(i).getIdLesson());
                                            namelesson = response.body().getLesson().get(i).getNameLesson();
                                            countunit = Integer.valueOf(response.body().getLesson().get(i).getCountUnit());
                                            nameteacher = response.body().getLesson().get(i).getNameTeacher();

                                            dayclass = Integer.valueOf(response.body().getLesson().get(i).getDayClass());
                                            clockclass = response.body().getLesson().get(i).getClockClass();
                                            numberclass = Integer.valueOf(response.body().getLesson().get(i).getNumberClass());
                                            dateexam = Integer.valueOf(response.body().getLesson().get(i).getDateExam());

                                            insert.ADD_lesson(id_user, l_id_term, namelesson, id_lesson, countunit, nameteacher, dayclass, clockclass, numberclass, dateexam);
                                        }
                                    }


                                    if (Integer.valueOf(response.body().getNote().get(0).getIdLesson())>0) {
                                        Integer n_id_lesson = 0;
                                        Integer id_note = 0;
                                        Integer note_id_term = 0;
                                        String titer = "";
                                        String note = "";


                                        for (int i = 0; i < response.body().getNote().size(); i++) {

                                            n_id_lesson = Integer.valueOf(response.body().getNote().get(i).getIdLesson());
                                            id_note = Integer.valueOf(response.body().getNote().get(i).getIdNote());
                                            note_id_term = Integer.valueOf(response.body().getNote().get(i).getIdTerm());
                                            titer = response.body().getNote().get(i).getTiter();
                                            note = response.body().getNote().get(i).getNote();


                                            insert.ADD_note(id_user, n_id_lesson, id_note, titer, note,note_id_term);
                                        }
                                    }


                                    if (Integer.valueOf(response.body().getReminder().get(0).getIdLesson())>0) {
                                        Integer r_id_lesson = 0;
                                        Integer id_reminder = 0;
                                        Integer  reminder_id_term = 0;
                                        Integer flag = 0;
                                        String r_titer = "";
                                        String r_note = "";
                                        Integer datereminder = 0;
                                        String clockreminder = "";


                                        for (int i = 0; i < response.body().getReminder().size(); i++) {

                                            r_id_lesson = Integer.valueOf(response.body().getReminder().get(i).getIdLesson());
                                            id_reminder = Integer.valueOf(response.body().getReminder().get(i).getIdReminder());
                                            reminder_id_term = Integer.valueOf(response.body().getReminder().get(i).getIdTerm());
                                            flag = Integer.valueOf(response.body().getReminder().get(i).getFlag());
                                            r_titer = response.body().getReminder().get(i).getTiter();
                                            r_note = response.body().getReminder().get(i).getNote();
                                            datereminder = Integer.valueOf(response.body().getReminder().get(i).getDateReminder());
                                            clockreminder = response.body().getReminder().get(i).getClockReminder();


                                            insert.ADD_noteReminder(id_user, r_id_lesson, id_reminder, flag, r_titer, r_note, datereminder, clockreminder,reminder_id_term);
                                        }
                                    }




                                    if (Integer.valueOf(response.body().getLessonReportCard().get(0).getIdLesson())>0) {
                                        Integer id_term_lesson_report = 0;
                                        Integer id_lesson_report_card = 0;

                                        Integer id_lesson = 0;
                                        float grade = 0;


                                        for (int i = 0; i < response.body().getLessonReportCard().size(); i++) {

                                            id_lesson_report_card = Integer.valueOf(response.body().getLessonReportCard().get(i).getIdLessonReportCard());
                                            id_term_lesson_report= Integer.valueOf(response.body().getLessonReportCard().get(i).getIdTerm());
                                            id_lesson = Integer.valueOf(response.body().getLessonReportCard().get(i).getIdLesson());
                                            grade = Float.valueOf(response.body().getLessonReportCard().get(i).getGrade());

                                            insert.ADD_lesson_report_card(id_user,id_term_lesson_report,id_lesson_report_card,id_lesson,grade);


                                        }
                                    }



                                    if (Integer.valueOf(response.body().getTermReportCard().get(0).getIdTerm())>0) {
                                        Integer id_term_report_card = 0;
                                        Integer id_term_r = 0;
                                        Integer semester_term_r = 0;


                                        for (int i = 0; i < response.body().getTermReportCard().size(); i++) {

                                            id_term_report_card = Integer.valueOf(response.body().getTermReportCard().get(i).getIdTermReportCard());
                                            id_term_r = Integer.valueOf(response.body().getTermReportCard().get(i).getIdTerm());
                                            semester_term_r = Integer.valueOf(response.body().getTermReportCard().get(i).getTerm());

                                            insert.ADD_term_report_card(id_user,id_term_report_card,id_term_r,semester_term_r);


                                        }
                                    }


                                    _GetTerm=true;
                                    loadingProgress.Close();


                                    Intent i=new Intent("loadData_ok");
                                    LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);
                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==201){
                                    utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin),view.getContext());
                                    loadingProgress.Close();
                                    Intent i=new Intent("loadData_faild");
                                    LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);
                                }
                            }else{
                                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD),view.getContext());
                                loadingProgress.Close();
                                Intent i=new Intent("loadData_faild");
                                LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);

                            }
                        }

                        @Override
                        public void onFailure(Call<SyncDataMessage> call, Throwable t) {
                            // Log error here since request failed
                            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.FragSignin_Message_TrayAgin),view.getContext());
                            loadingProgress.Close();
                            Intent i=new Intent("loadData_faild");
                            LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);

                        }
                    });




                }catch (Exception e){
                    loadingProgress.Close();
                    e.printStackTrace();
                    Intent i=new Intent("loadData_faild");
                    LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);

                }

            }else{
                // not internet connection
                loadingProgress.Close();
                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_NOT_INTERNET_CONNECTION),view.getContext());
                Intent i=new Intent("loadData_faild");
                LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);

            }


        }else {
            loadingProgress.Close();
            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.btn_Load_Data_tryAgain),view.getContext());
            Intent i=new Intent("loadData_faild");
            LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);

        }

        return _GetTerm;

    }

    private BroadcastReceiver mMessageReceiver_OK = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            btn_enter.setVisibility(View.VISIBLE);
        }

    };

    private BroadcastReceiver mMessageReceiver_FAILD = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            btn_enter.setVisibility(View.VISIBLE);
        }

    };

}
