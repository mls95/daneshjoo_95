package com.hsb.daneshjoo.settings;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Locale;

/**
 * Created by hsb on 7/15/2017.
 */

public class LanguageLocal {
    Context c;

    public LanguageLocal(Context c) {
        this.c = c;
    }

    public void language(String lan){
        Locale myLocale = new Locale(lan);
        Resources res = c.getApplicationContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

    }
}
