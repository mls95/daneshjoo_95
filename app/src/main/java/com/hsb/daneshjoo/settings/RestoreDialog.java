package com.hsb.daneshjoo.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.BackupSqliteDataBase;
import com.hsb.daneshjoo.db.RestoreSqliteDataBase;

/**
 * Created by hsb-pc on 3/12/2017.
 */
public class RestoreDialog extends Dialog {

    public RestoreDialog(Context context) {
        super(context);
    }

    Button btn_restore,btn_cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_restore);

        Initi();
    }


    private void Initi(){
        btn_restore=(Button) findViewById(R.id.btn_dialog_restore_save);
        btn_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestoreSqliteDataBase backup=new RestoreSqliteDataBase();
                try {
                    Log.i("Exception","fffffffffffffffffffffffffffff");
                    backup.Restore();
                }catch (Exception ex){
                    Log.i("Exception",ex.toString());
                }

            }
        });

        btn_cancel=(Button) findViewById(R.id.btn_dialog_restore_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
