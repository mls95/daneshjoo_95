package com.hsb.daneshjoo.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by hsb-pc on 3/9/2017.
 */
public class LanguageDialog extends Dialog {

    Button btn_cancel,btn_save;
    AppCompatRadioButton radioButton_persian,radioButton_english;
    KeySetting keySetting;
    LanguageLocal languageLocal;
    List<String> list;
    ArrayAdapter<String> adapter;

    int Lan_fa=0;
    int Lan_en=0;
    public LanguageDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_language);
        Initilaize();

    }

    private void Initilaize(){




        keySetting=new KeySetting(getContext());
        languageLocal = new LanguageLocal(getContext());





        btn_cancel=(Button) findViewById(R.id.btn_dialog_language_cancel);
        radioButton_english=(AppCompatRadioButton) findViewById(R.id.rad_dialog_language_EN);
        radioButton_persian=(AppCompatRadioButton) findViewById(R.id.rad_dialog_language_FA);
        btn_save=(Button) findViewById(R.id.btn_dialog_language_save);


        if (keySetting.getLanguage().equals("en")){
            radioButton_english.setChecked(true);
            radioButton_persian.setChecked(false);
        }else{
            radioButton_persian.setChecked(true);
            radioButton_english.setChecked(false);
        }



        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              dismiss();
            }
        });


        radioButton_english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    Lan_en=1;
                    Lan_fa=0;
                }else{

                }
            }
        });

        radioButton_persian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    Lan_fa=1;
                    Lan_en=0;
                }else{

                }
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Lan_fa==1){
                    keySetting.setLanguage("fa");
                    languageLocal.language("fa");
                }else if (Lan_en==1){
                    keySetting.setLanguage("en");
                    languageLocal.language("en");
                }

                Intent i=new Intent("change_Language");
                LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(i);
                dismiss();
            }
        });




    }




}
