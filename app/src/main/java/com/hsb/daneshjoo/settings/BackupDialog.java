package com.hsb.daneshjoo.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.BackupSqliteDataBase;

import org.json.JSONObject;

import static com.hsb.daneshjoo.R.layout.dialog_backup;

/**
 * Created by hsb-pc on 3/12/2017.
 */
public class BackupDialog extends Dialog {

    Button btn_backup, btn_cancel;

    public BackupDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);
        setContentView(dialog_backup);
        Initi();

    }


    private void Initi(){
        btn_backup=(Button) findViewById(R.id.btn_dialog_backup_save);
        btn_backup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BackupSqliteDataBase backup=new BackupSqliteDataBase();
                try {
                    backup.Backup();
                    Log.i("Exception","fffffffffffffffffffffffffffff");

                }catch (Exception ex){
                    Log.i("Exception",ex.toString());
                }

            }
        });

        btn_cancel=(Button) findViewById(R.id.btn_dialog_backup_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
