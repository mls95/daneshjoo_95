package com.hsb.daneshjoo.settings;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hsb-pc on 2/16/2017.
 */
public class KeySetting {
    SharedPreferences prefe;
    SharedPreferences.Editor editor;
    Context context;

    public KeySetting(Context context) {
        this.context = context;
    }


    public void setIntro_App(boolean introApp) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("IntroApp", introApp);
        editor.apply();
    }

    public boolean getIntro_App() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("IntroApp", false);
    }

    public void setFirst_AppRun(boolean firstAppRun) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("FirstAppRun", firstAppRun);
        editor.apply();
    }

    public boolean getFirst_AppRun() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("FirstAppRun", false);
    }

    public void setCreate_Account(boolean createAccount) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("CreateAccount", createAccount);
        editor.apply();
    }

    public boolean getCreate_Account() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("CreateAccount", false);
    }

    public void setCreate_Student(boolean createStudent) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("CreateStudent", createStudent);
        editor.apply();
    }

    public boolean getCreate_Student() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("CreateStudent", false);
    }

    public void setSync_App(boolean syncApp) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("SettingSyncApp", syncApp);
        editor.apply();
    }

    public boolean getSync_App() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("SettingSyncApp", false);
    }


    public void setLanguage(String language) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putString("SettingLanguage", language);
        editor.apply();
    }

    public String getLanguage() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getString("SettingLanguage", "");
    }

    public void setNew_Version(boolean newVersion) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("SettingNewVersion", newVersion);
        editor.apply();
    }

    public boolean getNew_Version() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("SettingNewVersion", false);
    }

    public void setVibration_Notification(boolean vibrationNotification) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("SettingVibrationNotification", vibrationNotification);
        editor.apply();
    }

    public boolean getVibration_Notification() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("SettingVibrationNotification", false);
    }

    public void setSaveDataOffline(boolean saveDataOffline) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("SettingSaveDataOffline", saveDataOffline);
        editor.apply();
    }

    public boolean getSaveDataOffline() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("SettingSaveDataOffline", false);
    }


    public void setClassPhoneVibration(boolean classPhoneVibration) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("settingClassPhoneVibration", classPhoneVibration);
        editor.apply();
    }

    public boolean getClassPhoneVibration() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("settingClassPhoneVibration", false);
    }


    public void setExamPhoneVibration(boolean examPhoneVibration) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("settingExamPhoneVibration", examPhoneVibration);
        editor.apply();
    }

    public boolean getExamPhoneVibration() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("settingExamPhoneVibration", false);
    }

    public void setTerm(int term) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingTerm", term);
        editor.apply();
    }

    public int getTerm() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingTerm", 0);
    }


    public void setOnStartFragment(int onStartFragment) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingOnStartFragment", onStartFragment);
        editor.apply();
    }

    public int getOnStartFragment() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingOnStartFragment", 0);
    }


    public void setActiveFragment(int activeFragment) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveFragment", activeFragment);
        editor.apply();
    }

    public int getActiveFragment() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveFragment", 0);
    }

    public void setActiveSCVFragReminder(int activeSCVReminder) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveSCVFragReminder", activeSCVReminder);
        editor.apply();
    }

    public int getActiveSCVFragReminder() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveSCVFragReminder", 0);
    }

    public void setActiveSCVMain(int activeSCVMain) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveMain", activeSCVMain);
        editor.apply();
    }

    public int getActiveSCVMain() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveMain", 0);
    }

    public void setActiveSCVFragNote(int activeSCVFragNote) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveFragNote", activeSCVFragNote);
        editor.apply();
    }

    public int getActiveSCVFragNote() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveFragNote", 0);
    }

    public void setActiveSCVFragLesson(int activeSCVFragLesson) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveFragLesson", activeSCVFragLesson);
        editor.apply();
    }

    public int getActiveSCVFragLesson() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveFragLesson", 0);
    }

    public void setActiveSCVFragTerm(int activeSCVFragTerm) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveFragTerm", activeSCVFragTerm);
        editor.apply();
    }

    public int getActiveSCVFragTerm() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveFragTerm", 0);
    }

    public void setActiveSCVFragReportCard(int activeSCVFragReportCard) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveFragReportCard", activeSCVFragReportCard);
        editor.apply();
    }

    public int getActiveSCVFragReportCard() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveFragReportCard", 0);
    }

    public void setActiveSCVTermRcyclerViewadapter(int activeSCVTermRcyclerViewadapter) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveTermRcyclerViewadapter", activeSCVTermRcyclerViewadapter);
        editor.apply();
    }

    public int getActiveSCVTermRcyclerViewadapter() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveTermRcyclerViewadapter", 0);
    }

    public void setActiveSCVTermReportCardRecyclerView(int activeSCVTermReportCardRecyclerView) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveTermReportCardRecyclerView", activeSCVTermReportCardRecyclerView);
        editor.apply();
    }

    public int getActiveSCVTermReportCardRecyclerView() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveTermReportCardRecyclerView", 0);
    }

    public void setActiveSCVLessonReportCardRecyclerView(int activeSCVLessonReportCardRecyclerView) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putInt("settingActiveLessonReportCardRecyclerView", activeSCVLessonReportCardRecyclerView);
        editor.apply();
    }

    public int getActiveSCVLessonReportCardRecyclerView() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getInt("settingActiveLessonReportCardRecyclerView", 0);
    }


    public void setTopPageHomeFragmet(boolean topPageHomeFragmet) {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        editor = prefe.edit();
        editor.putBoolean("settingTopPageHomeFragmet", topPageHomeFragmet);
        editor.apply();
    }

    public boolean getTopPageHomeFragmet() {
        prefe = context.getSharedPreferences("com.hsb.daneshjoo.settings", Context.MODE_PRIVATE);
        return prefe.getBoolean("settingTopPageHomeFragmet", false);
    }
}
