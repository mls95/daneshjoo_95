package com.hsb.daneshjoo.services;

import com.hsb.daneshjoo.services.webservice.request.ForgetPassword;
import com.hsb.daneshjoo.services.webservice.request.LessonJson;
import com.hsb.daneshjoo.services.webservice.request.LoginJson;
import com.hsb.daneshjoo.services.webservice.request.NoteJson;
import com.hsb.daneshjoo.services.webservice.request.ProfileJson;
import com.hsb.daneshjoo.services.webservice.request.ReminderJson;
import com.hsb.daneshjoo.services.webservice.request.SignUpJson;
import com.hsb.daneshjoo.services.webservice.request.TermJson;
import com.hsb.daneshjoo.services.webservice.request.TokenJson;
import com.hsb.daneshjoo.services.webservice.response.AppVersionMessage;
import com.hsb.daneshjoo.services.webservice.response.LessonMessage;
import com.hsb.daneshjoo.services.webservice.response.LoginMessage;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.services.webservice.response.NoteMessage;
import com.hsb.daneshjoo.services.webservice.response.ProfileMessage;
import com.hsb.daneshjoo.services.webservice.response.ReminderMessage;
import com.hsb.daneshjoo.services.webservice.response.SyncDataMessage;
import com.hsb.daneshjoo.services.webservice.response.TermMessage;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by hsb on 6/2/2017.
 */

public interface StoreClient {







    /**
     * RESTful
     * POST API
     * read directory
     * action read
     */

    // post read data login

    @POST("daneshjooo/daneshjooo/DB/db_account_login.php")
    Call<LoginMessage> postloginaccount(@Body LoginJson loginJson);

    // post read data term
    @POST("daneshjooo/daneshjooo/DB/read/db_term_read.php")
    Call<TermMessage> posttokenterm(@Body TokenJson tokenJson);


    // post read data term
    @POST("daneshjooo/daneshjooo/DB/read/db_data_read.php")
    Call<SyncDataMessage> posttokendata(@Body TokenJson tokenJson);


    // post read data lesson
    @POST("daneshjooo/daneshjooo/DB/read/db_lesson_read.php")
    Call<LessonMessage> posttokenlesson(@Body TokenJson tokenJson);

    // post read data profile
    @POST("daneshjooo/daneshjooo/DB/read/db_profile_read.php")
    Call<ProfileMessage> posttokenprofile(@Body TokenJson tokenJson);


    // post read data note
    @POST("daneshjooo/daneshjooo/DB/read/db_note_read.php")
    Call<NoteMessage> posttokennote(@Body TokenJson tokenJson);

    // post read data reminder
    @POST("daneshjooo/daneshjooo/DB/read/db_reminder_read.php")
    Call<ReminderMessage> posttokenreminder(@Body TokenJson tokenJson);

    // post read data app version
    @POST("daneshjooo/daneshjooo/DB/read/app_version.php")
    Call<AppVersionMessage> posttokenappversion(@Body TokenJson tokenJson);

    // post forget username and password
    @POST("daneshjooo/daneshjooo/DB/read/db_account_forget.php")
    Call<Messageing> postforgetpassword(@Body ForgetPassword forgetPassword);

    /**
     * **************************************************************************************
     * *********************************  END READ  ***********************************************
     */

    /**
     * RESTful
     * POST API
     * create directory
     * action create
     */

    // post crate new account
    @POST("daneshjooo/daneshjooo/DB/create/db_account_create.php")
    Call<Messageing> postcreateaccount(@Body SignUpJson accountjson);

    // post insert new reminder
    @POST("daneshjooo/daneshjooo/DB/create/db_profile_create.php")
    Call<Messageing> postinsertprofile(@Body ProfileJson profileJson);


    // post insert new term
    @POST("daneshjooo/daneshjooo/DB/create/db_term_create.php")
    Call<Messageing> postinsertterm(@Body TermJson termJson);


    // post insert new lesson
    @POST("daneshjooo/daneshjooo/DB/create/db_lesson_create.php")
    Call<Messageing> postinsertlesson(@Body LessonJson lessonJson);



    // post insert new Note
    @POST("daneshjooo/daneshjooo/DB/create/db_note_create.php")
    Call<Messageing> postinsertnote(@Body NoteJson noteJson);


    // post insert new reminder
    @POST("daneshjooo/daneshjooo/DB/create/db_reminder_create.php  ")
    Call<Messageing> postinsertreminder(@Body ReminderJson reminderJson);


    /**
     * **************************************************************************************
     * *********************************  END CREATE ****************************************
     */




    /**
     * RESTful
     * POST API
     * delete directory
     * action delete
     */

    // term
    @POST("daneshjooo/daneshjooo/DB/delete/db_term_delete.php")
    Call<Messageing> postdeleteterm(@Body Object termsjon);


    // lesson
    @POST("daneshjooo/daneshjooo/DB/delete/db_lesson_delete.php")
    Call<Messageing> postdeletelesson(@Body Object lessonjson);


    // note
    @POST("daneshjooo/daneshjooo/DB/delete/db_note_delete.php")
    Call<Messageing> postdeletenote(@Body Object ntoejson);


    // reminder
    @POST("daneshjooo/daneshjooo/DB/delete/db_reminder_delete.php")
    Call<Messageing> postdeletereminder(@Body Object reminderjson);

    /**
     * **************************************************************************************
     * *********************************  END  DELETE  ***********************************************
     */



    /**
     * RESTful
     * POST API
     * edit directory
     * action edit
     */

    // term
    @POST("daneshjooo/daneshjooo/DB/edit/db_term_edit.php")
    Call<Messageing> posteditterm(@Body Object termjson);


    // lesson
    @POST("daneshjooo/daneshjooo/DB/edit/db_lesson_edit.php")
    Call<Messageing> posteditlesson(@Body Object lessonjson);


    // lesson Report Card
    @POST("daneshjooo/daneshjooo/DB/edit/db_lesson_report_card_edit.php")
    Call<Messageing> posteditlessonReportCard(@Body Object lessonreportcardjson);


    // note
    @POST("daneshjooo/daneshjooo/DB/edit/db_note_edit.php")
    Call<Messageing> posteditnote(@Body Object notejson);


    // reminder
    @POST("daneshjooo/daneshjooo/DB/edit/db_reminder_edit.php")
    Call<Messageing> posteditreminder(@Body Object reminderjson);

    // profile
    @POST("daneshjooo/daneshjooo/DB/edit/db_profile_edit.php")
    Call<Messageing> posteditprofile(@Body Object profileJson);




    /**
     * **************************************************************************************
     * *********************************  END  EDIT  ***********************************************
     */
}
