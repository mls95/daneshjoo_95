package com.hsb.daneshjoo.services.webservice.response;

/**
 * Created by hsb on 6/2/2017.
 */

public class Messageing {

    private String SuccessMessage;
    private String SuccessCode;
    private Integer Token;

    public Messageing(String successMessage, String successCode, Integer token) {
        SuccessMessage = successMessage;
        SuccessCode = successCode;
        Token = token;
    }


    public String getSuccessMessage() {
        return SuccessMessage;
    }

    public void setSuccessMessage(String successMessage) {
        SuccessMessage = successMessage;
    }

    public String getSuccessCode() {
        return SuccessCode;
    }

    public void setSuccessCode(String successCode) {
        SuccessCode = successCode;
    }

    public Integer getToken() {
        return Token;
    }

    public void setToken(Integer token) {
        Token = token;
    }

    @Override
    public String toString() {
        return "Messageing{" +
                "SuccessMessage='" + SuccessMessage + '\'' +
                ", SuccessCode='" + SuccessCode + '\'' +
                ", Token=" + Token +
                '}';
    }
}
