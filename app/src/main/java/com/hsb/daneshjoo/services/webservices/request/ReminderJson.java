package com.hsb.daneshjoo.services.webservices.request;

/**
 * Created by MoSiOne70 on 02/06/2017.
 */

public class ReminderJson {
    private int id_user;
    private int id_lesson;
    private int flag;
    private String titr;
    private String note;
    private String date_reminder;
    private String clock_reminder;

    public ReminderJson(int idUser,int idLesson,int flag,String titr,String note
            ,String dateRemainder,String clockReminder)
    {
        this.id_user=idUser;
        this.id_lesson=idLesson;
        this.flag=flag;
        this.titr=titr;
        this.note=note;
        this.date_reminder=dateRemainder;
        this.clock_reminder=clockReminder;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_lesson(int id_lesson) {
        this.id_lesson = id_lesson;
    }

    public int getId_lesson() {
        return id_lesson;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setTitr(String titr) {
        this.titr = titr;
    }

    public String getTitr() {
        return titr;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setClock_reminder(String clock_reminder) {
        this.clock_reminder = clock_reminder;
    }

    public String getClock_reminder() {
        return clock_reminder;
    }

    public void setDate_reminder(String date_reminder) {
        this.date_reminder = date_reminder;
    }

    public String getDate_reminder() {
        return date_reminder;
    }
}
