package com.hsb.daneshjoo.services.webservice.response;

import com.hsb.daneshjoo.services.webservice.request.NoteJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb on 6/5/2017.
 */

public class NoteMessage extends Messageing {

    List<NoteJson> Note=new ArrayList<>();

    public NoteMessage(String successMessage, String successCode, Integer token, List<NoteJson> note) {
        super(successMessage, successCode, token);
        Note = note;
    }

    public List<NoteJson> getNote() {
        return Note;
    }

    public void setNote(List<NoteJson> note) {
        Note = note;
    }

    @Override
    public String toString() {
        return "NoteMessage{" +
                "Note=" + Note +
                '}';
    }
}
