package com.hsb.daneshjoo.services.webservices.request;

/**
 * Created by MoSiOne70 on 02/06/2017.
 */

public class LoginJson {
    private String username;
    private String password;
    public LoginJson(String userName,String passWord)
    {
        this.username=userName;
        this.password=passWord;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
