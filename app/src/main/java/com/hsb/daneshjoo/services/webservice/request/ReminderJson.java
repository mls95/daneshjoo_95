package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/5/2017.
 */

public class ReminderJson {

    private String Token;
    private String IdLesson;
    private String IdTerm;
    private String IdReminder;
    private String Flag;
    private String Titer;
    private String PastTiter;
    private String Note;
    private String DateReminder;
    private String ClockReminder;


    public ReminderJson(String token, String idLesson, String idTerm, String idReminder, String flag, String titer, String pastTiter, String note, String dateReminder, String clockReminder) {
        Token = token;
        IdLesson = idLesson;
        IdTerm = idTerm;
        IdReminder = idReminder;
        Flag = flag;
        Titer = titer;
        PastTiter = pastTiter;
        Note = note;
        DateReminder = dateReminder;
        ClockReminder = clockReminder;
    }


    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getIdLesson() {
        return IdLesson;
    }

    public void setIdLesson(String idLesson) {
        IdLesson = idLesson;
    }

    public String getIdTerm() {
        return IdTerm;
    }

    public void setIdTerm(String idTerm) {
        IdTerm = idTerm;
    }

    public String getIdReminder() {
        return IdReminder;
    }

    public void setIdReminder(String idReminder) {
        IdReminder = idReminder;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getTiter() {
        return Titer;
    }

    public void setTiter(String titer) {
        Titer = titer;
    }

    public String getPastTiter() {
        return PastTiter;
    }

    public void setPastTiter(String pastTiter) {
        PastTiter = pastTiter;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getDateReminder() {
        return DateReminder;
    }

    public void setDateReminder(String dateReminder) {
        DateReminder = dateReminder;
    }

    public String getClockReminder() {
        return ClockReminder;
    }

    public void setClockReminder(String clockReminder) {
        ClockReminder = clockReminder;
    }


    @Override
    public String toString() {
        return "ReminderJson{" +
                "Token='" + Token + '\'' +
                ", IdLesson='" + IdLesson + '\'' +
                ", IdTerm='" + IdTerm + '\'' +
                ", IdReminder='" + IdReminder + '\'' +
                ", Flag='" + Flag + '\'' +
                ", Titer='" + Titer + '\'' +
                ", PastTiter='" + PastTiter + '\'' +
                ", Note='" + Note + '\'' +
                ", DateReminder='" + DateReminder + '\'' +
                ", ClockReminder='" + ClockReminder + '\'' +
                '}';
    }
}


