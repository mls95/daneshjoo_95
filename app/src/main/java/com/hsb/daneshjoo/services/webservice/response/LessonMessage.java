package com.hsb.daneshjoo.services.webservice.response;

import com.hsb.daneshjoo.services.webservice.request.LessonJson;

import java.util.List;

/**
 * Created by hsb on 6/5/2017.
 */

public class LessonMessage extends Messageing {


   List<LessonJson> Lesson;


    public LessonMessage(String successMessage, String successCode, Integer token, List<LessonJson> lesson) {
        super(successMessage, successCode, token);
        Lesson = lesson;
    }


    public List<LessonJson> getLesson() {
        return Lesson;
    }

    public void setLesson(List<LessonJson> lesson) {
        Lesson = lesson;
    }

    @Override
    public String toString() {
        return "LessonMessage{" +
                "Lesson=" + Lesson +
                '}';
    }
}
