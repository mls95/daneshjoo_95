package com.hsb.daneshjoo.services;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hsb on 6/2/2017.
 */

public class RestClient {



   // public static final String API_BASE_URL = "https://smarstudent.000webhostapp.com";
    //public static final String API_BASE_URL = "http://hsb1992.advancedfreeweb.com";
    //public static final String API_BASE_URL = "http://www.smartstudent.byethost24.com";

    public static final String API_BASE_URL = "http://f10-preview.atspace.me/smartstudent.ocm/";
    private static Gson gson= new GsonBuilder()
            .setLenient()
            .create();


    private static OkHttpClient httpClient=new OkHttpClient.Builder().build();

    private static Retrofit.Builder builder=new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = null;


    public static <s> s createService(Class<s> serviceClass){
        Log.i("build",builder.toString());
        Retrofit retrofit=builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }





    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }



}
