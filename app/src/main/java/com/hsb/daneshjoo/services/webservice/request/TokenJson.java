package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/5/2017.
 */

public class TokenJson {
    private Integer token;

    public TokenJson(Integer token) {
        this.token = token;
    }


    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }


    @Override
    public String toString() {
        return "TokenJson{" +
                "token=" + token +
                '}';
    }
}
