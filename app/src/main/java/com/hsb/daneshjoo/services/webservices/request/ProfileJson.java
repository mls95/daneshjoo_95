package com.hsb.daneshjoo.services.webservices.request;

/**
 * Created by MoSiOne70 on 02/06/2017.
 */

public class ProfileJson {
    private int id_user;
    private String name;
    private String family;
    private String field;
    private String level;
    private String entering_year;
    private int age;
    private int sex;
    private String univercity_name;

    public ProfileJson(int idUser,String name,String family,String field,String level
            ,String enteringYear,int age,int sex,String univercityName)
    {
        this.id_user=idUser;
        this.name=name;
        this.family=family;
        this.field=field;
        this.level=level;
        this.entering_year=enteringYear;
        this.age=age;
        this.sex=sex;
        this.univercity_name=univercityName;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_user() {
        return id_user;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getFamily() {
        return family;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    public void setEntering_year(String entering_year) {
        this.entering_year = entering_year;
    }

    public String getEntering_year() {
        return entering_year;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getSex() {
        return sex;
    }

    public void setUnivercity_name(String univercity_name) {
        this.univercity_name = univercity_name;
    }

    public String getUnivercity_name() {
        return univercity_name;
    }
}
