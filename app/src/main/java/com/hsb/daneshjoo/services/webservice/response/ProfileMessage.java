package com.hsb.daneshjoo.services.webservice.response;

/**
 * Created by hsb on 6/4/2017.
 */

public class ProfileMessage extends Messageing {


    private String Name ;
    private String Family ;
    private String Field ;
    private String Level ;
    private String EnteringYear ;
    private String Age ;
    private String Sex ;
    private String UnivercityName ;

    public ProfileMessage(String successMessage, String successCode, Integer token, String name, String family, String field, String level, String enteringYear, String age, String sex, String univercityName) {
        super(successMessage, successCode, token);
        Name = name;
        Family = family;
        Field = field;
        Level = level;
        EnteringYear = enteringYear;
        Age = age;
        Sex = sex;
        UnivercityName = univercityName;
    }

    public String getNmae() {
        return Name;
    }

    public void setNmae(String nmae) {
        Name = nmae;
    }

    public String getFamily() {
        return Family;
    }

    public void setFamily(String family) {
        Family = family;
    }

    public String getField() {
        return Field;
    }

    public void setField(String field) {
        Field = field;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String level) {
        Level = level;
    }

    public String getEnteringYear() {
        return EnteringYear;
    }

    public void setEnteringYear(String enteringYear) {
        EnteringYear = enteringYear;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getUnivercityName() {
        return UnivercityName;
    }

    public void setUnivercityName(String univercityName) {
        UnivercityName = univercityName;
    }


    @Override
    public String toString() {
        return "ProfileMessage{" +
                "Nmae='" + Name + '\'' +
                ", Family='" + Family + '\'' +
                ", Field='" + Field + '\'' +
                ", Level='" + Level + '\'' +
                ", EnteringYear='" + EnteringYear + '\'' +
                ", Age='" + Age + '\'' +
                ", Sex='" + Sex + '\'' +
                ", UnivercityName='" + UnivercityName + '\'' +
                '}';
    }




}
