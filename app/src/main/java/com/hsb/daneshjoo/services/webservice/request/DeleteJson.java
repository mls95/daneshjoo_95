package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/16/2017.
 */

public class DeleteJson {

    private String Token;
    private String Id;


    public DeleteJson(String token, String id) {
        Token = token;
        Id = id;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "DeleteJson{" +
                "Token='" + Token + '\'' +
                ", Id='" + Id + '\'' +
                '}';
    }
}
