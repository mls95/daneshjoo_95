package com.hsb.daneshjoo.services.webservice.response;

import com.hsb.daneshjoo.services.webservice.request.TermJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb on 6/5/2017.
 */

public class TermMessage extends Messageing {

    List<TermJson> Term = new ArrayList<>();

    public TermMessage(String successMessage, String successCode, Integer token, List<TermJson> term) {
        super(successMessage, successCode, token);
        Term = term;
    }

    public List<TermJson> getTerm() {
        return Term;
    }

    public void setTerm(List<TermJson> term) {
        Term = term;
    }

    @Override
    public String toString() {
        return "TermMessage{" +
                "Term=" + Term +
                '}';
    }
}
