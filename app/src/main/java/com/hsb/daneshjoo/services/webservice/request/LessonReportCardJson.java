package com.hsb.daneshjoo.services.webservice.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hsb on 8/4/2017.
 */

public class LessonReportCardJson  {


    private String Token;
    private String IdTerm;
    private String IdLessonReportCard;
    private String IdLesson;
    private String Grade;


    public LessonReportCardJson(String token, String idTerm, String idLessonReportCard, String idLesson, String grade) {
        Token = token;
        IdTerm = idTerm;
        IdLessonReportCard = idLessonReportCard;
        IdLesson = idLesson;
        Grade = grade;
    }


    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getIdTerm() {
        return IdTerm;
    }

    public void setIdTerm(String idTerm) {
        IdTerm = idTerm;
    }

    public String getIdLessonReportCard() {
        return IdLessonReportCard;
    }

    public void setIdLessonReportCard(String idLessonReportCard) {
        IdLessonReportCard = idLessonReportCard;
    }

    public String getIdLesson() {
        return IdLesson;
    }

    public void setIdLesson(String idLesson) {
        IdLesson = idLesson;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }


    @Override
    public String toString() {
        return "LessonReportCardJson{" +
                "Token='" + Token + '\'' +
                ", IdTerm='" + IdTerm + '\'' +
                ", IdLessonReportCard='" + IdLessonReportCard + '\'' +
                ", IdLesson='" + IdLesson + '\'' +
                ", Grade='" + Grade + '\'' +
                '}';
    }
}
