package com.hsb.daneshjoo.services;

/**
 * Created by hsb on 6/16/2017.
 */

public enum EditApi {
    API_EDIT_TERM,
    API_EDIT_LESSON,
    API_EDIT_NOTE,
    API_EDIT_REMINDER,
    API_EDIT_PROFILE
}
