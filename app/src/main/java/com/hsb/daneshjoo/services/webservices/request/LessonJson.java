package com.hsb.daneshjoo.services.webservices.request;

import com.hsb.daneshjoo.cards.lesson.LessonItem;

/**
 * Created by MoSiOne70 on 02/06/2017.
 */

public class LessonJson {

    private int id_user;
    private int id_term;
    private int number_class;
    private String name_lesson;
    private String count_unit;
    private String name_teacher;
    private String day_class;
    private String date_exam;
    private String clock_class;

    public LessonJson(int idUser,int idTerm,int numberClass,String countUnit,String nameLesson
            ,String nameTeacher,String dayClass,String dateExam,String clockClass)
    {
        this.id_user=idUser;
        this.id_term=idTerm;
        this.number_class=numberClass;
        this.name_lesson=nameLesson;
        this.count_unit=countUnit;
        this.name_teacher=nameTeacher;
        this.day_class=dayClass;
        this.date_exam=dateExam;
        this.clock_class=clockClass;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_term(int id_term) {
        this.id_term = id_term;
    }

    public int getId_term() {
        return id_term;
    }

    public void setNumber_class(int number_class) {
        this.number_class = number_class;
    }

    public int getNumber_class() {
        return number_class;
    }

    public void setCount_unit(String count_unit) {
        this.count_unit = count_unit;
    }

    public String getCount_unit() {
        return count_unit;
    }

    public void setName_lesson(String name_lesson) {
        this.name_lesson = name_lesson;
    }

    public String getName_lesson() {
        return name_lesson;
    }

    public void setName_teacher(String name_teacher) {
        this.name_teacher = name_teacher;
    }

    public String getName_teacher() {
        return name_teacher;
    }

    public void setClock_class(String clock_class) {
        this.clock_class = clock_class;
    }

    public String getClock_class() {
        return clock_class;
    }

    public void setDate_exam(String date_exam) {
        this.date_exam = date_exam;
    }

    public String getDate_exam() {
        return date_exam;
    }

    public void setDay_class(String day_class) {
        this.day_class = day_class;
    }

    public String getDay_class() {
        return day_class;
    }
}
