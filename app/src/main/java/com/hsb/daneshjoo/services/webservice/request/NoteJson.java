package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/5/2017.
 */

public class NoteJson {

    private String Token;
    private String IdLesson;
    private String IdTerm;
    private String IdNote;
    private String Titer;
    private String PastTiter;
    private String Note;

    public NoteJson(String token, String idLesson, String idTerm, String idNote, String titer, String pastTiter, String note) {
        Token = token;
        IdLesson = idLesson;
        IdTerm = idTerm;
        IdNote = idNote;
        Titer = titer;
        PastTiter = pastTiter;
        Note = note;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getIdLesson() {
        return IdLesson;
    }

    public void setIdLesson(String idLesson) {
        IdLesson = idLesson;
    }

    public String getIdTerm() {
        return IdTerm;
    }

    public void setIdTerm(String idTerm) {
        IdTerm = idTerm;
    }

    public String getIdNote() {
        return IdNote;
    }

    public void setIdNote(String idNote) {
        IdNote = idNote;
    }

    public String getTiter() {
        return Titer;
    }

    public void setTiter(String titer) {
        Titer = titer;
    }

    public String getPastTiter() {
        return PastTiter;
    }

    public void setPastTiter(String pastTiter) {
        PastTiter = pastTiter;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }


    @Override
    public String toString() {
        return "NoteJson{" +
                "Token='" + Token + '\'' +
                ", IdLesson='" + IdLesson + '\'' +
                ", IdTerm='" + IdTerm + '\'' +
                ", IdNote='" + IdNote + '\'' +
                ", Titer='" + Titer + '\'' +
                ", PastTiter='" + PastTiter + '\'' +
                ", Note='" + Note + '\'' +
                '}';
    }
}
