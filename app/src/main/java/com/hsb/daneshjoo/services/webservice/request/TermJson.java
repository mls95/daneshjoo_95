package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/5/2017.
 */

public class TermJson {

    private String Token;
    private String Term;
    private String IdTerm;
    private String PastTerm;
    private String YearDate;
    private String SumUnit;
    private String DateStart;
    private String DateEnd;

    private String IdSemesterReportCard;

    public TermJson(String token, String term, String idTerm, String pastTerm, String yearDate, String sumUnit, String dateStart, String dateEnd, String idSemesterReportCard) {
        Token = token;
        Term = term;
        IdTerm = idTerm;
        PastTerm = pastTerm;
        YearDate = yearDate;
        SumUnit = sumUnit;
        DateStart = dateStart;
        DateEnd = dateEnd;
        IdSemesterReportCard = idSemesterReportCard;
    }


    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getTerm() {
        return Term;
    }

    public void setTerm(String term) {
        Term = term;
    }

    public String getIdTerm() {
        return IdTerm;
    }

    public void setIdTerm(String idTerm) {
        IdTerm = idTerm;
    }

    public String getPastTerm() {
        return PastTerm;
    }

    public void setPastTerm(String pastTerm) {
        PastTerm = pastTerm;
    }

    public String getYearDate() {
        return YearDate;
    }

    public void setYearDate(String yearDate) {
        YearDate = yearDate;
    }

    public String getSumUnit() {
        return SumUnit;
    }

    public void setSumUnit(String sumUnit) {
        SumUnit = sumUnit;
    }

    public String getDateStart() {
        return DateStart;
    }

    public void setDateStart(String dateStart) {
        DateStart = dateStart;
    }

    public String getDateEnd() {
        return DateEnd;
    }

    public void setDateEnd(String dateEnd) {
        DateEnd = dateEnd;
    }

    public String getIdSemesterReportCard() {
        return IdSemesterReportCard;
    }

    public void setIdSemesterReportCard(String idSemesterReportCard) {
        IdSemesterReportCard = idSemesterReportCard;
    }


    @Override
    public String toString() {
        return "TermJson{" +
                "Token='" + Token + '\'' +
                ", Term='" + Term + '\'' +
                ", IdTerm='" + IdTerm + '\'' +
                ", PastTerm='" + PastTerm + '\'' +
                ", YearDate='" + YearDate + '\'' +
                ", SumUnit='" + SumUnit + '\'' +
                ", DateStart='" + DateStart + '\'' +
                ", DateEnd='" + DateEnd + '\'' +
                ", IdSemesterReportCard='" + IdSemesterReportCard + '\'' +
                '}';
    }
}
