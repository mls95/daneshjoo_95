package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/21/2017.
 */

public class ForgetPassword {
    private String Email;

    public ForgetPassword(String email) {
        Email = email;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }


    @Override
    public String toString() {
        return "ForgetPassword{" +
                "Email='" + Email + '\'' +
                '}';
    }
}
