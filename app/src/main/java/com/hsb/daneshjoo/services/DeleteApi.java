package com.hsb.daneshjoo.services;

/**
 * Created by hsb on 6/16/2017.
 */

public enum DeleteApi {
     API_DEL_TERM,
     API_DEL_LESSON,
     API_DEL_NOTE,
     API_DEL_REMINDER,

}
