package com.hsb.daneshjoo.services.webservice.response;

import com.hsb.daneshjoo.services.webservice.request.ProfileJson;

/**
 * Created by hsb on 6/4/2017.
 */

public class LoginMessage extends Messageing  {

    private String UserName;
    private String Email;
    private String Password;

    private ProfileJson profileJson;

    public LoginMessage(String successMessage, String successCode, Integer token, String userName, String email, String password, ProfileJson profileJson) {
        super(successMessage, successCode, token);
        UserName = userName;
        Email = email;
        Password = password;
        this.profileJson = profileJson;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public ProfileJson getProfileJson() {
        return profileJson;
    }

    public void setProfileJson(ProfileJson profileJson) {
        this.profileJson = profileJson;
    }


    @Override
    public String toString() {
        return "LoginMessage{" +
                "UserName='" + UserName + '\'' +
                ", Email='" + Email + '\'' +
                ", Password='" + Password + '\'' +
                ", profileJson=" + profileJson +
                '}';
    }
}
