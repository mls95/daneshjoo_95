package com.hsb.daneshjoo.services.webservices.request;

/**
 * Created by MoSiOne70 on 02/06/2017.
 */

public class TermJson {
    private int id_user;
    private int term;
    private String year_date;
    private String sum_unit;
    private String date_start;
    private String date_end;

    public TermJson(int idUser,int term,String yearDate,String sumUnit,String dateStart
            ,String dateEnd)
    {
        this.id_user=idUser;
        this.term=term;
        this.year_date=yearDate;
        this.sum_unit=sumUnit;
        this.date_start=dateStart;
        this.date_end=dateEnd;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_user() {
        return id_user;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public int getTerm() {
        return term;
    }

    public void setYear_date(String year_date) {
        this.year_date = year_date;
    }

    public String getYear_date() {
        return year_date;
    }

    public void setSum_unit(String sum_unit) {
        this.sum_unit = sum_unit;
    }

    public String getSum_unit() {
        return sum_unit;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_start() {
        return date_start;
    }
}
