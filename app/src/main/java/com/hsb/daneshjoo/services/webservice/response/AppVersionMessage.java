package com.hsb.daneshjoo.services.webservice.response;

/**
 * Created by hsb on 7/14/2017.
 */

public class AppVersionMessage extends Messageing {
    private String CodeVersion ;

    public AppVersionMessage(String successMessage, String successCode, Integer token, String codeVersion) {
        super(successMessage, successCode, token);
        CodeVersion = codeVersion;
    }

    public String getCodeVersion() {
        return CodeVersion;
    }

    public void setCodeVersion(String codeVersion) {
        CodeVersion = codeVersion;
    }

    @Override
    public String toString() {
        return "AppVersionMessage{" +
                "CodeVersion=" + CodeVersion +
                '}';
    }
}
