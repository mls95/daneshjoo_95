package com.hsb.daneshjoo.services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.ReloadRecyclerView;
import com.hsb.daneshjoo.cards.reminder.ReminderDialog;
import com.hsb.daneshjoo.cards.term.TermDialog;
import com.hsb.daneshjoo.db.SqliteDelete;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.fragment.fragTerm;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 6/16/2017.
 */

public class DeleteApiWebService {

    private int _deleteCode=0;

    Context c;
    SqliteInsert insert;
    SqliteDelete sqliteDelete;
    LoadingProgress loadingProgress;
    Call<Messageing> call;

    public DeleteApiWebService(Context c) {
        this.c = c;
        sqliteDelete=new SqliteDelete(c);
        loadingProgress=new LoadingProgress(c);
    }

    public int RunApi(final DeleteApi deleteApi, Object objectjson){
        /**
         * REST API REMINDER
         *
         */
        //Check exeist username
        SqliteRepo repo=new SqliteRepo(c);
        if (repo.Repo_account_Iduser() > 0){
            insert=new SqliteInsert(c);
            NetStatus netStatus=new NetStatus(c);
            if (netStatus.checkInternetConnection()){
                // internet connection

                try {
                    loadingProgress.Show();

                    // build web service

                    // generate json

                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();


                    StoreClient client= RestClient.getClient().create(StoreClient.class);
                    if (deleteApi == DeleteApi.API_DEL_TERM){
                        call = client.postdeleteterm(objectjson);
                    }else if(deleteApi == DeleteApi.API_DEL_LESSON){
                        call = client.postdeletelesson(objectjson);
                    }else if(deleteApi == DeleteApi.API_DEL_NOTE){
                        call = client.postdeletenote(objectjson);
                    }else if (deleteApi == DeleteApi.API_DEL_REMINDER){
                        call = client.postdeletereminder(objectjson);
                    }
                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                            loadingProgress.Close();
                            if (response.code()==200){
                                if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                    _deleteCode=200;
                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){
                                    _deleteCode=201;
                                     DelRow(deleteApi);
                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==201){
                                    _deleteCode=202;
                                }
                            }else{


                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();

                            _deleteCode=0;
                        }
                    });




                }catch (Exception e){
                    loadingProgress.Close();
                    _deleteCode=0;
                    e.printStackTrace();
                }

            }else{
                // not internet connection
                loadingProgress.Close();
                utility.dialog(c.getResources().getString(R.string.Message), c.getResources().getString(R.string.NetMessage_NOT_INTERNET_CONNECTION),c);
                _deleteCode=0;
            }


        }else {
            loadingProgress.Close();
            _deleteCode=0;
        }


        return _deleteCode;
    }



    private void DelRow(DeleteApi deleteApi){
        if (deleteApi == DeleteApi.API_DEL_TERM){
            sqliteDelete.Del_term(utility.getIdTerm());
            sqliteDelete.Del_term_report_card(utility.getIdTerm());
            sqliteDelete.Del_lesson_idTerm(utility.getIdTerm());
            sqliteDelete.Del_lesson_report_card_id_term(utility.getIdTerm());

            Intent i=new Intent("del_term");
            LocalBroadcastManager.getInstance(c).sendBroadcast(i);
        }else if(deleteApi == DeleteApi.API_DEL_LESSON){
            sqliteDelete.Del_lesson(utility.getId());
            sqliteDelete.Del_lesson_report_card(utility.getId());
            Intent i=new Intent("del_lesson");
            LocalBroadcastManager.getInstance(c).sendBroadcast(i);
        }else if(deleteApi == DeleteApi.API_DEL_NOTE){
            sqliteDelete.Del_note(utility.getId());
            Intent i=new Intent("del_note");
            LocalBroadcastManager.getInstance(c).sendBroadcast(i);
        }else if (deleteApi == DeleteApi.API_DEL_REMINDER){

            sqliteDelete.Del_reminder(utility.getId());
            Intent i=new Intent("del_reminder");
            LocalBroadcastManager.getInstance(c).sendBroadcast(i);
        }

    }
}
