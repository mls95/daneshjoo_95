package com.hsb.daneshjoo.services;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.db.SqliteUpdate;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.webservice.request.LessonJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 6/16/2017.
 */

public class EditApiWebService {

    private int _editCode=0;

    Context c;
    SqliteInsert insert;
    LoadingProgress loadingProgress;
    Call<Messageing> call;
    SqliteUpdate sqliteUpdate;

    public EditApiWebService(Context c) {
        this.c = c;
        loadingProgress=new LoadingProgress(c);
        sqliteUpdate=new SqliteUpdate(c);
    }

    public int RunApi(final EditApi editApi, final Object objectjson, final Dialog dialog){
        /**
         * REST API REMINDER
         *
         */
        //Check exeist username
        SqliteRepo repo=new SqliteRepo(c);
        if (repo.Repo_account_Iduser() > 0){
            insert=new SqliteInsert(c);
            NetStatus netStatus=new NetStatus(c);
            if (netStatus.checkInternetConnection()){
                // internet connection

                try {
                        loadingProgress.Show();
                        Log.i("objectjson edit",objectjson.toString());
                    // build web service

                    // generate json

                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();
                    final String json=gson.toJson(objectjson);

                    StoreClient client= RestClient.getClient().create(StoreClient.class);
                    if (editApi == EditApi.API_EDIT_TERM){
                        call = client.posteditterm(objectjson);
                    }else if(editApi == EditApi.API_EDIT_LESSON){
                        call = client.posteditlesson(objectjson);
                    }else if(editApi == EditApi.API_EDIT_NOTE){
                        call = client.posteditnote(objectjson);
                    }else if (editApi == EditApi.API_EDIT_REMINDER){
                        call = client.posteditreminder(objectjson);
                    }else if (editApi == EditApi.API_EDIT_PROFILE){
                        call = client.posteditprofile(objectjson);
                    }
                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                            loadingProgress.Close();
                            if (response.code()==200){
                                if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                    _editCode=200;

                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){
                                    _editCode=201;
                                    OfflineEdit(editApi,json,dialog);


                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==201){

                                    _editCode=202;
                                }
                            }else{


                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();
                            _editCode=0;
                        }
                    });




                }catch (Exception e){
                    loadingProgress.Close();
                    _editCode=0;
                    e.printStackTrace();
                }

            }else{
                // not internet connection
                loadingProgress.Close();
                utility.dialog(c.getResources().getString(R.string.Message), c.getResources().getString(R.string.NetMessage_NOT_INTERNET_CONNECTION),c);
                _editCode=0;
            }


        }else {
            loadingProgress.Close();
            _editCode=0;
        }

        return _editCode;
    }

    private void OfflineEdit(EditApi editApi,String json , Dialog dialog) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (editApi == EditApi.API_EDIT_TERM) {


                sqliteUpdate.Update_term(Integer.valueOf(jsonObject.getString("PastTerm")), Integer.valueOf(jsonObject.getString("Term")),Integer.valueOf(jsonObject.getString("YearDate")), Integer.valueOf(jsonObject.getString("SumUnit")), Integer.valueOf(jsonObject.getString("DateStart")), Integer.valueOf(jsonObject.getString("DateEnd")));

                Toast.makeText(c.getApplicationContext(),c.getResources().getString(R.string.EditApiWebService1), Toast.LENGTH_SHORT).show();

                Intent i=new Intent("del_term");
                LocalBroadcastManager.getInstance(c).sendBroadcast(i);

                dialog.dismiss();


            } else if (editApi == EditApi.API_EDIT_LESSON) {



                sqliteUpdate.Update_lesson(Integer.valueOf(jsonObject.getString("IdLesson")), jsonObject.getString("NameLesson"), Integer.valueOf(jsonObject.getString("CountUnit")), jsonObject.getString("NameTeacher"), Integer.valueOf(jsonObject.getString("DayClass")), jsonObject.getString("ClockClass"), Integer.valueOf(jsonObject.getString("NumberClass")), Integer.valueOf(jsonObject.getString("DateExam")));

                Intent i=new Intent("del_lesson");
                LocalBroadcastManager.getInstance(c).sendBroadcast(i);

                dialog.dismiss();

                Toast.makeText(c.getApplicationContext(), c.getResources().getString(R.string.EditApiWebService2), Toast.LENGTH_SHORT).show();
            } else if (editApi == EditApi.API_EDIT_NOTE) {


                sqliteUpdate.Update_note(Integer.valueOf(jsonObject.getString("IdNote")),Integer.valueOf(jsonObject.getString("IdLesson")),jsonObject.getString("Titer"),jsonObject.getString("Note"));
                Toast.makeText(c.getApplicationContext(),c.getResources().getString(R.string.EditApiWebService3), Toast.LENGTH_SHORT).show();

                Intent i=new Intent("del_note");
                LocalBroadcastManager.getInstance(c).sendBroadcast(i);

                dialog.dismiss();

            } else if (editApi == EditApi.API_EDIT_REMINDER) {


                sqliteUpdate.Update_reminder(Integer.valueOf(jsonObject.getString("IdReminder")), Integer.valueOf(jsonObject.getString("IdLesson")),jsonObject.getString("Titer"),jsonObject.getString("Note"), Integer.valueOf(jsonObject.getString("DateReminder")),jsonObject.getString("ClockReminder"));
                Toast.makeText(c.getApplicationContext(), c.getResources().getString(R.string.EditApiWebService3), Toast.LENGTH_SHORT).show();

                Intent i=new Intent("del_reminder");
                LocalBroadcastManager.getInstance(c).sendBroadcast(i);

                dialog.dismiss();
            } else if (editApi == EditApi.API_EDIT_PROFILE) {

            }

        } catch (Exception ex) {

        }
    }

}
