package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 8/4/2017.
 */

public class TermReportCardJson {

    private String Token;
    private String IdTermReportCard;
    private String IdTerm;
    private String Term;

    public TermReportCardJson(String token, String idTermReportCard, String idTerm, String term) {
        Token = token;
        IdTermReportCard = idTermReportCard;
        IdTerm = idTerm;
        Term = term;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getIdTermReportCard() {
        return IdTermReportCard;
    }

    public void setIdTermReportCard(String idTermReportCard) {
        IdTermReportCard = idTermReportCard;
    }

    public String getIdTerm() {
        return IdTerm;
    }

    public void setIdTerm(String idTerm) {
        IdTerm = idTerm;
    }

    public String getTerm() {
        return Term;
    }

    public void setTerm(String term) {
        Term = term;
    }
}
