package com.hsb.daneshjoo.services.webservice.response;

import com.hsb.daneshjoo.services.webservice.request.ReminderJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb on 6/5/2017.
 */

public class ReminderMessage extends Messageing {

  List<ReminderJson> Reminder=new ArrayList<>();

    public ReminderMessage(String successMessage, String successCode, Integer token, List<ReminderJson> reminder) {
        super(successMessage, successCode, token);
        Reminder = reminder;
    }


    public List<ReminderJson> getReminder() {
        return Reminder;
    }

    public void setReminder(List<ReminderJson> reminder) {
        Reminder = reminder;
    }


    @Override
    public String toString() {
        return "ReminderMessage{" +
                "Reminder=" + Reminder +
                '}';
    }
}
