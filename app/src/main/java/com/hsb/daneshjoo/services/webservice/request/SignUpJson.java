package com.hsb.daneshjoo.services.webservice.request;

import com.hsb.daneshjoo.phone.PhoneInfoModel;

/**
 * Created by MoSiOne70 on 02/06/2017.
 */

public class SignUpJson extends PhoneInfoModel {

    private String token;
    private String username;
    private String email;
    private String password;


    public SignUpJson(String id_Phone_User_Info, String version_Android, String name_Phone, String model_Phone, String manufacturer, String domain, String token, String username, String email, String password) {
        super(id_Phone_User_Info, version_Android, name_Phone, model_Phone, manufacturer, domain);
        this.token = token;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "SignUpJson{" +
                "token='" + token + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
