package com.hsb.daneshjoo.services.webservice.response;

import com.hsb.daneshjoo.services.webservice.request.LessonJson;
import com.hsb.daneshjoo.services.webservice.request.LessonReportCardJson;
import com.hsb.daneshjoo.services.webservice.request.NoteJson;
import com.hsb.daneshjoo.services.webservice.request.ReminderJson;
import com.hsb.daneshjoo.services.webservice.request.TermJson;
import com.hsb.daneshjoo.services.webservice.request.TermReportCardJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb on 7/12/2017.
 */

public class SyncDataMessage extends ProfileMessage {

    List<TermJson> Term=new ArrayList<>();
    List<LessonJson> Lesson=new ArrayList<>();
    List<NoteJson> Note=new ArrayList<>();
    List<ReminderJson> Reminder=new ArrayList<>();
    List<LessonReportCardJson> LessonReportCard=new ArrayList<>();
    List<TermReportCardJson> TermReportCard=new ArrayList<>();


    public SyncDataMessage(String successMessage, String successCode, Integer token, String name, String family, String field, String level, String enteringYear, String age, String sex, String univercityName, List<TermJson> term, List<LessonJson> lesson, List<NoteJson> note, List<ReminderJson> reminder, List<LessonReportCardJson> lessonReportCard, List<TermReportCardJson> termReportCard) {
        super(successMessage, successCode, token, name, family, field, level, enteringYear, age, sex, univercityName);
        Term = term;
        Lesson = lesson;
        Note = note;
        Reminder = reminder;
        LessonReportCard = lessonReportCard;
        TermReportCard = termReportCard;
    }


    public List<TermJson> getTerm() {
        return Term;
    }

    public void setTerm(List<TermJson> term) {
        Term = term;
    }

    public List<LessonJson> getLesson() {
        return Lesson;
    }

    public void setLesson(List<LessonJson> lesson) {
        Lesson = lesson;
    }

    public List<NoteJson> getNote() {
        return Note;
    }

    public void setNote(List<NoteJson> note) {
        Note = note;
    }

    public List<ReminderJson> getReminder() {
        return Reminder;
    }

    public void setReminder(List<ReminderJson> reminder) {
        Reminder = reminder;
    }

    public List<LessonReportCardJson> getLessonReportCard() {
        return LessonReportCard;
    }

    public void setLessonReportCard(List<LessonReportCardJson> lessonReportCard) {
        LessonReportCard = lessonReportCard;
    }

    public List<TermReportCardJson> getTermReportCard() {
        return TermReportCard;
    }

    public void setTermReportCard(List<TermReportCardJson> termReportCard) {
        TermReportCard = termReportCard;
    }


    @Override
    public String toString() {
        return "SyncDataMessage{" +
                "Term=" + Term +
                ", Lesson=" + Lesson +
                ", Note=" + Note +
                ", Reminder=" + Reminder +
                ", LessonReportCard=" + LessonReportCard +
                ", TermReportCard=" + TermReportCard +
                '}';
    }
}
