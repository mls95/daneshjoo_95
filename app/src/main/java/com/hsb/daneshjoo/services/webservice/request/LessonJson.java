package com.hsb.daneshjoo.services.webservice.request;

/**
 * Created by hsb on 6/5/2017.
 */

public class LessonJson  {

    private String Token;
    private String IdTerm;
    private String IdLesson;
    private String NameLesson;
    private String CountUnit;
    private String NameTeacher;
    private String DayClass;
    private String ClockClass;
    private String NumberClass;
    private String DateExam;
    private String PastNameLesson;



    private String IdLessonReportCard;
    private String Grade;

    public LessonJson(String token, String idTerm, String idLesson, String nameLesson, String countUnit, String nameTeacher, String dayClass, String clockClass, String numberClass, String dateExam, String pastNameLesson, String idLessonReportCard, String grade) {
        Token = token;
        IdTerm = idTerm;
        IdLesson = idLesson;
        NameLesson = nameLesson;
        CountUnit = countUnit;
        NameTeacher = nameTeacher;
        DayClass = dayClass;
        ClockClass = clockClass;
        NumberClass = numberClass;
        DateExam = dateExam;
        PastNameLesson = pastNameLesson;
        IdLessonReportCard = idLessonReportCard;
        Grade = grade;
    }


    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getIdTerm() {
        return IdTerm;
    }

    public void setIdTerm(String idTerm) {
        IdTerm = idTerm;
    }

    public String getIdLesson() {
        return IdLesson;
    }

    public void setIdLesson(String idLesson) {
        IdLesson = idLesson;
    }

    public String getNameLesson() {
        return NameLesson;
    }

    public void setNameLesson(String nameLesson) {
        NameLesson = nameLesson;
    }

    public String getCountUnit() {
        return CountUnit;
    }

    public void setCountUnit(String countUnit) {
        CountUnit = countUnit;
    }

    public String getNameTeacher() {
        return NameTeacher;
    }

    public void setNameTeacher(String nameTeacher) {
        NameTeacher = nameTeacher;
    }

    public String getDayClass() {
        return DayClass;
    }

    public void setDayClass(String dayClass) {
        DayClass = dayClass;
    }

    public String getClockClass() {
        return ClockClass;
    }

    public void setClockClass(String clockClass) {
        ClockClass = clockClass;
    }

    public String getNumberClass() {
        return NumberClass;
    }

    public void setNumberClass(String numberClass) {
        NumberClass = numberClass;
    }

    public String getDateExam() {
        return DateExam;
    }

    public void setDateExam(String dateExam) {
        DateExam = dateExam;
    }

    public String getPastNameLesson() {
        return PastNameLesson;
    }

    public void setPastNameLesson(String pastNameLesson) {
        PastNameLesson = pastNameLesson;
    }

    public String getIdLessonReportCard() {
        return IdLessonReportCard;
    }

    public void setIdLessonReportCard(String idLessonReportCard) {
        IdLessonReportCard = idLessonReportCard;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }


    @Override
    public String toString() {
        return "LessonJson{" +
                "Token='" + Token + '\'' +
                ", IdTerm='" + IdTerm + '\'' +
                ", IdLesson='" + IdLesson + '\'' +
                ", NameLesson='" + NameLesson + '\'' +
                ", CountUnit='" + CountUnit + '\'' +
                ", NameTeacher='" + NameTeacher + '\'' +
                ", DayClass='" + DayClass + '\'' +
                ", ClockClass='" + ClockClass + '\'' +
                ", NumberClass='" + NumberClass + '\'' +
                ", DateExam='" + DateExam + '\'' +
                ", PastNameLesson='" + PastNameLesson + '\'' +
                ", IdLessonReportCard='" + IdLessonReportCard + '\'' +
                ", Grade='" + Grade + '\'' +
                '}';
    }
}
