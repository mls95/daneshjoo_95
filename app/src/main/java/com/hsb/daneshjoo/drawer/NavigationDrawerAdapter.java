package com.hsb.daneshjoo.drawer;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hsb.daneshjoo.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by hsb-pc on 1/27/2017.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        if (position==5) {

         // holder.line.setVisibility(View.VISIBLE);
        }

        holder.title.setText(current.getTitle());

        Drawable drawableRed = ContextCompat.getDrawable(context, current.getIcon());
        drawableRed.setBounds(
                drawableRed.getIntrinsicWidth(), // left
                0, // top
                0, // right
                0 // bottom
        );
        holder.title.setCompoundDrawablesWithIntrinsicBounds(drawableRed, null, null, null);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title,line;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            line = (TextView) itemView.findViewById(R.id.line);
        }
    }
}
