package com.hsb.daneshjoo.drawer;

import android.app.Activity;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.home.HomeItem;
import com.hsb.daneshjoo.cards.home.HomeRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.lesson.LessonItem;
import com.hsb.daneshjoo.cards.note.NoteDialog;
import com.hsb.daneshjoo.cards.reminder.ReminderRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.term.TermItem;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.fragment.fragLesson;
import com.hsb.daneshjoo.fragment.fragTerm;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 1/27/2017.
 */
public class HomeFragment extends android.support.v4.app.Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    TextView tvDesc;
    View rootView;
    TextView tvGo;
    KeySetting keySetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        keySetting=new KeySetting(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        tvDesc = (TextView) rootView.findViewById(R.id.fragment_home_desc);
        tvGo = (TextView) rootView.findViewById(R.id.fragment_home_tvGo);

        RefreshRecyclerView();

        SqliteRepo sqliteRepo = new SqliteRepo(getContext());
        if (sqliteRepo.getCountLesson() <= 0) {
            tvDesc.setVisibility(View.VISIBLE);
            tvGo.setVisibility(View.VISIBLE);
            tvGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    android.support.v4.app.FragmentManager fragmentManager = (getActivity().getSupportFragmentManager());
                    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.container_body, new fragTerm());
                    fragmentTransaction.commit();
                }
            });
        }

        // Inflate the layout for this fragment
        return rootView;
    }


    private void RefreshRecyclerView() {
        //tvDesc.setVisibility(View.GONE);
        DbToArrayList dbToArrayList = new DbToArrayList(getActivity());
        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.home_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        HomeRecyclerViewAdapter mAdapter = new HomeRecyclerViewAdapter(dbToArrayList.getDataHome(), rootView.getContext());
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onStart() {

        keySetting.setTopPageHomeFragmet(true);
        if (keySetting.getTopPageHomeFragmet()== false) {

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.title_basic));
        }
        super.onStart();

    }

    @Override
    public void onResume() {


        if (!keySetting.getTopPageHomeFragmet()== false) {

            keySetting.setTopPageHomeFragmet(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.title_basic));
        }

        super.onResume();

    }

    @Override
    public void onPause() {

        keySetting.setTopPageHomeFragmet(false);
        super.onPause();
    }
}
