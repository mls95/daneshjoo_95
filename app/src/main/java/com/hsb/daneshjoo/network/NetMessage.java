package com.hsb.daneshjoo.network;

/**
 * Created by hsb on 6/4/2017.
 */

    public  class NetMessage {

    public static final String NOT_DATA ="دیتا ضعیف است";
    public static final String NOT_INTERNET_CONNECTION ="گوشی به اینترنت متصل نیست";
    public static final String REQUEST_BAD ="دوباره امتحان کنید";
    public static final String REQUEST_OK ="ارسال شد";
    public static final String RESPONSE_OK ="ارسال شد";
    public static final String SAVE_DATA_OFFLINE ="ذخیره شد";

}
