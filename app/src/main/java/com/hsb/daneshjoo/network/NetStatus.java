package com.hsb.daneshjoo.network;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by hsb on 6/3/2017.
 * hamed developer
 */


/**
 *NetStatus is a Java class, this class shows internet network status.
 * Network Wi-Fi and network data phone.
 * Status on or off.
 */

public class NetStatus {


    Context context;

    /**
     * constractor
     * set context
     * @param context
     */
    public NetStatus(Context context)
    {
        this.context=context;
    }

    /**
     * status internet network connection
     * @return boolean
     */
    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            //  Log.v(TAG, "Internet Connection Not Present");
            return false;
        }
    }
}
