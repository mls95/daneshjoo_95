package com.hsb.daneshjoo.cards.term;

/**
 * Created by hsb-pc on 1/29/2017.
 */
public class TermItem {
    private String YEAR;
    private String SUM_UNIT;
    private String DATE_START;
    private String DATE_END;
    private String TERM;
    private int ID;
    private int ID_TERM;


    public TermItem(){}




    public TermItem(int ID,int ID_TERM, String TERM, String SUM_UNIT, String DATE_START, String DATE_END, String YEAR) {
        this.YEAR = YEAR;
        this.SUM_UNIT = SUM_UNIT;
        this.DATE_START = DATE_START;
        this.DATE_END = DATE_END;
        this.TERM = TERM;
        this.ID_TERM=ID_TERM;
        this.ID=ID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID_TERM() {
        return ID_TERM;
    }

    public void setID_TERM(int ID_TERM) {
        this.ID_TERM = ID_TERM;
    }

    public String getTERM() {
        return TERM;
    }

    public void setTERM(String TERM) {
        this.TERM = TERM;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getSUM_UNIT() {
        return SUM_UNIT;
    }

    public void setSUM_UNIT(String SUM_UNIT) {
        this.SUM_UNIT = SUM_UNIT;
    }

    public String getDATE_START() {
        return DATE_START;
    }

    public void setDATE_START(String DATE_START) {
        this.DATE_START = DATE_START;
    }

    public String getDATE_END() {
        return DATE_END;
    }

    public void setDATE_END(String DATE_END) {
        this.DATE_END = DATE_END;
    }


}
