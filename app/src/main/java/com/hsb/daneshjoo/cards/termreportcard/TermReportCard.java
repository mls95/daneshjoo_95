package com.hsb.daneshjoo.cards.termreportcard;

/**
 * Created by hsb on 8/14/2017.
 */

public class TermReportCard {

    private int ID_TERM;
    private int ID_TERM_REPORT_CARD;
    private int TERM;
    private int SUM_UNIT;
    private float AVG_GRADE;

    public TermReportCard(int ID_TERM, int ID_TERM_REPORT_CARD, int TERM, int SUM_UNIT, float AVG_GRADE) {
        this.ID_TERM = ID_TERM;
        this.ID_TERM_REPORT_CARD = ID_TERM_REPORT_CARD;
        this.TERM = TERM;
        this.SUM_UNIT = SUM_UNIT;
        this.AVG_GRADE = AVG_GRADE;
    }


    public int getID_TERM() {
        return ID_TERM;
    }

    public void setID_TERM(int ID_TERM) {
        this.ID_TERM = ID_TERM;
    }

    public int getID_TERM_REPORT_CARD() {
        return ID_TERM_REPORT_CARD;
    }

    public void setID_TERM_REPORT_CARD(int ID_TERM_REPORT_CARD) {
        this.ID_TERM_REPORT_CARD = ID_TERM_REPORT_CARD;
    }

    public int getTERM() {
        return TERM;
    }

    public void setTERM(int TERM) {
        this.TERM = TERM;
    }

    public int getSUM_UNIT() {
        return SUM_UNIT;
    }

    public void setSUM_UNIT(int SUM_UNIT) {
        this.SUM_UNIT = SUM_UNIT;
    }

    public float getAVG_GRADE() {
        return AVG_GRADE;
    }

    public void setAVG_GRADE(float AVG_GRADE) {
        this.AVG_GRADE = AVG_GRADE;
    }


    @Override
    public String toString() {
        return "TermReportCard{" +
                "ID_TERM=" + ID_TERM +
                ", ID_TERM_REPORT_CARD=" + ID_TERM_REPORT_CARD +
                ", TERM=" + TERM +
                ", SUM_UNIT=" + SUM_UNIT +
                ", AVG_GRADE=" + AVG_GRADE +
                '}';
    }
}
