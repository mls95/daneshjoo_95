package com.hsb.daneshjoo.cards.note;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.reminder.ReminderDialog;
import com.hsb.daneshjoo.cards.reminder.Reminderitem;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 2/14/2017.
 */
public class NoteRecyclerViewAdpter  extends RecyclerView.Adapter<NoteRecyclerViewAdpter.NoteItemHolder> {

    private static ArrayList<NoteItem> mDataset;
    Context context;
    Activity activity;
    public static class  NoteItemHolder extends RecyclerView.ViewHolder {
        TextView tv_nameLesson,tv_note,tv_title;
        Button iv_edit,iv_view,iv_delete;
        public NoteItemHolder(View itemView) {
            super(itemView);
            tv_nameLesson=(TextView) itemView.findViewById(R.id.tv_notecardview_valuenameLesson);
            tv_note=(TextView) itemView.findViewById(R.id.tv_notecardview_note);
            tv_title=(TextView) itemView.findViewById(R.id.tv_notecardview_valuetitle);

            iv_edit=(Button) itemView.findViewById(R.id.btn_notecardview_edit);
            iv_view=(Button) itemView.findViewById(R.id.btn_notecardview_view);
            iv_delete=(Button) itemView.findViewById(R.id.btn_notecardview_delete);
        }
    }


    @Override
    public NoteItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.note_cardview,parent,false);
        NoteItemHolder noteItemHolder=new NoteItemHolder(view);
        return noteItemHolder;
    }

    @Override
    public void onBindViewHolder(final NoteItemHolder holder, int position) {

        holder.tv_nameLesson.setText(mDataset.get(position).getNAME_LESSON());
        holder.tv_nameLesson.setTag(mDataset.get(position).getID_LESSON());
        holder.tv_title.setText(mDataset.get(position).getTITLE());
        holder.tv_note.setText(mDataset.get(position).getNOTE());
        holder.tv_note.setTag(mDataset.get(position).getID_NOTE());
        holder.tv_title.setTag(mDataset.get(position).getID());

        holder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoteDialog noteDialog=new NoteDialog((Activity) context,(Integer) holder.tv_title.getTag(),"view-note");
                noteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                noteDialog.show();
            }
        });

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utility.setIdTerm((Integer) holder.tv_title.getTag());

                utility.setId(Integer.valueOf(holder.tv_note.getTag().toString()));

                utility.dialog(context.getResources().getString(R.string.NoteRecyclerViewAdapter1), context.getResources().getString(R.string.NoteRecyclerViewAdapter2), context.getResources().getString(R.string.NoteRecyclerViewAdapter3), context.getResources().getString(R.string.NoteRecyclerViewAdapter4), "delete-note", context,activity);

            }
        });

        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoteDialog noteDialog=new NoteDialog((Activity) context,(Integer) holder.tv_title.getTag(),"update-note");
                noteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                noteDialog.show();
            }
        });
    }

    public NoteRecyclerViewAdpter(ArrayList<NoteItem> myDataset,Context context,Activity activity) {

        mDataset = myDataset;
        this.context=context;
        this.activity=activity;
    }
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
