package com.hsb.daneshjoo.cards.reminder;

/**
 * Created by hsb-pc on 2/11/2017.
 */
public class LessonItem {
    private int ID;
    private String NAME_LESSON;

    public LessonItem(int ID, String NAME_LESSON) {
        this.ID = ID;
        this.NAME_LESSON = NAME_LESSON;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNAME_LESSON() {
        return NAME_LESSON;
    }

    public void setNAME_LESSON(String NAME_LESSON) {
        this.NAME_LESSON = NAME_LESSON;
    }
}
