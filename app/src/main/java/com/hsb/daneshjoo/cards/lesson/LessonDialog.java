package com.hsb.daneshjoo.cards.lesson;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.hsb.daneshjoo.PersianDateDialog;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.TimeDialog;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.db.SqliteUpdate;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.EditApi;
import com.hsb.daneshjoo.services.EditApiWebService;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.LessonJson;
import com.hsb.daneshjoo.services.webservice.request.TokenJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.services.webservice.response.TermMessage;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 2/3/2017.
 */
public class LessonDialog extends Dialog implements View.OnClickListener {
    Activity a;
    int ID_TERM;

    EditText et_namelesson, et_countunit, et_nameteacher, et_numberclass;
    TextView et_clockcalss ,et_dateexam;
    Spinner sp_dayclass;
    TextView tv_namelesson, tv_countunit, tv_nameteacher, tv_examdate, tv_dayclass, tv_clockclass, tv_numberclass;
    Button btn_save, btn_cancel;
    utility utilit;
    List<String> list;
    ArrayAdapter<String> adapter;
    LoadingProgress loadingProgress;

    String token = "";
    String examDate = "";
    String _PastNameLesson = "";

    int Id_Lesson = 0;
    int _ID_LESSON = 0;
    int day = 0;
    SqliteRepo repo;
    PersianDateDialog persianDateDialog;

    public LessonDialog(Activity activity, int ID_TERM) {
        super(activity);
        a = activity;
        utilit = new utility(a);
        this.ID_TERM = ID_TERM;
    }

    public LessonDialog(Activity activity, int Id_Lesson, String token) {
        super(activity);
        a = activity;
        utilit = new utility(a);
        this.token = token;
        this.Id_Lesson = Id_Lesson;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // initlazeion
        loadingProgress = new LoadingProgress(a);
        repo = new SqliteRepo(a);
        persianDateDialog = new PersianDateDialog(a);

        if (this.token == "update-lesson") {
            setContentView(R.layout.fragment_dialog_lesson);
            setCanceledOnTouchOutside(false);
            Initilize();
            initSpinner();
            ValueShow();
        } else if (this.token == "view-lesson") {
            setContentView(R.layout.dialog_view_lesson);
            setCanceledOnTouchOutside(false);
            Initilize_view();
            ValueShow();
        } else {
            setContentView(R.layout.fragment_dialog_lesson);
            setCanceledOnTouchOutside(false);
            Initilize();
            initSpinner();
        }

    }

    private void Initilize() {

        et_countunit = (EditText) findViewById(R.id.et_dialog_lesson_counUnit);
        et_clockcalss = (TextView) findViewById(R.id.et_fragment_lesson_clockClass);
        et_clockcalss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeDialog timeDialog = new TimeDialog(a, et_clockcalss);
                timeDialog.show();
            }
        });
        et_numberclass = (EditText) findViewById(R.id.et_fragment_lesson_numberClass);
        et_dateexam = (TextView) findViewById(R.id.et_dialog_lesson_dateExam);
        et_dateexam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersianDateDialog persianDateDialog = new PersianDateDialog(a);
                examDate = persianDateDialog.getStringDate(et_dateexam);
            }
        });


        et_namelesson = (EditText) findViewById(R.id.et_dialog_lesson_nameLesson);
        et_nameteacher = (EditText) findViewById(R.id.et_dialog_lesson_name_teacher);

        sp_dayclass = (Spinner) findViewById(R.id.sp_dialog_lesson_dayClass);
        sp_dayclass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                day = sp_dayclass.getSelectedItemPosition();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_cancel = (Button) findViewById(R.id.btn_dialog_lesson_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btn_save = (Button) findViewById(R.id.btn_dialog_lesson_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValidation()) {
                    int unit =Integer.valueOf(et_countunit.getText().toString());
                    unit+=repo.getCountLesson(utility.getIdTerm());

                    if (unit>repo.getSumUnit(utility.getIdTerm())){
                        utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.LessonDialog15), a);
                    }else{
                        if (token == "update-lesson") {
                            LessonJson lessonJson = new LessonJson(String.valueOf(repo.getIdUser()), String.valueOf(utility.getIdTerm()), String.valueOf(_ID_LESSON), et_namelesson.getText().toString(), et_countunit.getText().toString(), et_nameteacher.getText().toString(), String.valueOf(day), et_clockcalss.getTag().toString(), et_numberclass.getText().toString(), et_dateexam.getTag().toString(), _PastNameLesson, "", "");
                            EditApiWebService webService = new EditApiWebService(a);
                            webService.RunApi(EditApi.API_EDIT_LESSON, lessonJson, LessonDialog.this);
                        } else {
                            RunApiLesson();
                        }
                    }

                }



            }
        });
    }

    private void Initilize_view() {

        tv_namelesson = (TextView) findViewById(R.id.tv_dialog_lesson_valuenameLesson);
        tv_countunit = (TextView) findViewById(R.id.tv_dialog_lesson_valuecountUnit);
        tv_nameteacher = (TextView) findViewById(R.id.tv_dialog_lesson_valuenameTeacher);
        tv_dayclass = (TextView) findViewById(R.id.tv_dialog_lesson_valuedayClass);
        tv_examdate = (TextView) findViewById(R.id.tv_dialog_lesson_valueexamDate);
        tv_numberclass = (TextView) findViewById(R.id.tv_dialog_lesson_valuenumberClass);
        tv_clockclass = (TextView) findViewById(R.id.tv_dialog_lesson_valueclockClass);

        btn_cancel = (Button) findViewById(R.id.btn_dialogview_lesson_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    private boolean isValidation() {
        if (et_namelesson.getText().toString().isEmpty() || et_namelesson.getText().toString().length() <= 0) {
            et_namelesson.setError(getContext().getResources().getString(R.string.LessonDialog1));
        } else if (et_countunit.getText().toString().equals("") || et_countunit.getText().toString().isEmpty() || et_countunit.getText().toString().length() <= 0) {
            et_countunit.setError(getContext().getResources().getString(R.string.LessonDialog2));
        } else if (Integer.valueOf(et_countunit.getText().toString()) <= 0) {
            et_countunit.setError(getContext().getResources().getString(R.string.LessonDialog3));
        } else if (Integer.valueOf(et_countunit.getText().toString()) > 4 || Integer.valueOf(et_countunit.getText().toString()) <= 0) {
            et_countunit.setError(getContext().getResources().getString(R.string.LessonDialog4));
        } else if (et_nameteacher.getText().toString().isEmpty() || et_nameteacher.getText().length() <= 0) {
            et_nameteacher.setError(getContext().getResources().getString(R.string.LessonDialog5));
        } else if (et_dateexam.getText().toString().equals(a.getResources().getString(R.string.Lesson_Exam_date))) {
            utility.dialog(getContext().getResources().getString(R.string.LessonDialog6),getContext().getResources().getString(R.string.Message),a);
        } else if (et_numberclass.getText().toString().isEmpty() || et_numberclass.getText().length() <= 0  ) {
            et_numberclass.setError(getContext().getResources().getString(R.string.LessonDialog7));
        } else if (et_numberclass.getText().toString().length() >= 4) {
            et_numberclass.setError(getContext().getResources().getString(R.string.LessonDialog8));
        } else if (et_clockcalss.getText().toString().equals(a.getResources().getString(R.string.Lesson_Start_time))) {
            utility.dialog(getContext().getResources().getString(R.string.LessonDialog9),getContext().getResources().getString(R.string.Message),a);
        } else {
            return true;
        }
        return false;
    }


    private void initSpinner() {
        list = new LinkedList<>();
        list.add(getContext().getResources().getString(R.string.Saturday));
        list.add(getContext().getResources().getString(R.string.Sunday));
        list.add(getContext().getResources().getString(R.string.Monday));
        list.add(getContext().getResources().getString(R.string.Tuesday));
        list.add(getContext().getResources().getString(R.string.Wednesday));
        list.add(getContext().getResources().getString(R.string.Thursday));


        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_dayclass.setAdapter(adapter);
    }


    private void ValueShow() {


        Cursor cur = repo.Repo_Idlesson(this.Id_Lesson);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    String h = "";
                    String m = "";
                    if (this.token == "update-lesson") {
                        _ID_LESSON = cur.getInt(3);
                        et_namelesson.setText(cur.getString(4));
                        _PastNameLesson = cur.getString(4);
                        et_countunit.setText(String.valueOf(cur.getInt(5)));
                        et_nameteacher.setText(cur.getString(6));
                        sp_dayclass.setSelection(cur.getInt(7));
                        h = String.valueOf(cur.getString(8)).substring(0, 2);
                        m = String.valueOf(cur.getString(8)).substring(2, 4);
                        et_clockcalss.setText(h + ":" + m);
                        et_clockcalss.setTag(cur.getString(8));
                        et_numberclass.setText(String.valueOf(cur.getInt(9)));
                        et_dateexam.setText(persianDateDialog.setDate(cur.getString(10)));
                        et_dateexam.setTag(cur.getString(10));

                        btn_save.setText(getContext().getResources().getString(R.string.LessonDialog10));
                    } else if (this.token == "view-lesson") {

                        _ID_LESSON = cur.getInt(3);
                        tv_namelesson.setText(cur.getString(4));
                        tv_countunit.setText(String.valueOf(cur.getInt(5)));
                        tv_nameteacher.setText(cur.getString(6));
                        tv_dayclass.setText(utilit.getNameDay(cur.getInt(7)));
                        h = String.valueOf(cur.getString(8)).substring(0, 2);
                        m = String.valueOf(cur.getString(8)).substring(2, 4);
                        tv_clockclass.setText(h + ":" + m);

                        tv_numberclass.setText(String.valueOf(cur.getInt(9)));
                        tv_examdate.setText(persianDateDialog.setDate(cur.getString(10)));

                    }


                } while (cur.moveToNext());

            }

        }

    }


    @Override
    public void onClick(View view) {

    }


    private void RefreshRecyclerView() {
        DbToArrayList dbToArrayList = new DbToArrayList(a);
        RecyclerView mRecyclerView = (RecyclerView) a.findViewById(R.id.lesson_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(a.getApplicationContext()));
        LessonRecyclerViewAdapter mAdapter = new LessonRecyclerViewAdapter(dbToArrayList.getDataLesson(), a, a);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void RunApiLesson() {

        /**
         * RESTFUL API LESSON
         */
        //Check exeist username

        if (repo.Repo_account_Iduser() > 0) {

            NetStatus netStatus = new NetStatus(a);
            if (netStatus.checkInternetConnection()) {
                // internet connection

                try {

                    // build web service
                    loadingProgress.Show();
                    // generate json

                    UUID single = UUID.randomUUID();
                    final Integer id_lesson = Math.abs(single.hashCode());


                    final Integer id_lesson_report_card = Math.abs(single.hashCode())+1024;

                    LessonJson lessonJson = new LessonJson(String.valueOf(repo.getIdUser()), String.valueOf(utility.getIdTerm()), String.valueOf(id_lesson), et_namelesson.getText().toString(), et_countunit.getText().toString(), et_nameteacher.getText().toString(), String.valueOf(day), et_clockcalss.getTag().toString(), et_numberclass.getText().toString(), et_dateexam.getTag().toString(), "", String.valueOf(id_lesson_report_card), "");


                    StoreClient client = RestClient.getClient().create(StoreClient.class);
                    Call<Messageing> call = client.postinsertlesson(lessonJson);

                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                            loadingProgress.Close();
                            if (response.code() == 200) {
                                if (Integer.valueOf(response.body().getSuccessCode().toString()) == 0) {
                                    utility.dialog(getContext().getResources().getString(R.string.LessonDialog11), getContext().getResources().getString(R.string.LessonDialog12), a);
                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 1) {

                                    SqliteRepo sqliteRepo = new SqliteRepo(a);
                                    SqliteInsert insert = new SqliteInsert(a);

                                    insert.ADD_lesson(response.body().getToken(), utility.getIdTerm(), et_namelesson.getText().toString(), id_lesson, Integer.valueOf(et_countunit.getText().toString()), et_nameteacher.getText().toString(), day, et_clockcalss.getTag().toString(), Integer.valueOf(et_numberclass.getText().toString()), Integer.valueOf(et_dateexam.getTag().toString()));
                                    insert.ADD_lesson_report_card(response.body().getToken(), utility.getIdTerm(), id_lesson_report_card, id_lesson, 0);
                                    RefreshRecyclerView();

                                    dismiss();
                                    Toast.makeText(a, getContext().getResources().getString(R.string.LessonDialog13), Toast.LENGTH_SHORT).show();

                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 202) {
                                    utility.dialog(getContext().getResources().getString(R.string.LessonDialog11), getContext().getResources().getString(R.string.LessonDialog12), a);

                                }
                            } else {
                                utility.dialog(getContext().getResources().getString(R.string.LessonDialog11), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD), a);


                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            try {
                                loadingProgress.Close();

                                Gson gson1 = new Gson();
                                JsonReader reader = new JsonReader(new StringReader(t.getMessage()));
                                reader.setLenient(true);
                                Messageing ms = gson1.fromJson(t.getMessage(), Messageing.class);

                                utility.dialog(getContext().getResources().getString(R.string.LessonDialog11), getContext().getResources().getString(R.string.LessonDialog14), a);
                            } catch (Exception e) {
                                loadingProgress.Close();
                            }
                        }
                    });

                } catch (Exception e) {
                    loadingProgress.Close();
                    e.printStackTrace();
                }
            } else {
                // not internet connection
                loadingProgress.Close();
                utility.dialog(getContext().getResources().getString(R.string.LessonDialog11), getContext().getResources().getString(R.string.NetMessage_NOT_INTERNET_CONNECTION), a);
            }
        } else {
            loadingProgress.Close();
            utility.dialog(getContext().getResources().getString(R.string.LessonDialog11), getContext().getResources().getString(R.string.LessonDialog12), a);
        }
    }
}
