package com.hsb.daneshjoo.cards.home;

/**
 * Created by hsb-pc on 2/10/2017.
 */
public class HomeItem {

    private int ID;
    private int ID_TERM;
    private String NAME_LESSON;
    private String NAME_TEACHER;
    private String DAY_CLASS;
    private int DAY_WEEK;
    private String CLOCK_CLASS;
    private String NUMBER_CLASS;
    private String EXAM_DATE;


    public HomeItem(int ID, int ID_TERM, String NAME_LESSON, String NAME_TEACHER,int DAY_WEEK, String DAY_CLASS, String CLOCK_CLASS, String NUMBER_CLASS, String EXAM_DATE) {
        this.ID = ID;
        this.ID_TERM = ID_TERM;
        this.NAME_LESSON = NAME_LESSON;
        this.NAME_TEACHER = NAME_TEACHER;
        this.DAY_WEEK=DAY_WEEK;
        this.DAY_CLASS = DAY_CLASS;
        this.CLOCK_CLASS = CLOCK_CLASS;
        this.NUMBER_CLASS=NUMBER_CLASS;
        this.EXAM_DATE = EXAM_DATE;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID_TERM() {
        return ID_TERM;
    }

    public void setID_TERM(int ID_TERM) {
        this.ID_TERM = ID_TERM;
    }

    public String getNAME_LESSON() {
        return NAME_LESSON;
    }

    public void setNAME_LESSON(String NAME_LESSON) {
        this.NAME_LESSON = NAME_LESSON;
    }

    public String getNAME_TEACHER() {
        return NAME_TEACHER;
    }

    public void setNAME_TEACHER(String NAME_TEACHER) {
        this.NAME_TEACHER = NAME_TEACHER;
    }

    public String getDAY_CLASS() {
        return DAY_CLASS;
    }

    public void setDAY_CLASS(String DAY_CLASS) {
        this.DAY_CLASS = DAY_CLASS;
    }

    public String getCLOCK_CLASS() {
        return CLOCK_CLASS;
    }

    public void setCLOCK_CLASS(String CLOCK_CLASS) {
        this.CLOCK_CLASS = CLOCK_CLASS;
    }

    public String getNUMBER_CLASS() {
        return NUMBER_CLASS;
    }

    public void setNUMBER_CLASS(String NUMBER_CLASS) {
        this.NUMBER_CLASS = NUMBER_CLASS;
    }

    public String getEXAM_DATE() {
        return EXAM_DATE;
    }

    public void setEXAM_DATE(String EXAM_DATE) {
        this.EXAM_DATE = EXAM_DATE;
    }

    public int getDAY_WEEK() {
        return DAY_WEEK;
    }

    public void setDAY_WEEK(int DAY_WEEK) {
        this.DAY_WEEK = DAY_WEEK;
    }
}
