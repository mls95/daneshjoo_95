package com.hsb.daneshjoo.cards.lessonreportcard;

/**
 * Created by hsb on 8/14/2017.
 */

public class LessonReportCardItem {

    private int ID_LESSON_REPORT_CARD;
    private int ID_LESSON;
    private int ID_TERM;
    private int COUNT_UNIT;
    private String NAME_LESSON;
    private float GRADE;


    public LessonReportCardItem(int ID_LESSON_REPORT_CARD, int ID_LESSON, int ID_TERM, int COUNT_UNIT, String NAME_LESSON, float GRADE) {
        this.ID_LESSON_REPORT_CARD = ID_LESSON_REPORT_CARD;
        this.ID_LESSON = ID_LESSON;
        this.ID_TERM = ID_TERM;
        this.COUNT_UNIT = COUNT_UNIT;
        this.NAME_LESSON = NAME_LESSON;
        this.GRADE = GRADE;
    }


    public int getID_LESSON_REPORT_CARD() {
        return ID_LESSON_REPORT_CARD;
    }

    public void setID_LESSON_REPORT_CARD(int ID_LESSON_REPORT_CARD) {
        this.ID_LESSON_REPORT_CARD = ID_LESSON_REPORT_CARD;
    }

    public int getID_LESSON() {
        return ID_LESSON;
    }

    public void setID_LESSON(int ID_LESSON) {
        this.ID_LESSON = ID_LESSON;
    }

    public int getID_TERM() {
        return ID_TERM;
    }

    public void setID_TERM(int ID_TERM) {
        this.ID_TERM = ID_TERM;
    }

    public int getCOUNT_UNIT() {
        return COUNT_UNIT;
    }

    public void setCOUNT_UNIT(int COUNT_UNIT) {
        this.COUNT_UNIT = COUNT_UNIT;
    }

    public String getNAME_LESSON() {
        return NAME_LESSON;
    }

    public void setNAME_LESSON(String NAME_LESSON) {
        this.NAME_LESSON = NAME_LESSON;
    }

    public float getGRADE() {
        return GRADE;
    }

    public void setGRADE(float GRADE) {
        this.GRADE = GRADE;
    }
}
