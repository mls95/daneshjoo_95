package com.hsb.daneshjoo.cards.note;

import android.provider.ContactsContract;

/**
 * Created by hsb-pc on 2/14/2017.
 */
public class NoteItem {

    private int ID;
    private int ID_LESSON;
    private String ID_NOTE;
    private String NAME_LESSON;
    private String TITLE;
    private String NOTE;

    public NoteItem(int ID, int ID_LESSON,String ID_NOTE, String NAME_LESSON, String TITLE, String NOTE) {
        this.ID = ID;
        this.ID_LESSON = ID_LESSON;
        this.ID_NOTE = ID_NOTE;
        this.NAME_LESSON = NAME_LESSON;
        this.TITLE = TITLE;
        this.NOTE = NOTE;
    }

    public String getID_NOTE() {
        return ID_NOTE;
    }

    public void setID_NOTE(String ID_NOTE) {
        this.ID_NOTE = ID_NOTE;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID_LESSON() {
        return ID_LESSON;
    }

    public void setID_LESSON(int ID_LESSON) {
        this.ID_LESSON = ID_LESSON;
    }

    public String getNAME_LESSON() {
        return NAME_LESSON;
    }

    public void setNAME_LESSON(String NAME_LESSON) {
        this.NAME_LESSON = NAME_LESSON;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getNOTE() {
        return NOTE;
    }

    public void setNOTE(String NOTE) {
        this.NOTE = NOTE;
    }
}
