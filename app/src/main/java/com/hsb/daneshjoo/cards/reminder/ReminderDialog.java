package com.hsb.daneshjoo.cards.reminder;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.PersianDateDialog;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.TimeDialog;
import com.hsb.daneshjoo.cards.note.NoteRecyclerViewAdpter;
import com.hsb.daneshjoo.cards.term.TermItem;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.date.CalendarTool;
import com.hsb.daneshjoo.date.DateTime;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.db.SqliteUpdate;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.EditApi;
import com.hsb.daneshjoo.services.EditApiWebService;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.NoteJson;
import com.hsb.daneshjoo.services.webservice.request.ReminderJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 2/10/2017.
 */
public class ReminderDialog extends Dialog {

    Activity a;
    Button btn_cancel, btn_save;
    EditText et_title, et_note;
    TextView tv_nameLesson, tv_clock, tv_date, tv_title, tv_note;
    TextView et_date, et_clock;
    Spinner sp_namelesson, sp_term;
    ArrayAdapter adapter;
    int idlesson = 0;
    int idterm = 0;
    int idReminder = 0;
    int _IDREMINDER = 0;
    List<Integer> listidlesson;
    List<String> listnamelesson;
    List<String> listterm;
    private String token;

    SqliteRepo repo;
    PersianDateDialog persianDateDialog;
    LoadingProgress loadingProgress;
    KeySetting keySetting;

    public ReminderDialog(Activity a, String token) {
        super(a);
        this.a = a;
        this.token = token;
    }

    public ReminderDialog(Activity a, int idReminder, String token) {
        super(a);
        this.a = a;
        this.token = token;
        this.idReminder = idReminder;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        loadingProgress = new LoadingProgress(a);
        repo = new SqliteRepo(a.getApplicationContext());
        persianDateDialog = new PersianDateDialog(a);
        keySetting = new KeySetting(a);

        if (this.token == "view-reminder") {
            setCanceledOnTouchOutside(true);
            setContentView(R.layout.dialog_view_reminder);
            Initilize_view();
            ValueShow();
        } else if (this.token == "update-reminder") {
            setCanceledOnTouchOutside(true);
            setContentView(R.layout.dialog_note_reminder);

            Initialize();
            setValueSpinnerTerm();
            setSpinnerValue(Integer.valueOf(keySetting.getTerm()));
            ValueShow();
            setListnamelesson();
        } else {
            setCanceledOnTouchOutside(true);
            setContentView(R.layout.dialog_note_reminder);
            Initialize();
            setValueSpinnerTerm();
            setSpinnerValue(Integer.valueOf(keySetting.getTerm()));
            setListnamelesson();
        }


    }

    private void Initilize_view() {

        tv_nameLesson = (TextView) findViewById(R.id.tv_dialog_view_reminder_valuenameLesson);
        tv_date = (TextView) findViewById(R.id.tv_dialog_view_reminder_valuedate);
        tv_clock = (TextView) findViewById(R.id.tv_dialog_view_reminder_valueclock);
        tv_title = (TextView) findViewById(R.id.tv_dialog_view_reminder_valuetitle);
        tv_note = (TextView) findViewById(R.id.tv_dialog_view_reminder_valuennote);


        btn_cancel = (Button) findViewById(R.id.btn_dialog_view_reminder_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    private void Initialize() {
        et_title = (EditText) findViewById(R.id.et_dialog_notereminder_titer);
        et_note = (EditText) findViewById(R.id.et_dialog_notereminder_note);
        et_date = (TextView) findViewById(R.id.et_dialog_notereminder_date);
        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                persianDateDialog.getStringDate(et_date);
            }
        });
        et_clock = (TextView) findViewById(R.id.et_dialog_notereminder_clock);
        et_clock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeDialog timeDialog = new TimeDialog(a, et_clock);
                timeDialog.show();
            }
        });

        sp_term = (Spinner) findViewById(R.id.sp_dialog_notereminder_term);
        sp_term.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idterm = sp_term.getSelectedItemPosition();
                idterm = Integer.valueOf(listterm.get(idterm));
                setSpinnerValue(idterm);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_namelesson = (Spinner) findViewById(R.id.sp_dialog_notereminder_nameLesson);
        sp_namelesson.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idlesson = sp_namelesson.getSelectedItemPosition();
                idlesson = listidlesson.get(idlesson);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_cancel = (Button) findViewById(R.id.btn_dialog_notereminder_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btn_save = (Button) findViewById(R.id.btn_dialog_notereminder_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidation()) {
                        /*SqliteRepo sqliteRepo =new SqliteRepo(getContext());
                        sqliteRepo.getIdUser();*/

                    if (token == "update-reminder") {
                        ReminderJson reminderJson = new ReminderJson(String.valueOf(repo.getIdUser()), String.valueOf(idlesson), String.valueOf(idterm), String.valueOf(_IDREMINDER), String.valueOf("1"), et_title.getText().toString(), "", et_note.getText().toString(), et_date.getTag().toString(), et_clock.getTag().toString());
                        EditApiWebService webService = new EditApiWebService(a);
                        webService.RunApi(EditApi.API_EDIT_REMINDER, reminderJson, ReminderDialog.this);
                    } else {
                        RunApiReminder();
                    }
                }
            }
        });

    }

    private boolean isValidation() {
        if (et_title.getText().toString().isEmpty() || et_title.getText().toString().length() <= 0) {
            //utility.dialog("پیام","عنوان را وارد کنید",a);
            et_title.setError(a.getResources().getString(R.string.ReminderDialog_Error_TypeTitle));
        } else if (et_note.getText().toString().equals("") || et_note.getText().toString().isEmpty() || et_note.getText().toString().length() <= 0) {
            // utility.dialog("پیام","متن را وارد کنید",a);
            et_note.setError(a.getResources().getString(R.string.ReminderDialog_Error_TypeText));
        } else if (et_date.getText().toString().equals(a.getResources().getString(R.string.Dia_Reminder_Date))) {
            utility.dialog(a.getResources().getString(R.string.Message),a.getResources().getString(R.string.ReminderDialog_Error_EnterDate),a);
           /* et_date.setError("تاریخ را وارد کنید");*/
        } else if (et_clock.getText().toString().equals(a.getResources().getString(R.string.Dia_Reminder_Time))) {
             utility.dialog(a.getResources().getString(R.string.Message),a.getResources().getString(R.string.ReminderDialog_Error_EnterTime),a);
            /*et_clock.setError("ساعت را وارد کنید");*/
        } else {
            return true;
        }
        return false;
    }


    private void ValueShow() {

        SqliteRepo repo = new SqliteRepo(a.getApplicationContext());
        Cursor cur = repo.Repo_reminder(this.idReminder);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    String h = "";
                    String m = "";

                    if (this.token == "update-reminder") {

                        Log.i("POSITION : ",listnamelesson.indexOf(repo.getNameLesson(cur.getInt(2)))+"");
                        //sp_namelesson.setSelection(adapter.getPosition(repo.getNameLesson(cur.getInt(2))));

                        _IDREMINDER = cur.getInt(3);
                        et_title.setText(cur.getString(5));
                        et_note.setText(cur.getString(6));
                        h = String.valueOf(cur.getString(8)).substring(0, 2);
                        m = String.valueOf(cur.getString(8)).substring(2, 4);
                        et_clock.setText(h + ":" + m);
                        et_clock.setTag(cur.getString(8));
                        et_date.setText(persianDateDialog.setDate(cur.getString(7)));
                        et_date.setTag(cur.getString(7));
                        btn_save.setText(getContext().getResources().getString(R.string.LessonDialog10) );
                    } else if (this.token == "view-reminder") {

                        _IDREMINDER = cur.getInt(3);
                        tv_nameLesson.setText(repo.getNameLesson(2));
                        tv_title.setText(cur.getString(5));
                        tv_note.setText(cur.getString(6));
                        h = String.valueOf(cur.getString(8)).substring(0, 2);
                        m = String.valueOf(cur.getString(8)).substring(2, 4);
                        tv_clock.setText(h + ":" + m);

                        tv_date.setText(persianDateDialog.setDate(cur.getString(7)));

                    }


                } while (cur.moveToNext());

            }

        }

    }


    private void setValueSpinnerTerm() {

        listterm = new ArrayList<>();


        SqliteRepo sqliteRq = new SqliteRepo(getContext());
        Cursor cur = sqliteRq.Repo_getterm();
        int i = 0;
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {


                    listterm.add(String.valueOf(cur.getInt(0)));


                    i++;
                } while (cur.moveToNext());
            }
        }


        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listterm);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_term.setAdapter(adapter);
    }


    private void setSpinnerValue(int idterm) {
        Log.i("utility idterm",String.valueOf(idterm));
         listnamelesson = new ArrayList<>();
        listidlesson = new ArrayList<>();

        SqliteRepo sqliteRq = new SqliteRepo(getContext());
        Cursor cur = sqliteRq.Repo_ListNameLesson(idterm);
        int i = 0;
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {


                    listnamelesson.add(cur.getString(1));
                    listidlesson.add(cur.getInt(2));

                    i++;
                } while (cur.moveToNext());
            }
        }


        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listnamelesson);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_namelesson.setAdapter(adapter);
        Log.i("utility",utility.getNameLesson());
        sp_namelesson.setSelection(adapter.getPosition(utility.getNameLesson()));

    }

    private void setListnamelesson(){
        sp_namelesson.setSelection(adapter.getPosition(utility.getNameLesson()));
        Log.i("listterm",listterm.indexOf(String.valueOf(keySetting.getTerm()))+"");
        sp_term.setSelection(listterm.indexOf(String.valueOf(keySetting.getTerm())));
    }
    public void RefreshRecyclerView() {
        if (token != "home") {
            DbToArrayList dbToArrayList = new DbToArrayList(a);
            RecyclerView mRecyclerView = (RecyclerView) a.findViewById(R.id.reminder_recycler_view);
            mRecyclerView.setHasFixedSize(true);
            // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
            mRecyclerView.setLayoutManager(new LinearLayoutManager(a.getApplicationContext()));
            ReminderRecyclerViewAdapter mAdapter = new ReminderRecyclerViewAdapter(dbToArrayList.getDataReminder(), a, a);
            mRecyclerView.setAdapter(mAdapter);
        }
    }


    private void RunApiReminder() {

        /**
         * RESTFUL API REMINDER
         */
        //Check exeist username

        if (repo.Repo_account_Iduser() > 0) {

            NetStatus netStatus = new NetStatus(a.getApplicationContext());
            if (netStatus.checkInternetConnection()) {
                // internet connection

                try {

                    // build web service

                    loadingProgress.Show();
                    // generate json
                    UUID uuid = UUID.randomUUID();
                    final int id_reminder = Math.abs(uuid.hashCode());
                    ReminderJson reminderJson = new ReminderJson(String.valueOf(repo.getIdUser()), String.valueOf(idlesson), String.valueOf(idterm), String.valueOf(id_reminder), String.valueOf("1"), et_title.getText().toString(), null, et_note.getText().toString(), et_date.getTag().toString(), et_clock.getTag().toString());
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();
                    String json = gson.toJson(reminderJson);
                    Log.i("reminderjson",gson.toJson(reminderJson));
                    StoreClient client = RestClient.getClient().create(StoreClient.class);
                    Call<Messageing> call = client.postinsertreminder(reminderJson);
                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                            loadingProgress.Close();
                            if (response.code() == 200) {
                                if (Integer.valueOf(response.body().getSuccessCode().toString()) == 0) {
                                    utility.dialog(a.getResources().getString(R.string.Message), a.getResources().getString(R.string.FragSignin_Message_TrayAgin), a.getApplicationContext());
                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 1) {
                                    SqliteInsert sqliteInsert = new SqliteInsert(getContext());
                                    SqliteRepo sqliteRepo = new SqliteRepo(getContext());
                                    sqliteInsert.ADD_noteReminder(response.body().getToken(), idlesson, id_reminder, 1, et_title.getText().toString(), et_note.getText().toString(), Integer.valueOf(et_date.getTag().toString()), et_clock.getTag().toString(), idterm);

                                    RefreshRecyclerView();

                                    dismiss();
                                    Toast.makeText(a.getApplicationContext(), a.getResources().getString(R.string.ReminderDialog_Message_SaveNewNoteReminder), Toast.LENGTH_SHORT).show();

                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 202) {
                                    utility.dialog(a.getResources().getString(R.string.Message), a.getResources().getString(R.string.FragSignin_Message_TrayAgin), a.getApplicationContext());

                                }
                            } else {
                                utility.dialog(a.getResources().getString(R.string.Message), a.getResources().getString(R.string.NetMessage_REQUEST_BAD), a.getApplicationContext());


                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();
                            utility.dialog(a.getResources().getString(R.string.Message), a.getResources().getString(R.string.FragSignin_Message_TrayAgin), a.getApplicationContext());
                        }
                    });


                } catch (Exception e) {
                    loadingProgress.Close();
                    e.printStackTrace();
                }

            } else {
                // not internet connection
                loadingProgress.Close();
                utility.dialog(a.getResources().getString(R.string.Message), a.getResources().getString(R.string.NOT_INTERNET_CONNECTION), a);

            }


        } else {
            loadingProgress.Close();
            utility.dialog(a.getResources().getString(R.string.Message), a.getResources().getString(R.string.FragSignin_Message_TrayAgin), a.getApplicationContext());

        }


    }
}
