package com.hsb.daneshjoo.cards.lessonreportcard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.term.TermDialog;
import com.hsb.daneshjoo.cards.term.TermRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.termreportcard.TermReportCard;
import com.hsb.daneshjoo.cards.termreportcard.TermReportCardRecyclerViewAdapter;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.HelpShowCaseView;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

import static java.security.AccessController.getContext;

/**
 * Created by hsb on 8/14/2017.
 */

public class LessonReportCardRecyclerViewAdapter extends RecyclerView.Adapter<LessonReportCardRecyclerViewAdapter.LessonReportCardItemHolder> {

    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<LessonReportCardItem> mDataset;
    private static TermRecyclerViewAdapter.MyClickListener myClickListener;
    Context context;
    Activity activity;
    final float grade_weak = 10;
    final float grade_normal = 14;
    final float grade_good = 18;
    KeySetting keySetting;

    public static class LessonReportCardItemHolder extends RecyclerView.ViewHolder {
        TextView tv_countunit;
        TextView tv_namelesson;
        TextView tv_grade;
        CardView cardView;


        public void helpLessonReportCardRecyclerView(Activity activity) {


            ViewTarget viewTarget = new ViewTarget(itemView.findViewById(R.id.lesson_report_cardView_grade));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            lps.addRule(RelativeLayout.CENTER_IN_PARENT);


            int margin = ((Number) (activity.getResources().getDisplayMetrics().density * 80)).intValue();
            lps.setMargins(margin, margin * 2, margin, 10);

            ShowcaseView sv = new ShowcaseView.Builder(activity)
                    //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                    .setTarget(viewTarget)
                    .setContentTitle(activity.getResources().getString(R.string.FragLessonReportCard_ShowCase_title))
                    .setContentText(activity.getResources().getString(R.string.FragLessonReportCard_ShowCase_desc))
                    .setStyle(R.style.BlueShowcaseTheme)
                    .build();
            sv.setButtonText(activity.getResources().getString(R.string.FragLessonReportCard_ShowCase_btn));
            sv.setButtonPosition(lps);
            sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                }
            });
        }


        public LessonReportCardItemHolder(final View itemView, final Context context) {
            super(itemView);

            tv_countunit = (TextView) itemView.findViewById(R.id.lesson_report_cardView_countunit);
            tv_namelesson = (TextView) itemView.findViewById(R.id.lesson_report_cardView_namelesson);
            tv_grade = (TextView) itemView.findViewById(R.id.lesson_report_cardView_grade);
            cardView = (CardView) itemView.findViewById(R.id.lesson_report_card_view);
        }

    }


    public void setOnItemClickListener(TermRecyclerViewAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;


    }

    public LessonReportCardRecyclerViewAdapter(ArrayList<LessonReportCardItem> myDataset, Context c, Activity activity) {
        Log.i("size", String.valueOf(myDataset.size()));
        context = c;
        mDataset = myDataset;
        this.activity = activity;


    }


    @Override
    public LessonReportCardItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_report_card_view, parent, false);
        //TermItemHolder dataObjectHolder = new TermItemHolder(view);
        LessonReportCardRecyclerViewAdapter.LessonReportCardItemHolder lessonReportCardItemHolder = new LessonReportCardRecyclerViewAdapter.LessonReportCardItemHolder(view, context);

        return lessonReportCardItemHolder;

    }

    @Override
    public void onBindViewHolder(final LessonReportCardRecyclerViewAdapter.LessonReportCardItemHolder holder, int position) {

        if (position == 0) {
            keySetting = new KeySetting(activity);

            if (keySetting.getActiveSCVLessonReportCardRecyclerView() == 0) {
                holder.helpLessonReportCardRecyclerView(activity);
                keySetting.setActiveSCVLessonReportCardRecyclerView(1);
            }
        }

        holder.tv_namelesson.setText(String.valueOf(mDataset.get(position).getNAME_LESSON()));

        holder.tv_grade.setText(String.valueOf(mDataset.get(position).getGRADE()));


        if (mDataset.get(position).getGRADE() >= grade_good) {
            holder.tv_grade.setBackgroundColor(activity.getResources().getColor(R.color.msi_EMERALD));
        } else if (mDataset.get(position).getGRADE() >= grade_normal) {
            holder.tv_grade.setBackgroundColor(activity.getResources().getColor(R.color.msi_BELIZE_HOLE));
        } else if (mDataset.get(position).getGRADE() >= grade_weak) {
            holder.tv_grade.setBackgroundColor(activity.getResources().getColor(R.color.msi_ORANGE));
        } else {
            holder.tv_grade.setBackgroundColor(activity.getResources().getColor(R.color.msi_ALIZARIN));
        }


        holder.tv_grade.setTag(String.valueOf(mDataset.get(position).getID_LESSON_REPORT_CARD()));
        holder.tv_countunit.setText(String.valueOf(mDataset.get(position).getCOUNT_UNIT()));

        holder.tv_grade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utility.setIdLessonReport(Integer.valueOf(holder.tv_grade.getTag().toString()));
                Log.i("id_LESSON REPORT CARD", String.valueOf(utility.getIdTerm()));
                LessonReportCardDialog lessonReportCardDialog = new LessonReportCardDialog((Activity) context);
                lessonReportCardDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lessonReportCardDialog.show();
            }
        });


    }

    public void addItem(LessonReportCardItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


}
