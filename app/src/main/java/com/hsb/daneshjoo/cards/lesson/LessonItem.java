package com.hsb.daneshjoo.cards.lesson;

/**
 * Created by hsb-pc on 1/30/2017.
 */
public class LessonItem {

    private String NAME_LESSON;
    private Integer ID_LESSON;
    private String COUNT_UNIT;
    private String NAME_TEACHER;
    private String DAY_CLASS;
    private String DATE_EXAM;
    private int ID;

    public  LessonItem(){}

    public LessonItem(int ID,String NAME_LESSON,Integer ID_LESSON, String COUNT_UNIT, String NAME_TEACHER, String DAY_CLASS, String DATE_EXAM) {
        this.ID=ID;
        this.NAME_LESSON = NAME_LESSON;
        this.ID_LESSON = ID_LESSON;
        this.COUNT_UNIT = COUNT_UNIT;
        this.NAME_TEACHER = NAME_TEACHER;
        this.DAY_CLASS = DAY_CLASS;
        this.DATE_EXAM = DATE_EXAM;
    }

    public Integer getID_LESSON() {
        return ID_LESSON;
    }

    public void setID_LESSON(Integer ID_LESSON) {
        this.ID_LESSON = ID_LESSON;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNAME_LESSON() {
        return NAME_LESSON;
    }

    public void setNAME_LESSON(String NAME_LESSON) {
        this.NAME_LESSON = NAME_LESSON;
    }

    public String getCOUNT_UNIT() {
        return COUNT_UNIT;
    }

    public void setCOUNT_UNIT(String COUNT_UNIT) {
        this.COUNT_UNIT = COUNT_UNIT;
    }

    public String getNAME_TEACHER() {
        return NAME_TEACHER;
    }

    public void setNAME_TEACHER(String NAME_TEACHER) {
        this.NAME_TEACHER = NAME_TEACHER;
    }

    public String getDAY_CLASS() {
        return DAY_CLASS;
    }

    public void setDAY_CLASS(String DAY_CLASS) {
        this.DAY_CLASS = DAY_CLASS;
    }

    public String getDATE_EXAM() {
        return DATE_EXAM;
    }

    public void setDATE_EXAM(String DATE_EXAM) {
        this.DATE_EXAM = DATE_EXAM;
    }
}
