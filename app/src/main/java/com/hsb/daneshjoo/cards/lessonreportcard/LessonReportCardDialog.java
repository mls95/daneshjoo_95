package com.hsb.daneshjoo.cards.lessonreportcard;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;

import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.db.SqliteUpdate;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.LessonReportCardJson;

import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 8/18/2017.
 */

public class LessonReportCardDialog extends Dialog {

    Activity a;
    Button btn_save,btn_cancel;
    TextInputEditText et_input_grade;
    LoadingProgress loadingProgress;
    final float topGrade = 20;
    final float downGrade = 0;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public LessonReportCardDialog(Activity activity){
        super(activity);
        this.a=activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_lesson_report_card_input_grade);
        setCanceledOnTouchOutside(false);
        loadingProgress=new LoadingProgress(a);

        Initilize_view();
        Initilize_EditText();

    }



    private void Initilize_view(){
        et_input_grade=(TextInputEditText) findViewById(R.id.et_dialog_lesson_report_card_input_grade);
        btn_cancel=(Button) findViewById(R.id.btn_dialog_lesson_report_card_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btn_save=(Button) findViewById(R.id.btn_dialog_lesson_report_card_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runApi_editGrade();
            }
        });
    }


    private void Initilize_EditText(){
        SqliteRepo repo=new SqliteRepo(a);
        /**
         * getIdTerm = id_lesson_report_card
          */
        et_input_grade.setText(String.valueOf( repo.getGrade(utility.getIdLessonReport())));
    }


    private void runApi_editGrade(){
        /**
         * RESTFUL API TERM
         */
        //Check exeist username
        if (String.valueOf(et_input_grade.getText()).matches("")) {
/*
            Toast.makeText(a, "مقداری وارد نشده است!", Toast.LENGTH_SHORT).show();
*/
            et_input_grade.setError(getContext().getResources().getString(R.string.LessonReportCardDialog6));
        }
        else if (validationGrade(String.valueOf(et_input_grade.getText()))) {

            SqliteRepo repo=new SqliteRepo(a);
        if (repo.Repo_account_Iduser() > 0){

            NetStatus netStatus=new NetStatus(a);
            if (netStatus.checkInternetConnection()){
                // internet connection

                try {
                    // build web service
                    loadingProgress.Show();
                    // generate json
                    LessonReportCardJson lessonReportCardJson=new LessonReportCardJson(String.valueOf(repo.getIdUser()),"idterm",String.valueOf(utility.getIdLessonReport()),"idlesson",String.valueOf(et_input_grade.getText()));
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();


                    String json=gson.toJson(lessonReportCardJson);
                    Log.i("",lessonReportCardJson.toString());
                    Log.i("json",json);
                    StoreClient client= RestClient.getClient().create(StoreClient.class);
                    Call<Messageing> call = client.posteditlessonReportCard(lessonReportCardJson);
                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {
                            Log.i("response",response.body().toString());
                            loadingProgress.Close();
                            if (response.code()==200){
                                if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                    utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.LessonReportCardDialog9),a);
                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){

                                    SqliteUpdate update = new SqliteUpdate(a);

                                    update.Update_lessonreportcard(utility.getIdLessonReport(),Float.valueOf(String.valueOf(et_input_grade.getText())));


                                    dismiss();
                                    RefreshRecyClerView();
                                    Toast.makeText(a.getApplicationContext(), getContext().getResources().getString(R.string.LessonReportCardDialog8), Toast.LENGTH_SHORT).show();
                                    //refresh_recyclerview();


                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==202){
                                    utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.LessonReportCardDialog2),a);

                                }
                            }else{
                                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_REQUEST_BAD),a);

                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();

                            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.LessonReportCardDialog4),a);
                        }
                    });

                }catch (Exception e){
                    loadingProgress.Close();
                    e.printStackTrace();
                }

            }else{
                // not internet connection
                loadingProgress.Close();
                utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.NetMessage_NOT_INTERNET_CONNECTION),a);

            }


        }else {
            loadingProgress.Close();
            utility.dialog(getContext().getResources().getString(R.string.Message),getContext().getResources().getString(R.string.LessonReportCardDialog10),a);

        }
    }}
    private Boolean validationGrade(String grade) {
        try {
            Float.parseFloat(grade);
            if (Float.parseFloat(grade) > topGrade || Float.parseFloat(grade) < downGrade) {
                et_input_grade.setError(getContext().getResources().getString(R.string.LessonReportCardDialog11));
                return false;
            }
        } catch (Exception e) {
            et_input_grade.setError(getContext().getResources().getString(R.string.LessonReportCardDialog11));
        }
        return true;
    }

    public void RefreshRecyClerView() {
        DbToArrayList dbToArrayList = new DbToArrayList(a);
        mRecyclerView = (RecyclerView) a.findViewById(R.id.lesson_report_card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(a.getApplicationContext()));


        mAdapter = new LessonReportCardRecyclerViewAdapter(dbToArrayList.getDataLessonReportCard(utility.getIdTerm()), a, a);
        mRecyclerView.setAdapter(mAdapter);
    }

}
