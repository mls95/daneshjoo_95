package com.hsb.daneshjoo.cards.term;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.fragment.fragLesson;
import com.hsb.daneshjoo.fragment.fragSignUp;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 1/29/2017.
 */
public class TermRecyclerViewAdapter extends RecyclerView.Adapter<TermRecyclerViewAdapter.TermItemHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<TermItem> mDataset;
    private static MyClickListener myClickListener;
    Context context;
    Activity activity;
    KeySetting keySetting;

    public static class TermItemHolder extends RecyclerView.ViewHolder {
        TextView tv_year;
        TextView tv_sumunit;
        TextView tv_datestart;
        TextView tv_dateend;
        TextView tv_term;

        ImageView btn_edit;
        ImageView btn_delete;
        ImageView btn_view;

        CardView cardView;

        public void helpTermRecyclerView(Activity activity) {

            ViewTarget viewTarget = new ViewTarget(itemView.findViewById(R.id.term_card_view));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            lps.addRule(RelativeLayout.CENTER_IN_PARENT);

            int margin = ((Number) (activity.getResources().getDisplayMetrics().density * 80)).intValue();
            lps.setMargins(margin, margin * 2, margin, 60);

            ShowcaseView sv = new ShowcaseView.Builder(activity)
                    //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                    .setTarget(viewTarget)
                    .setContentTitle(activity.getResources().getString(R.string.TermRecyclerViewAdapter_ShowCase_title))
                    .setContentText(activity.getResources().getString(R.string.TermRecyclerViewAdapter_ShowCase_desc))
                    .setStyle(R.style.GreenShowcaseTheme)
                    .build();
            sv.setButtonText(activity.getResources().getString(R.string.TermRecyclerViewAdapter_ShowCase_btn));
            sv.setButtonPosition(lps);
            sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                }
            });
        }




        public TermItemHolder(final View itemView, final Context context) {
            super(itemView);
            tv_year = (TextView) itemView.findViewById(R.id.tv_termcardview_year);
            tv_sumunit = (TextView) itemView.findViewById(R.id.tv_termcardview_sumUnit);
            tv_datestart = (TextView) itemView.findViewById(R.id.tv_termcardview_dateStart);
            tv_dateend = (TextView) itemView.findViewById(R.id.tv_termcardview_dateEnd);
            tv_term = (TextView) itemView.findViewById(R.id.tv_termcardview_term);

            btn_edit = (ImageView) itemView.findViewById(R.id.btn_termcardview_edit);

            btn_delete = (ImageView) itemView.findViewById(R.id.btn_termcardview_delete);
            btn_view = (ImageView) itemView.findViewById(R.id.btn_termcardview_view);
            cardView = (CardView) itemView.findViewById(R.id.term_card_view);


        }


    }


    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;


    }

    public TermRecyclerViewAdapter(ArrayList<TermItem> myDataset, Context c, Activity activity) {

        context = c;
        mDataset = myDataset;
        this.activity = activity;
    }

    @Override
    public TermItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.term_cardview, parent, false);
        //TermItemHolder dataObjectHolder = new TermItemHolder(view);
        TermRecyclerViewAdapter.TermItemHolder termItemHolder = new TermItemHolder(view, context);
        return termItemHolder;
    }

    @Override
    public void onBindViewHolder(final TermItemHolder holder, int position) {

        if(position==0){

            keySetting = new KeySetting(activity);

            if (keySetting.getActiveSCVTermRcyclerViewadapter() == 0) {
                holder.helpTermRecyclerView(activity);
                keySetting.setActiveSCVTermRcyclerViewadapter(1);
            }

        }

        holder.tv_year.setText(mDataset.get(position).getYEAR());
        holder.tv_sumunit.setText(mDataset.get(position).getSUM_UNIT());
        holder.tv_datestart.setText(mDataset.get(position).getDATE_START());
        holder.tv_dateend.setText(mDataset.get(position).getDATE_END());
        holder.tv_dateend.setTag(mDataset.get(position).getID_TERM());
        holder.tv_term.setText(mDataset.get(position).getTERM());
        holder.tv_term.setTag(mDataset.get(position).getID());

        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TermDialog termDialog = new TermDialog((Activity) context, "update-term", (Integer) holder.tv_term.getTag());
                termDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                termDialog.show();
            }
        });


        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int IdTerm = 0;
                SqliteRepo sqliteRepo = new SqliteRepo(context);
                Cursor cur = sqliteRepo.Repo_getterm((Integer) holder.tv_term.getTag());
                if (cur != null) {
                    if (cur.moveToFirst()) {
                        do {
                            IdTerm = cur.getInt(0);
                        } while (cur.moveToNext());
                    }
                    utility.setIdTerm(IdTerm);
                    utility.dialog(activity.getResources().getString(R.string.TermRecyclerView_DeleteTermTitle), activity.getResources().getString(R.string.TermRecyclerView_DeleteTermText), activity.getResources().getString(R.string.TermRecyclerView_DeleteTermYes), activity.getResources().getString(R.string.TermRecyclerView_DeleteTermNo), "delete-term", context, activity);
                }
            }
        });


        holder.btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TermDialog termDialog = new TermDialog((Activity) context, "view-term", (Integer) holder.tv_term.getTag());
                termDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                termDialog.show();
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                utility.setIdTerm(Integer.valueOf(holder.tv_term.getText().toString()));

                utility.setRow_TERM(Integer.valueOf(holder.tv_dateend.getTag().toString()));

                android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new fragLesson());
                fragmentTransaction.commit();
            }
        });
    }

    public void addItem(TermItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


}
