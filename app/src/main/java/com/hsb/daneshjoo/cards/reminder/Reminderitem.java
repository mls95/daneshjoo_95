package com.hsb.daneshjoo.cards.reminder;

/**
 * Created by hsb-pc on 2/13/2017.
 */
public class Reminderitem {
    private  int ID;
    private  int ID_LESSON;
    private  String ID_REMINDER;
    private  String NAME_LESSON;
    private  String TITLE;
    private  String DATE;
    private  String CLOCK;

    public Reminderitem(int ID, int ID_LESSON,String ID_REMINDER,String NAME_LESSON, String TITLE, String DATE, String CLOCK) {
        this.ID = ID;
        this.ID_LESSON = ID_LESSON;
        this.ID_REMINDER = ID_REMINDER;
        this.NAME_LESSON=NAME_LESSON;
        this.TITLE = TITLE;
        this.DATE = DATE;
        this.CLOCK = CLOCK;
    }

    public String getID_REMINDER() {
        return ID_REMINDER;
    }

    public void setID_REMINDER(String ID_REMINDER) {
        this.ID_REMINDER = ID_REMINDER;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID_LESSON() {
        return ID_LESSON;
    }

    public void setID_LESSON(int ID_LESSON) {
        this.ID_LESSON = ID_LESSON;
    }

    public String getNAME_LESSON() {
        return NAME_LESSON;
    }

    public void setNAME_LESSON(String NAME_LESSON) {
        this.NAME_LESSON = NAME_LESSON;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getCLOCK() {
        return CLOCK;
    }

    public void setCLOCK(String CLOCK) {
        this.CLOCK = CLOCK;
    }
}
