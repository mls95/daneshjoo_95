package com.hsb.daneshjoo.cards.note;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.EditApi;
import com.hsb.daneshjoo.services.EditApiWebService;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.NoteJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 2/12/2017.
 */
public class NoteDialog extends Dialog {


    Activity a;
    Button btn_cancel, btn_save;
    EditText et_title, et_note;
    TextView tv_nameLesson, tv_note, tv_title;
    Spinner sp_namelesson, sp_term;
    int idlesson = 0;
    int _IDNOTE = 0;
    int idterm = 0;
    List<Integer> listidlesson;
    List<String> listterm;
    ArrayAdapter adapter;
    String token;
    int idNote;

    SqliteRepo repo;
    LoadingProgress loadingProgress;
    KeySetting keySetting;

    public NoteDialog(Activity a) {
        super(a);
        this.a = a;
    }

    public NoteDialog(Activity a, int idNote, String token) {
        super(a);
        this.a = a;
        this.token = token;
        this.idNote = idNote;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(true);

        loadingProgress = new LoadingProgress(a);
        repo = new SqliteRepo(a.getApplicationContext());

        keySetting = new KeySetting(a);
        if (token == "view-note") {
            setContentView(R.layout.dialog_view_note);
            Initilize_view();
            ValueShow();
        } else if (token == "update-note") {
            setContentView(R.layout.dialog_note);
            Initialize();
            setValueSpinnerTerm();
            setSpinnerValue(Integer.valueOf(keySetting.getTerm()));
            ValueShow();
            setListnamelesson();

        } else {
            setContentView(R.layout.dialog_note);
            Initialize();
            setValueSpinnerTerm();
            setSpinnerValue(Integer.valueOf(keySetting.getTerm()));
            setListnamelesson();
        }

    }

    private void Initilize_view() {

        tv_nameLesson = (TextView) findViewById(R.id.tv_noteview_valuenameLesson);

        tv_title = (TextView) findViewById(R.id.tv_noteview_valuetitle);
        tv_note = (TextView) findViewById(R.id.tv_noteview_valuenote);


        btn_cancel = (Button) findViewById(R.id.btn_noteview_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    private void Initialize() {
        et_title = (EditText) findViewById(R.id.et_dialog_note_titer);
        et_note = (EditText) findViewById(R.id.et_dialog_note_note);
        sp_namelesson = (Spinner) findViewById(R.id.sp_dialog_note_nameLesson);
        sp_namelesson.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idlesson = sp_namelesson.getSelectedItemPosition();
                idlesson = listidlesson.get(idlesson);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        sp_term = (Spinner) findViewById(R.id.sp_dialog_note_term);
        sp_term.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                idterm = sp_term.getSelectedItemPosition();
                idterm = Integer.valueOf(listterm.get(idterm));
                setSpinnerValue(idterm);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_cancel = (Button) findViewById(R.id.btn_dialog_notereminder_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btn_save = (Button) findViewById(R.id.btn_dialog_notereminder_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidation()) {

                    if (token == "update-note") {
                        NoteJson noteJson = new NoteJson(String.valueOf(repo.getIdUser()), String.valueOf(idlesson), String.valueOf(idterm), String.valueOf(_IDNOTE), et_title.getText().toString(), et_note.getText().toString(), null);
                        EditApiWebService webService = new EditApiWebService(a);
                        webService.RunApi(EditApi.API_EDIT_NOTE, noteJson, NoteDialog.this);
                    } else {

                        RunApiNote();
                    }


                }
            }
        });

    }

    private boolean isValidation() {
        if (et_title.getText().toString().isEmpty() || et_title.getText().toString().length() <= 0) {
            // utility.dialog("پیام","عنوان را وارد کنید",a);
            et_title.setError(getContext().getResources().getString(R.string.NoteDialog7));
        } else if (et_note.getText().toString().equals(" ") || et_note.getText().toString().isEmpty() || et_note.getText().toString().length() <= 0) {
            //utility.dialog("پیام","متن را وارد کنید",a);
            et_note.setError(getContext().getResources().getString(R.string.NoteDialog8));
        } else {
            return true;
        }
        return false;
    }

    private void ValueShow() {

        SqliteRepo repo = new SqliteRepo(a.getApplicationContext());
        Cursor cur = repo.Repo_note(idNote);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    if (this.token == "update-note") {
                        _IDNOTE = cur.getInt(3);
                        sp_namelesson.setSelection(listidlesson.indexOf(cur.getInt(2)));
                        Log.i("list id lesson ", String.valueOf(listidlesson.indexOf(cur.getInt(2))));
                        et_title.setText(cur.getString(4));
                        et_note.setText(cur.getString(5));


                        btn_save.setText(getContext().getResources().getString(R.string.LessonDialog10));
                    } else if (this.token == "view-note") {
                        _IDNOTE = cur.getInt(3);
                        tv_nameLesson.setText(repo.getNameLesson(cur.getInt(2)));
                        tv_title.setText(cur.getString(4));
                        tv_note.setText(cur.getString(5));


                    }


                } while (cur.moveToNext());

            }

        }

    }

    private void setSpinnerValue(int idterm) {
        List<String> listnamelesson = new ArrayList<>();
        listidlesson = new ArrayList<>();

        SqliteRepo sqliteRq = new SqliteRepo(getContext());

        Cursor cur = sqliteRq.Repo_ListNameLesson(idterm);
        int i = 0;
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {


                    listnamelesson.add(cur.getString(1));
                    listidlesson.add(cur.getInt(2));

                    i++;
                } while (cur.moveToNext());
            }
        }


        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listnamelesson);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_namelesson.setAdapter(adapter);

        sp_namelesson.setSelection(adapter.getPosition(utility.getNameLesson()));


    }


    private void setListnamelesson() {
        sp_namelesson.setSelection(adapter.getPosition(utility.getNameLesson()));
        Log.i("listterm", listterm.indexOf(String.valueOf(keySetting.getTerm())) + "");
        sp_term.setSelection(listterm.indexOf(String.valueOf(keySetting.getTerm())));
    }

    private void setValueSpinnerTerm() {

        listterm = new ArrayList<>();


        SqliteRepo sqliteRq = new SqliteRepo(getContext());
        Cursor cur = sqliteRq.Repo_getterm();
        int i = 0;
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {


                    listterm.add(String.valueOf(cur.getInt(0)));


                    i++;
                } while (cur.moveToNext());
            }
        }


        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listterm);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_term.setAdapter(adapter);
    }

    private void RefreshRecyclerView() {
        DbToArrayList dbToArrayList = new DbToArrayList(a);
        RecyclerView mRecyclerView = (RecyclerView) a.findViewById(R.id.note_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(a));
        NoteRecyclerViewAdpter mAdapter = new NoteRecyclerViewAdpter(dbToArrayList.getDataNote(), a, a);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void RunApiNote() {

        /**
         * RESTFUL API NOTE
         */
        //Check exeist username

        if (repo.Repo_account_Iduser() > 0) {

            NetStatus netStatus = new NetStatus(a.getApplicationContext());
            if (netStatus.checkInternetConnection()) {
                // internet connection

                try {

                    // build web service

                    loadingProgress.Show();
                    // generate json
                    UUID uuid = UUID.randomUUID();
                    final Integer id_note = Math.abs(uuid.hashCode());
                    NoteJson noteJson = new NoteJson(String.valueOf(repo.getIdUser()), String.valueOf(idlesson), String.valueOf(idterm), String.valueOf(id_note), et_title.getText().toString(), null, et_note.getText().toString());
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();
                    String json = gson.toJson(noteJson);
                    Log.i("", noteJson.toString());
                    StoreClient client = RestClient.getClient().create(StoreClient.class);
                    Call<Messageing> call = client.postinsertnote(noteJson);
                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {
                            loadingProgress.Close();

                            if (response.code() == 200) {
                                if (Integer.valueOf(response.body().getSuccessCode().toString()) == 0) {
                                    // utility.dialog(getContext().getResources().getString(R.string.NoteDialog1), getContext().getResources().getString(R.string.NoteDialog5), getContext());
                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 1) {
                                    SqliteInsert sqliteInsert = new SqliteInsert(a);
                                    SqliteRepo sqliteRepo = new SqliteRepo(a);
                                    sqliteInsert.ADD_note(response.body().getToken(), idlesson, id_note, et_title.getText().toString(), et_note.getText().toString(), idterm);

                                    RefreshRecyclerView();

                                    dismiss();

                                    Toast.makeText(a.getApplicationContext(), getContext().getResources().getString(R.string.NoteDialog3), Toast.LENGTH_SHORT).show();

                                } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 202) {
                                    //  utility.dialog(getContext().getResources().getString(R.string.NoteDialog1), getContext().getResources().getString(R.string.NoteDialog2), a.getApplicationContext());

                                }
                            } else {
                                //   utility.dialog(getContext().getResources().getString(R.string.NoteDialog1), NetMessage.REQUEST_BAD, a.getApplicationContext());


                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();
                            // utility.dialog(getContext().getResources().getString(R.string.NoteDialog1), getContext().getResources().getString(R.string.NoteDialog4), a);
                        }
                    });


                } catch (Exception e) {
                    loadingProgress.Close();
                    e.printStackTrace();
                }

            } else {
                // not internet connection
                loadingProgress.Close();
                utility.dialog(getContext().getResources().getString(R.string.NoteDialog1), getContext().getResources().getString(R.string.NetMessage_NOT_INTERNET_CONNECTION), a);

            }


        } else {
            loadingProgress.Close();
            utility.dialog(getContext().getResources().getString(R.string.NoteDialog1), getContext().getResources().getString(R.string.NoteDialog2), a);

        }


    }
}
