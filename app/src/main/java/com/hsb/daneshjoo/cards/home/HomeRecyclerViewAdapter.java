package com.hsb.daneshjoo.cards.home;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.lesson.LessonItem;
import com.hsb.daneshjoo.cards.lessonreportcard.LessonReportCardDialog;
import com.hsb.daneshjoo.cards.note.NoteDialog;
import com.hsb.daneshjoo.cards.reminder.ReminderDialog;
import com.hsb.daneshjoo.cards.setterm.SetTermDialog;
import com.hsb.daneshjoo.cards.term.TermDialog;
import com.hsb.daneshjoo.fragment.fragReportCard;
import com.hsb.daneshjoo.utilities.utility;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb-pc on 2/10/2017.
 */
public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.HomeItemHolder>{

    private static ArrayList<HomeItem> mDataset;
    Context context;
    int i_saturday =0;
    int i_sunday =0;
    int i_monday =0;
    int i_tuesday =0;

    int i_wednesday =0;
    int i_thursday =0;
    int i_friday =0;

    public static class HomeItemHolder extends RecyclerView.ViewHolder {

        TextView tv_namelesson,tv_nameteacher,tv_dayclass,tv_clockclass,tv_examdate,tv_numberclass,tv_dayweek;
        Button btn_note,btn_noteRimender,btn_viewnote;
        public HomeItemHolder(View itemView) {
            super(itemView);

            tv_nameteacher=(TextView) itemView.findViewById(R.id.tv_homecardview_nameTeacher);
            tv_namelesson=(TextView) itemView.findViewById(R.id.tv_homecardview_namelesson);
            tv_dayclass=(TextView) itemView.findViewById(R.id.tv_homecardview_dayclass);
            tv_clockclass=(TextView) itemView.findViewById(R.id.tv_homecardview_clockclass);
            tv_examdate=(TextView) itemView.findViewById(R.id.tv_homecardview_examdate);
            tv_numberclass=(TextView) itemView.findViewById(R.id.tv_homecardview_numberclass);
            tv_dayweek=(TextView) itemView.findViewById(R.id.tv_home_cardview_dayWeek);

            btn_note=(Button) itemView.findViewById(R.id.btn_homecardview_addnote);
            btn_noteRimender=(Button) itemView.findViewById(R.id.btn_homecardview_noteNotification);
            btn_viewnote=(Button) itemView.findViewById(R.id.btn_homecardview_noteView);
        }
    }

    public HomeRecyclerViewAdapter(ArrayList<HomeItem> data,Context context) {
        mDataset=data;
        this.context = context;
    }

    @Override
    public HomeItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.home_cardview,parent,false);
        HomeRecyclerViewAdapter.HomeItemHolder homeItemHolder=new HomeItemHolder(view);
        return homeItemHolder;
    }

    @Override
    public void onBindViewHolder(final HomeItemHolder holder, int position) {
        int dayweek=Integer.valueOf(mDataset.get(position).getDAY_WEEK()) ;
        if(dayweek==0) {

            Log.i("dayweek","0");
            Log.i("dayweek i ",String.valueOf(i_saturday));
            if (i_saturday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());
            i_saturday++;
        }else if (dayweek==1){

            if (i_sunday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());

            i_sunday++;

        }else if(dayweek==2){

            if (i_monday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());


            i_monday++;

        }else if(dayweek==3){

            if (i_tuesday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());


            i_tuesday++;
        }else if(dayweek==4){

            if (i_wednesday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());


            i_wednesday++;
        }else if(dayweek==5){

            if (i_thursday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());


            i_thursday++;
        }else if(dayweek==6){

            if (i_friday ==0)
                holder.tv_dayweek.setText(mDataset.get(position).getDAY_CLASS());
            else
                holder.tv_dayweek.setVisibility(View.GONE);
            holder.tv_namelesson.setText(mDataset.get(position).getNAME_LESSON());
            holder.tv_nameteacher.setText(mDataset.get(position).getNAME_TEACHER());
            holder.tv_examdate.setText(mDataset.get(position).getEXAM_DATE());
            holder.tv_dayclass.setText(mDataset.get(position).getDAY_CLASS());
            holder.tv_clockclass.setText(mDataset.get(position).getCLOCK_CLASS());
            holder.tv_numberclass.setText(mDataset.get(position).getNUMBER_CLASS());


            i_friday++;
        }


        holder.btn_noteRimender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                utility.setNameLesson(holder.tv_namelesson.getText().toString());

                ReminderDialog reminderDialog=new ReminderDialog((AppCompatActivity) context,"home");
                reminderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                reminderDialog.show();
            }
        });

        holder.btn_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utility.setNameLesson(holder.tv_namelesson.getText().toString());

                NoteDialog noteDialog=new NoteDialog((Activity) context);
                noteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                noteDialog.show();
            }
        });


        holder.btn_viewnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                android.support.v4.app.FragmentManager fragmentManager =((AppCompatActivity) context).getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body,new fragReportCard());
                fragmentTransaction.commit();



            }
        });


    }

    public void addItem(HomeItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);

    }




    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
