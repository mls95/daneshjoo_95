package com.hsb.daneshjoo.cards.termreportcard;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.term.TermDialog;
import com.hsb.daneshjoo.cards.term.TermItem;
import com.hsb.daneshjoo.cards.term.TermRecyclerViewAdapter;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.fragment.fragLesson;

import com.hsb.daneshjoo.fragment.fragLessonReportCard;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb on 8/14/2017.
 */

public class TermReportCardRecyclerViewAdapter extends RecyclerView.Adapter<TermReportCardRecyclerViewAdapter.TermReportCardItemHolder> {

    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<TermReportCard> mDataset;
    private static TermRecyclerViewAdapter.MyClickListener myClickListener;
    Context context;
    Activity activity;
    final float avg_weak = 10;
    final float avg_normal = 14;
    final float avg_good = 18;
    KeySetting keySetting;


    public static class TermReportCardItemHolder extends RecyclerView.ViewHolder {
        TextView tv_term;
        TextView tv_sumunit;
        TextView tv_avggrade;
        CardView cardView;

        public void helpTermReportCardRecyclerView(Activity activity) {

            ViewTarget viewTarget = new ViewTarget(itemView.findViewById(R.id.term_report_cardView_term));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            lps.addRule(RelativeLayout.CENTER_IN_PARENT);

            int margin = ((Number) (activity.getResources().getDisplayMetrics().density * 80)).intValue();
            lps.setMargins(margin, margin * 2, margin, 60);

            ShowcaseView sv = new ShowcaseView.Builder(activity)
                    //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                    .setTarget(viewTarget)
                    .setContentTitle(activity.getResources().getString(R.string.LessonReportCardView_ShowCase_title))
                    .setContentText(activity.getResources().getString(R.string.LessonReportCardView_ShowCase_desc))
                    .setStyle(R.style.PurpleShowcaseTheme)
                    .build();
            sv.setButtonText(activity.getResources().getString(R.string.LessonReportCardView_ShowCase_btn));
            sv.setButtonPosition(lps);
            sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                }
            });
        }


        public TermReportCardItemHolder(final View itemView, final Context context) {
            super(itemView);

            tv_sumunit = (TextView) itemView.findViewById(R.id.term_report_cardView_sumunit);

            tv_term = (TextView) itemView.findViewById(R.id.term_report_cardView_term);
            tv_avggrade = (TextView) itemView.findViewById(R.id.term_report_cardView_avegrade);

            cardView = (CardView) itemView.findViewById(R.id.term_report_card_view);

        }
    }

    public void setOnItemClickListener(TermRecyclerViewAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;

    }

    public TermReportCardRecyclerViewAdapter(ArrayList<TermReportCard> myDataset, Context c, Activity activity) {
        context = c;
        mDataset = myDataset;
        this.activity = activity;
    }

    @Override
    public TermReportCardItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.term_report_card_view, parent, false);
        //TermItemHolder dataObjectHolder = new TermItemHolder(view);
        TermReportCardRecyclerViewAdapter.TermReportCardItemHolder termItemHolder = new TermReportCardRecyclerViewAdapter.TermReportCardItemHolder(view, context);
        return termItemHolder;
    }

    @Override
    public void onBindViewHolder(final TermReportCardRecyclerViewAdapter.TermReportCardItemHolder holder, int position) {

        if (position == 0) {

            keySetting = new KeySetting(activity);

            if (keySetting.getActiveSCVTermReportCardRecyclerView() == 0) {
                holder.helpTermReportCardRecyclerView(activity);
                keySetting.setActiveSCVTermReportCardRecyclerView(1);
            }
        }
        float avarage, sumGrade, sumUnit;
        holder.tv_term.setText(String.valueOf(mDataset.get(position).getTERM()));

        holder.tv_term.setTag(String.valueOf(mDataset.get(position).getID_TERM()));
        holder.tv_sumunit.setText(String.valueOf(mDataset.get(position).getSUM_UNIT()));

        sumGrade = Float.parseFloat(String.valueOf(mDataset.get(position).getAVG_GRADE()));
        sumUnit = Float.parseFloat(String.valueOf(mDataset.get(position).getSUM_UNIT()));
        avarage = sumGrade / sumUnit;

        avarage = (float) (Math.round(avarage * 100) / 100.0d);

        holder.tv_avggrade.setText(String.valueOf(avarage));


        if (avarage >= avg_good) {
            holder.tv_avggrade.setBackgroundColor(activity.getResources().getColor(R.color.msi_EMERALD));
        } else if (avarage >= avg_normal) {
            holder.tv_avggrade.setBackgroundColor(activity.getResources().getColor(R.color.msi_BELIZE_HOLE));
        } else if (avarage >= avg_weak) {
            holder.tv_avggrade.setBackgroundColor(activity.getResources().getColor(R.color.msi_ORANGE));
        } else {
            holder.tv_avggrade.setBackgroundColor(activity.getResources().getColor(R.color.msi_ALIZARIN));
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("size", "set on click listener");
                utility.setIdTerm(Integer.valueOf(holder.tv_term.getText().toString()));
                android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new fragLessonReportCard());
                fragmentTransaction.commit();
            }
        });
    }

    public void addItem(TermReportCard dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

}
