package com.hsb.daneshjoo.cards.term;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.hsb.daneshjoo.PersianDateDialog;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.db.SqliteUpdate;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.EditApi;
import com.hsb.daneshjoo.services.EditApiWebService;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.TermJson;
import com.hsb.daneshjoo.services.webservice.request.TokenJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.services.webservice.response.TermMessage;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.util.PersianCalendar;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb-pc on 1/27/2017.
 */
public class TermDialog extends Dialog implements View.OnClickListener {

    private String token="";
    public Activity c;
    EditText et_term,et_unitsum;
    TextView tv_valueterm,tv_valueyear,tv_valuesumunit,tv_valuedatestart,tv_valuedateend,tv_datestart,tv_eateend;
    PersianDateDialog persianDateDialog;
    Spinner sp_year;
    Button btn_save,btn_cancel;

    ArrayAdapter<String> adapter;
    SqliteRepo repo;
    int id_term=0;

    int _PASTTERM = 0;
    int numberYear=3;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    LoadingProgress loadingProgress;
    public TermDialog(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;

    }
    public TermDialog(Activity a,String token,int id_term) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.token=token;
        this.id_term=id_term;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        loadingProgress=new LoadingProgress(c);
         persianDateDialog=new PersianDateDialog(c);
        repo=new SqliteRepo(c.getApplicationContext());

        if (token=="update-term"){
            setContentView(R.layout.fragment_dialog_term);
            setCanceledOnTouchOutside(false);

            Initilize();
            initSpinner();
           Value_show();
        }else if(token=="view-term"){
            setContentView(R.layout.dialog_view_term);
            setCanceledOnTouchOutside(false);
            Initilize_view();
            Value_show();
        }else{
            setContentView(R.layout.fragment_dialog_term);
            setCanceledOnTouchOutside(false);
            Initilize();
            initSpinner();
        }



    }


    private void Initilize_view(){
        tv_valueterm=(TextView) findViewById(R.id.tv_dialog_term_valueterm);
        tv_valueyear=(TextView) findViewById(R.id.tv_dialog_term_valueyear);
        tv_valuesumunit=(TextView) findViewById(R.id.tv_dialog_term_valuesumunit);
        tv_valuedatestart=(TextView) findViewById(R.id.tv_dialog_term_valuedatestart);
        tv_valuedateend=(TextView) findViewById(R.id.tv_dialog_term_valuedateend);

        btn_cancel=(Button) findViewById(R.id.btn_dialog_term_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
    private void Initilize(){

        tv_datestart=(TextView) findViewById(R.id.et_fragment_dialog_term_dateStart);
        tv_datestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersianDateDialog persianDateDialog=new PersianDateDialog(c);
                persianDateDialog.getStringDate(tv_datestart);
            }
        });


        tv_eateend=(TextView) findViewById(R.id.et_fragment_dialog_term_dateEnd);
        tv_eateend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                persianDateDialog.getStringDate(tv_eateend);
            }
        });


        et_term=(EditText) findViewById(R.id.et_fragment_dialog_term_term);
        et_unitsum=(EditText) findViewById(R.id.et_fragment_dialog_term_unitSum);

        sp_year=(Spinner) findViewById(R.id.sp_fragment_dialog_term_year);
        sp_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                numberYear=sp_year.getSelectedItemPosition();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_cancel=(Button) findViewById(R.id.btn_fragment_dialog_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btn_save=(Button) findViewById(R.id.btn_fragment_dialog_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Log.i("-------------tv_eateend", tv_eateend.getTag()+"");
                Log.i("-----------tv_datestart", tv_datestart.getTag()+"");

                if (isValidation()) {
                    if (token=="update-term") {
                        Value_Update();
                        refresh_recyclerview();
                    }else{
                        RunApiTerm();
                    }
                }
            }
        });
    }


    private boolean isValidation(){
        if(et_term.getText().toString().isEmpty() || et_term.getText().toString().length()<=0){
            et_term.setError(c.getResources().getString(R.string.TermDialog_Error_TermNotCorrect));
        }else if(Integer.valueOf(et_term.getText().toString())<=0) {
            et_term.setError(c.getResources().getString(R.string.TermDialog_Error_TermCanNotZero));
        }else if(Integer.valueOf(et_term.getText().toString())>=15) {
            et_term.setError(c.getResources().getString(R.string.TermDialog_Error_TermHigher15));
        }else if(et_unitsum.getText().toString().equals("") || et_unitsum.getText().toString().isEmpty() || et_unitsum.getText().toString().length()<=0){
            et_unitsum.setError(c.getResources().getString(R.string.TermDialog_Error_TermTotalNotCorrect));
        }else if(Integer.valueOf(et_unitsum.getText().toString())<=0 ){
            et_unitsum.setError(c.getResources().getString(R.string.TermDialog_Error_TermTotalNotZero));
        }else if(Integer.valueOf(et_unitsum.getText().toString())>=25 ){
            et_unitsum.setError(c.getResources().getString(R.string.TermDialog_Error_TermTotalMore24));
        }

        else if(tv_datestart.getText().toString().equals(getContext().getResources().getString(R.string.et_fragment_dialog_term_dateStart))
                || tv_datestart.getText().toString().isEmpty() || tv_datestart.getText().length()<=0){
            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.TermDialog_Error_StartDateNotCorrect),getContext());
        }

        else if(tv_eateend.getText().toString().equals(getContext().getResources().getString(R.string.et_fragment_dialog_term_dateEnd)) ||
                tv_eateend.getText().toString().isEmpty() || tv_eateend.getText().length()<=0){
            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.TermDialog_Error_EndDateNotCorrect),getContext());
        }
        else if (Integer.valueOf(String.valueOf(tv_datestart.getTag())) >= Integer.valueOf(String.valueOf(tv_eateend.getTag())))
            utility.dialog(getContext().getResources().getString(R.string.Message), getContext().getResources().getString(R.string.TermDialog_Error_StartEndDateNotCorrect),getContext());
        else{
            return true;
        }
        return false;
    }
    private void initSpinner(){

                adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,getContext().getResources().getStringArray(R.array.Term_Spinner));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_year.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {

    }


    private void Value_show(){

        Cursor cur=repo.Repo_term(this.id_term);

        if (cur !=null){
            if (cur.moveToFirst()){
                do{

                    if (this.token=="update-term") {
                        _PASTTERM =cur.getInt(2);
                        et_term.setText(String.valueOf(cur.getInt(3)));
                        sp_year.setSelection(cur.getInt(4));
                        et_unitsum.setText(cur.getString(5));
                        tv_datestart.setText(persianDateDialog.setDate(cur.getString(6)));
                        tv_datestart.setTag(cur.getString(6));
                        tv_eateend.setText(persianDateDialog.setDate(cur.getString(7)));
                        tv_eateend.setTag(cur.getString(7));
                        btn_save.setText(getContext().getResources().getString(R.string.TermDialog_Error_TermEdit));
                    }else if(this.token=="view-term"){
                        _PASTTERM =cur.getInt(2);
                        tv_valueterm.setText(String.valueOf(cur.getInt(3)));
                        if (cur.getInt(4)==0){
                            tv_valueyear.setText(getContext().getResources().getStringArray(R.array.Term_Spinner)[0]);
                        }else if(cur.getInt(4)==1){
                            tv_valueyear.setText(getContext().getResources().getStringArray(R.array.Term_Spinner)[1]);
                        }else{
                            tv_valueyear.setText(getContext().getResources().getStringArray(R.array.Term_Spinner)[2]);
                        }

                        tv_valuesumunit.setText(cur.getString(5));
                        tv_valuedatestart.setText(persianDateDialog.setDate(cur.getString(6)));
                        tv_valuedateend.setText(persianDateDialog.setDate(cur.getString(7)));
                    }


                }while (cur.moveToNext());

            }
        }
    }


    private void Value_Update(){

        if (isValidation()) {
            TermJson termJson=new TermJson(String.valueOf(repo.getIdUser()),et_term.getText().toString(),"id_term",String.valueOf(_PASTTERM),String.valueOf(numberYear), et_unitsum.getText().toString(), tv_datestart.getTag().toString(),tv_eateend.getTag().toString(),"id_term_report_card");
            EditApiWebService webService=new EditApiWebService(c);
            webService.RunApi(EditApi.API_EDIT_TERM,termJson,TermDialog.this);
        }
    }

    public void refresh_recyclerview(){
        DbToArrayList dbToArrayList=new DbToArrayList(c);
        mRecyclerView = (RecyclerView) c.findViewById(R.id.term_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(c));
        mAdapter = new TermRecyclerViewAdapter(dbToArrayList.getDataTerm(),c,c);
        mRecyclerView.setAdapter(mAdapter);
    }



    private void   RunApiTerm(){

        /**
         * RESTFUL API TERM
         */
        //Check exeist username
        final SqliteRepo repo=new SqliteRepo(c);
        if (repo.Repo_account_Iduser() > 0){

            NetStatus netStatus=new NetStatus(c);
            if (netStatus.checkInternetConnection()){
                // internet connection

                try {


                    UUID single=UUID.randomUUID();
                    final Integer id_term=Math.abs(single.hashCode());

                    UUID single1=UUID.randomUUID();
                    final Integer id_term_report_card=Math.abs(single.hashCode())+1024;

                    // build web service
                    loadingProgress.Show();
                    // generate json
                    TermJson termJson=new TermJson(String.valueOf(repo.getIdUser()),et_term.getText().toString(),String.valueOf(id_term),"",String.valueOf(numberYear), et_unitsum.getText().toString(), tv_datestart.getTag().toString(),tv_eateend.getTag().toString(),String.valueOf(id_term_report_card));
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();


                    Log.i("",termJson.toString());

                    StoreClient client= RestClient.getClient().create(StoreClient.class);
                    Call<Messageing> call = client.postinsertterm(termJson);
                    call.enqueue(new Callback<Messageing>() {
                        @Override
                        public void onResponse(Call<Messageing> call, Response<Messageing> response) {

                            loadingProgress.Close();
                            Log.i("response",response.body().toString());
                            if (response.code()==200){
                                if(Integer.valueOf(response.body().getSuccessCode().toString())==0){
                                    utility.dialog(c.getResources().getString(R.string.Message),c.getResources().getString(R.string.FragSignin_Message_TrayAgin),c);
                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==1){

                                    SqliteInsert insert = new SqliteInsert(c);
                                    SqliteRepo sqliteRepo=new SqliteRepo(c);

                                    // insert record to table 'term'
                                    insert.ADD_term(response.body().getToken(),id_term,Integer.valueOf(et_term.getText().toString()), numberYear, Integer.valueOf(et_unitsum.getText().toString()), Integer.valueOf(tv_datestart.getTag().toString()), Integer.valueOf(tv_eateend.getTag().toString()));
                                    // insert record to table 'term_report_card'
                                    insert.ADD_term_report_card(response.body().getToken(),id_term_report_card,id_term_report_card,Integer.valueOf(et_term.getText().toString()));

                                    dismiss();
                                    Toast.makeText(c.getApplicationContext(), c.getResources().getString(R.string.TermDialog_Message_SaveTerm), Toast.LENGTH_SHORT).show();
                                    refresh_recyclerview();


                                }else if(Integer.valueOf(response.body().getSuccessCode().toString())==202){
                                    utility.dialog(c.getResources().getString(R.string.Message),c.getResources().getString(R.string.TermDialog_Message_NotSaveTerm),c);

                                }
                            }else{
                                utility.dialog(c.getResources().getString(R.string.Message), c.getResources().getString(R.string.NetMessage_REQUEST_BAD),c);
                            }
                        }

                        @Override
                        public void onFailure(Call<Messageing> call, Throwable t) {
                            // Log error here since request failed
                            loadingProgress.Close();

                            utility.dialog(c.getResources().getString(R.string.Message),c.getResources().getString(R.string.FragSignin_Message_TrayAgin),c);
                        }
                    });




                }catch (Exception e){
                    loadingProgress.Close();
                    e.printStackTrace();
                }

            }else{
                // not internet connection
                loadingProgress.Close();
                utility.dialog(c.getResources().getString(R.string.Message),c.getResources().getString(R.string.NOT_INTERNET_CONNECTION),c);

            }


        }else {
            loadingProgress.Close();
            utility.dialog(c.getResources().getString(R.string.Message),c.getResources().getString(R.string.FragSignin_Message_TrayAgin),c);

        }



    }




}
