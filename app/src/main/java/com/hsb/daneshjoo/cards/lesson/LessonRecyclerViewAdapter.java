package com.hsb.daneshjoo.cards.lesson;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.term.TermItem;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteDelete;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;

/**
 * Created by hsb-pc on 1/30/2017.
 */
public class LessonRecyclerViewAdapter extends RecyclerView.Adapter<LessonRecyclerViewAdapter.LessonItemHoder> {

    private static ArrayList<LessonItem> mDataset;
    Context context;
    Activity activity;
    public static class LessonItemHoder extends RecyclerView.ViewHolder{
        TextView tv_nameLesson,tv_unit,tv_dayName,tv_nameTeacher;
        ImageView iv_edit,iv_view,iv_delete;
        CardView cardView;


        public LessonItemHoder(View itemView ){
            super(itemView);

            tv_nameLesson=(TextView) itemView.findViewById(R.id.tv_lesson_cardview_nameLesson);
            tv_unit=(TextView) itemView.findViewById(R.id.tv_lesson_cardview_Unit);
            tv_dayName=(TextView) itemView.findViewById(R.id.tv_lesson__cardview_dayClass);
            tv_nameTeacher=(TextView) itemView.findViewById(R.id.tv_lesson_cardveiw_name_teacher);
            iv_edit=(ImageView) itemView.findViewById(R.id.iv_lesson_cardview_edit);
            iv_view=(ImageView) itemView.findViewById(R.id.iv_lesson_cardview_view);
            iv_delete=(ImageView) itemView.findViewById(R.id.iv_lesson_cardview_delete);

        }
    }


    @Override
    public LessonItemHoder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_cardview,parent,false);
        LessonRecyclerViewAdapter.LessonItemHoder lessonItemHoder=new LessonItemHoder(view);
        return lessonItemHoder;
    }


    @Override
    public void onBindViewHolder(final LessonItemHoder holder, int position) {

        holder.tv_dayName.setText(mDataset.get(position).getDAY_CLASS());
        holder.tv_nameTeacher.setText(mDataset.get(position).getNAME_TEACHER());
        holder.tv_nameLesson.setText(mDataset.get(position).getNAME_LESSON());
        holder.tv_nameLesson.setTag(mDataset.get(position).getID_LESSON());
        holder.tv_unit.setText(mDataset.get(position).getCOUNT_UNIT());
        holder.tv_unit.setTag(mDataset.get(position).getID());

        holder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonDialog dialog=new LessonDialog((Activity) context,(Integer) holder.tv_unit.getTag(),"view-lesson");
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utility.setIdTerm((Integer) holder.tv_unit.getTag());
                utility.setId((Integer) holder.tv_nameLesson.getTag());
                utility.dialog(context.getResources().getString(R.string.LessonRecyclerViewAdapter1), context.getResources().getString(R.string.LessonRecyclerViewAdapter2), context.getResources().getString(R.string.LessonRecyclerViewAdapter3), context.getResources().getString(R.string.LessonRecyclerViewAdapter4), "delete-lesson", context,activity);

            }
        });

        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonDialog dialog=new LessonDialog((Activity) context,(Integer) holder.tv_unit.getTag(),"update-lesson");
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });


    }

    public LessonRecyclerViewAdapter(ArrayList<LessonItem> myDataset,Context context,Activity activity) {

        mDataset = myDataset;
        this.context=context;
        this.activity=activity;
    }

    public void addItem(LessonItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }



}
