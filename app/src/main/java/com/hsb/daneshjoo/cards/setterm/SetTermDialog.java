package com.hsb.daneshjoo.cards.setterm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.home.HomeRecyclerViewAdapter;
import com.hsb.daneshjoo.cards.note.NoteDialog;
import com.hsb.daneshjoo.cards.note.NoteRecyclerViewAdpter;
import com.hsb.daneshjoo.cards.reminder.ReminderRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.EditApi;
import com.hsb.daneshjoo.services.EditApiWebService;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.NoteJson;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.util.LoadingProgress;
import com.hsb.daneshjoo.utilities.utility;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 9/3/2017.
 */

public class SetTermDialog extends Dialog {

    RecyclerView mRecyclerView;

    Activity a;
    Button btn_cancel, btn_save;

    Spinner sp_setterm;
    int idlesson = 0;

    List<String> listterm;
    SqliteRepo repo;
    KeySetting keySetting;
    LoadingProgress loadingProgress;


    public SetTermDialog(Activity a) {
        super(a);
        this.a = a;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);

        loadingProgress = new LoadingProgress(a);
        repo = new SqliteRepo(a.getApplicationContext());
        keySetting=new KeySetting(a.getApplicationContext());


        setContentView(R.layout.dialog_set_term);
        Initilize_view();
        setSpinnerValue();



    }

    private void Initilize_view() {

        sp_setterm=(Spinner) findViewById(R.id.sp_dialog_setterm_term) ;

        sp_setterm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idlesson = sp_setterm.getSelectedItemPosition();

                if(listterm.get(idlesson).equals(a.getResources().getString(R.string.SetTerm_All)))
                    idlesson=0;
                else
                    idlesson = Integer.valueOf(listterm.get(idlesson));

                keySetting.setTerm(idlesson);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_cancel = (Button) findViewById(R.id.btn_dialog_setterm_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btn_save=(Button) findViewById(R.id.btn_dialog_setterm_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefreshRecyclerView();
                dismiss();
            }
        });

    }






    private void setSpinnerValue() {
        listterm = new ArrayList<>();


        SqliteRepo sqliteRq = new SqliteRepo(getContext());
        Cursor cur = sqliteRq.Repo_getterm();
        int i = 0;
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {


                    listterm.add(String.valueOf(cur.getInt(0)));


                    i++;
                } while (cur.moveToNext());
            }
        }

        listterm.add(a.getResources().getString(R.string.SetTerm_All));

        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listterm);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_setterm.setAdapter(adapter);
        sp_setterm.setSelection(listterm.indexOf(String.valueOf(keySetting.getTerm())));

    }

    private void RefreshRecyclerView(){
        if (keySetting.getActiveFragment()==0) {
            HOME_RecyclerView();
        }else if(keySetting.getActiveFragment()==3){
            REMINDER_RecyclerView();

        }else if(keySetting.getActiveFragment()==4){
            NOTE_RecyclerView();
        }
    }


    private void HOME_RecyclerView(){
        DbToArrayList dbToArrayList = new DbToArrayList(a);
        mRecyclerView = (RecyclerView) a.findViewById(R.id.home_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(a));
        HomeRecyclerViewAdapter mAdapter = new HomeRecyclerViewAdapter(dbToArrayList.getDataHome(), a);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void NOTE_RecyclerView(){
        DbToArrayList dbToArrayList=new DbToArrayList(a);
        mRecyclerView = (RecyclerView) a.findViewById(R.id.note_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(a));
        NoteRecyclerViewAdpter mAdapter = new NoteRecyclerViewAdpter(dbToArrayList.getDataNote(),a,a);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void REMINDER_RecyclerView(){
        DbToArrayList dbToArrayList=new DbToArrayList(a);
        mRecyclerView = (RecyclerView) a.findViewById(R.id.reminder_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(a));
        ReminderRecyclerViewAdapter mAdapter = new ReminderRecyclerViewAdapter(dbToArrayList.getDataReminder(),a,a);
        mRecyclerView.setAdapter(mAdapter);
    }
}
