package com.hsb.daneshjoo.cards;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.term.TermRecyclerViewAdapter;
import com.hsb.daneshjoo.data.DbToArrayList;

/**
 * Created by hsb on 7/14/2017.
 */

public class ReloadRecyclerView  {

    Activity a;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    public ReloadRecyclerView(Activity a) {
        this.a = a;
        mRecyclerView = (RecyclerView) a.findViewById(R.id.term_recycler_view);
    }

    public void term_RecyclerView(){
        DbToArrayList dbToArrayList=new DbToArrayList(a);

        mRecyclerView.setHasFixedSize(true);
        // mLayoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(a));
        mAdapter = new TermRecyclerViewAdapter(dbToArrayList.getDataTerm(),a,a);
        mRecyclerView.setAdapter(mAdapter);
    }
}
