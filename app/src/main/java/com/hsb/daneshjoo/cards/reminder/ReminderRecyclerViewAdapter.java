package com.hsb.daneshjoo.cards.reminder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.cards.lesson.*;
import com.hsb.daneshjoo.cards.lesson.LessonItem;
import com.hsb.daneshjoo.data.DbToArrayList;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsb-pc on 2/13/2017.
 */
public class ReminderRecyclerViewAdapter extends RecyclerView.Adapter<ReminderRecyclerViewAdapter.ReminderItemHolder> {

    private static ArrayList<Reminderitem> mDataset;
    Context context;
    Activity activity;
    public static class  ReminderItemHolder extends RecyclerView.ViewHolder {
        TextView tv_nameLesson,tv_clock,tv_date,tv_title;
        Button iv_edit,iv_view,iv_delete;
        public ReminderItemHolder(View itemView) {
            super(itemView);
            tv_nameLesson=(TextView) itemView.findViewById(R.id.tv_remindercardview_namelesson);
            tv_clock=(TextView) itemView.findViewById(R.id.tv_remindercardview_clock);
            tv_date=(TextView) itemView.findViewById(R.id.tv_remindercardview_date);
            tv_title=(TextView) itemView.findViewById(R.id.tv_remindercardview_title);

            iv_edit=(Button) itemView.findViewById(R.id.btn_remindercardview_edit);
            iv_view=(Button) itemView.findViewById(R.id.btn_remindercardview_view);
            iv_delete=(Button) itemView.findViewById(R.id.btn_remindercardview_delete);
        }
    }


    @Override
    public ReminderItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.reminder_cardview,parent,false);
        ReminderItemHolder lessonItemHoder=new ReminderItemHolder(view);
        return lessonItemHoder;
    }

    @Override
    public void onBindViewHolder(final ReminderItemHolder holder, int position) {

        holder.tv_clock.setText(String.valueOf(mDataset.get(position).getCLOCK()));
        holder.tv_date.setText(String.valueOf(mDataset.get(position).getDATE()));
        holder.tv_nameLesson.setText(mDataset.get(position).getNAME_LESSON());
        holder.tv_nameLesson.setTag(mDataset.get(position).getID_REMINDER());
        holder.tv_title.setText(mDataset.get(position).getTITLE());
        holder.tv_title.setTag(mDataset.get(position).getID());

        holder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReminderDialog reminderDialog=new ReminderDialog((Activity) context,(Integer) holder.tv_title.getTag(),"view-reminder");
                reminderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                reminderDialog.show();
            }
        });

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utility.setIdTerm((Integer) holder.tv_title.getTag());

                utility.setId(Integer.valueOf( holder.tv_nameLesson.getTag().toString()));
                utility.dialog(context.getResources().getString(R.string.NoteRecyclerViewAdapter1),context.getResources().getString(R.string.NoteRecyclerViewAdapter2), context.getResources().getString(R.string.NoteRecyclerViewAdapter3), context.getResources().getString(R.string.NoteRecyclerViewAdapter4), "delete-reminder", context,activity);

            }
        });

        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utility.setNameLesson((String) holder.tv_nameLesson.getText() );
                ReminderDialog reminderDialog=new ReminderDialog((Activity) context,(Integer) holder.tv_title.getTag(),"update-reminder");
                reminderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                reminderDialog.show();
            }
        });
    }

    public ReminderRecyclerViewAdapter(ArrayList<Reminderitem> myDataset,Context context,Activity activity) {

        mDataset = myDataset;
        this.context=context;
        this.activity=activity;
    }
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


}
