package com.hsb.daneshjoo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.hsb.daneshjoo.settings.KeySetting;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hsb-pc on 2/25/2017.
 */
public class TimeDialog implements RadialTimePickerDialogFragment.OnTimeSetListener ,TimePickerDialog.OnTimeSetListener {

    Activity activity;
    TextView et;
    KeySetting keySetting;

    public TimeDialog(){}
    public TimeDialog(Activity c,TextView editText){
        this.activity=c;
        keySetting=new KeySetting(c);
        this.et=editText;
    }


    public void show(){
        if (keySetting.getLanguage().equals("fa")) { // language is farsi , open datepicker persian
            RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                    .setOnTimeSetListener(TimeDialog.this)
                    .setStartTime(10, 10)
                    .setDoneText("تایید")
                    .setCancelText("انصراف")
                    .setForced24hFormat()
                    .setThemeLight();
            rtpd.show(((AppCompatActivity) activity).getSupportFragmentManager(), "hamed");
        }else {// language is english , open datepicker english


            Calendar now = Calendar.getInstance();
            TimePickerDialog dpd = TimePickerDialog.newInstance(
                    TimeDialog.this,
                    now.HOUR,
                    now.MINUTE,
                    true
            );

            dpd.show(activity.getFragmentManager(), "Timepickerdialog");
        }
    }
    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {


        et.setText(InitilaizeHours(hourOfDay)+":"+InitilaizeMinutes(minute));

        et.setTag(InitilaizeHours(hourOfDay)+InitilaizeMinutes(minute));
    }


    public int getTimeNow(){
        Date date=new Date();
        String clock=InitilaizeHours(date.getHours())+InitilaizeMinutes(date.getMinutes());

        return Integer.valueOf(clock);
    }


    public String InitilaizeHours(int hourOfDay){

        String h="";
        switch (hourOfDay){
            case 0:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 1:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 2:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 3:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 4:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 5:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 6:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 7:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 8:
                h="0"+String.valueOf(hourOfDay);
                break;
            case 9:
                h="0"+String.valueOf(hourOfDay);
                break;
            default:
                h=String.valueOf(hourOfDay);
        }

        return  h;
    }


    public String InitilaizeMinutes(int minute){
        String m="";
        switch (minute){
            case 0:
                m="0"+String.valueOf(minute);
                break;
            case 1:
                m="0"+String.valueOf(minute);
                break;
            case 2:
                m="0"+String.valueOf(minute);
                break;
            case 3:
                m="0"+String.valueOf(minute);
                break;
            case 4:
                m="0"+String.valueOf(minute);
                break;
            case 5:
                m="0"+String.valueOf(minute);
                break;
            case 6:
                m="0"+String.valueOf(minute);
                break;
            case 7:
                m="0"+String.valueOf(minute);
                break;
            case 8:
                m="0"+String.valueOf(minute);
                break;
            case 9:
                m="0"+String.valueOf(minute);
                break;
            default:
                m=String.valueOf(minute);
        }

        return  m;
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        et.setText(InitilaizeHours(hourOfDay)+":"+InitilaizeMinutes(minute));

        et.setTag(InitilaizeHours(hourOfDay)+InitilaizeMinutes(minute));
    }
}
