package com.hsb.daneshjoo.broadcastreceiver;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.hsb.daneshjoo.PersianDateDialog;
import com.hsb.daneshjoo.PersianDatePicker;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.cards.home.HomeItem;
import com.hsb.daneshjoo.date.CalendarTool;
import com.hsb.daneshjoo.date.DateTime;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.utility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hsb-pc on 2/27/2017.
 */
public class ChangeDateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

       /* Log.i("ChangeDateReceiver"," change date " );
        PendingIntent pi = PendingIntent.getActivity(context.getApplicationContext(), 0, new Intent(context.getApplicationContext(), MainActivity.class), 0);
        Resources r = context.getResources();
        Notification notification = new NotificationCompat.Builder(context.getApplicationContext())
                .setTicker("Come Hire")
                .setSmallIcon(android.R.drawable.ic_menu_report_image)
                .setContentTitle("WellCOme")
                .setContentText("Hamed safarzadeh")
                .setContentIntent(pi)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);*/

       // OnVibrat(context);
        VivrationPhoneLesson(context);

        KeySetting keySetting=new KeySetting(context);
        if (keySetting.getLanguage().equals("en")){
            getMessage(context,getEnDate());
        }else{
            getMessage(context,getDate());
        }

    }

    // get note reminder of table reminder
    private void getMessage(Context context,int date){


            Log.i("date   ",date+"");
            SqliteRepo sqliteRepo = new SqliteRepo(context.getApplicationContext());
            Cursor cur = sqliteRepo.Repo_Notereminder(date);
            int time = 5000;
            if (cur != null) { // cursor not null
                if (cur.getCount() > 0) { // cursor count row over 0
                    if (cur.moveToFirst()) { // mover cursor to first row
                        do {
                                if (cur.getInt(4)>0) {

                                    String hours = cur.getString(8).substring(0,2);
                                    String minuts = cur.getString(8).substring(2,4);

                                    int h=Integer.valueOf(hours) *60*60*1000;
                                    int m=Integer.valueOf(minuts) *60*1000;
                                    int s=20*1000;
                                    time=3000;

                                    DateTime dateTime=new DateTime();
                                    Log.i("date reminder",String.valueOf(hours));
                                    Log.i("date minuts",String.valueOf(minuts));
                                    Log.i("date HOURS app",String.valueOf((h/(60*60*1000)%24)));
                                    Log.i("date Time system",dateTime.Hours()+" : "+ dateTime.Minute());
                                    Log.i("date Time system",dateTime.Hours()+" : "+ dateTime.Minute());

                                    if (Integer.valueOf(dateTime.Hours())== Integer.valueOf(hours) && Integer.valueOf(dateTime.Minute())== Integer.valueOf(minuts)) {
                                        scheduleNotification(cur.getInt(3), context, getNotification(context, cur.getString(6), cur.getString(5)), time);
                                    }
                                }

                            //scheduleNotification(context,getNotification(context,"5 second delay"), 5000);
                        } while (cur.moveToNext()); // move to next row
                    }
                }
            }

    }


    private void scheduleNotification(int idReminder,Context context, Notification notification, int delay) {


        Intent notificationIntent = new Intent(context.getApplicationContext(), ShowNotification.class);
        notificationIntent.putExtra(ShowNotification.NOTIFICATION_ID, (int)System.currentTimeMillis());
        notificationIntent.putExtra(ShowNotification.NOTIFICATION, notification);
        notificationIntent.putExtra(ShowNotification.REMINDER_ID, idReminder);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), (int)System.currentTimeMillis(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = System.currentTimeMillis() + delay;
        AlarmManager alarmManager = (AlarmManager)  context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification getNotification(Context context,String content,String title) {
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder builder = new Notification.Builder(context.getApplicationContext());
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setSound(uri);
        builder.setSmallIcon(R.drawable.student_icon);
        builder.setDefaults(Notification.DEFAULT_SOUND);
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
        builder.setAutoCancel(true);
        builder.setLights(Color.BLUE, 500, 500);
        return builder.getNotification();
    }

    private int getDate(){
        Date date1=new Date();
        DateTime dateTime=new DateTime();


        CalendarTool calendarTool=new CalendarTool(date1.getYear(),date1.getMonth(),dateTime.Day());
        PersianDateDialog persianDateDialog=new PersianDateDialog();
        int IranianDate= Integer.valueOf( persianDateDialog.validation(calendarTool.getIranianYear(),calendarTool.getIranianMonth(),calendarTool.getIranianDay()));

        return IranianDate;
    }


    private int getEnDate(){
        Date date1=new Date();
        DateTime dateTime=new DateTime();


        CalendarTool calendarTool=new CalendarTool(date1.getYear(),date1.getMonth(),dateTime.Day());
        PersianDateDialog persianDateDialog=new PersianDateDialog();
        int IranianDate= Integer.valueOf( persianDateDialog.validation(calendarTool.getIranianYear(),calendarTool.getIranianMonth(),calendarTool.getIranianDay()));

        Integer year =dateTime.Yaer();
        Integer month =dateTime.Month();
        Integer day =dateTime.Day();


        String date=String.valueOf(persianDateDialog.validation(year,month,day));
        Log.i("En Date",date);
        return Integer.valueOf(persianDateDialog.validation(year,month,day));
    }


    public int getEnDay(){
        Calendar calendar=Calendar.getInstance();
        int day=calendar.get(Calendar.DAY_OF_WEEK);
        Log.i("En Day",String.valueOf(day));
        return day;
    }


    private int getFaDay(){
        Date date1=new Date();
        DateTime dateTime=new DateTime();
        CalendarTool calendarTool=new CalendarTool(date1.getYear(),date1.getMonth(),dateTime.Day());
        int  day =calendarTool.getIranianDay();

        return day;
    }
    private void VivrationPhoneLesson(Context context) {
        KeySetting keySetting=new KeySetting(context);
        if (keySetting.getClassPhoneVibration()) {
            ArrayList results = new ArrayList<HomeItem>();
            SqliteRepo repo = new SqliteRepo(context);
            Cursor cur = repo.Repo_lesson(1);

            int i = 0;
            if (cur != null) {
                if (cur.getCount() > 0) {
                    cur.moveToFirst();
                    do {


                        if (cur.getInt(7) == getEnDay()) { // equals day class
                            String hours = cur.getString(8).substring(0, 2);
                            String minuts = cur.getString(8).substring(2, 4);


                            DateTime dateTime = new DateTime();
                            if (Integer.valueOf(dateTime.Hours()) == Integer.valueOf(hours) && Integer.valueOf(dateTime.Minute()) == Integer.valueOf(minuts)) {// equal time
                                OnSilent(context);
                            }
                        }

                        /*if(cur.getInt(10) == getDate()){ // equals date exam

                        }*/
                    } while (cur.moveToNext());
                }
            }

        }
    }


    private void OnVibrat(Context context){
        Vibrator vibrator = (Vibrator)  context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(400);
    }
    private void OffVibrate(){

    }


    private void OnSilent(Context context){
        AudioManager audioManager=(AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
    }

    private void OffSilent(Context context){
        AudioManager audioManager=(AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
    }
}

