package com.hsb.daneshjoo.broadcastreceiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hsb.daneshjoo.db.SqliteUpdate;

/**
 * Created by hsb-pc on 3/2/2017.
 */
public class ShowNotification extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    public static String REMINDER_ID = "reminder_id";
    @Override
    public void onReceive(Context context, Intent intent) {

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        notificationManager.notify(intent.getIntExtra(NOTIFICATION_ID, 0), notification);

        SqliteUpdate sqliteUpdate=new SqliteUpdate(context.getApplicationContext());
        sqliteUpdate.Update_reminder(intent.getIntExtra(REMINDER_ID,0),0);
    }
}
