package com.hsb.daneshjoo.broadcastreceiver;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.activity.MainActivity;
import com.hsb.daneshjoo.db.SqliteDelete;
import com.hsb.daneshjoo.db.SqliteInsert;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.network.NetMessage;
import com.hsb.daneshjoo.network.NetStatus;
import com.hsb.daneshjoo.services.RestClient;
import com.hsb.daneshjoo.services.StoreClient;
import com.hsb.daneshjoo.services.webservice.request.NoteJson;
import com.hsb.daneshjoo.services.webservice.request.TokenJson;
import com.hsb.daneshjoo.services.webservice.response.AppVersionMessage;
import com.hsb.daneshjoo.services.webservice.response.Messageing;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.utilities.utility;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsb on 7/13/2017.
 */

public class ConnectInternetReceiver extends BroadcastReceiver {
    public ConnectInternetReceiver() {
        super();
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        getServiceCodeVersion(context);
    }

    private void getServiceCodeVersion(final Context a) {

        NetStatus netStatus = new NetStatus(a.getApplicationContext());
        if (netStatus.checkInternetConnection()) {
            // internet connection
            try {
                SqliteRepo repo =new SqliteRepo(a);
                TokenJson tokenJson = new TokenJson(repo.getIdUser());
                StoreClient client = RestClient.getClient().create(StoreClient.class);
                Call<AppVersionMessage> call = client.posttokenappversion(tokenJson);
                call.enqueue(new Callback<AppVersionMessage>() {
                    @Override
                    public void onResponse(Call<AppVersionMessage> call, Response<AppVersionMessage> response) {

                        Log.i("response app version", response.body().getCodeVersion().toString());
                        if (response.code() == 200) {
                            if (Integer.valueOf(response.body().getSuccessCode().toString()) == 0) {

                            } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 1) {

                                SqliteRepo repo = new SqliteRepo(a.getApplicationContext());
                                if (Integer.valueOf(response.body().getCodeVersion()) > repo.getCodeVersion()) {

                                    Log.i("database app version", String.valueOf(repo.getCodeVersion()));
                                    SqliteDelete sqliteDelete = new SqliteDelete(a.getApplicationContext());
                                    SqliteInsert sqliteInsert = new SqliteInsert(a.getApplicationContext());

                                    sqliteDelete.Del_appVersion();
                                    sqliteInsert.ADD_app_version(Integer.valueOf(response.body().getCodeVersion()), "2017");

                                    ShowNotification(a);
                                }
                            } else if (Integer.valueOf(response.body().getSuccessCode().toString()) == 202) {

                            }
                        } else {
                        }
                    }

                    @Override
                    public void onFailure(Call<AppVersionMessage> call, Throwable t) {
                        // Log error here since request failed
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // not internet connection

        }

    }

    private void scheduleNotification(int idReminder, Context context, Notification notification, int delay) {

        Intent notificationIntent = new Intent(context.getApplicationContext(), ShowNotification.class);
        notificationIntent.putExtra(ShowNotification.NOTIFICATION_ID, (int) System.currentTimeMillis());
        notificationIntent.putExtra(ShowNotification.NOTIFICATION, notification);
        notificationIntent.putExtra(ShowNotification.REMINDER_ID, idReminder);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), (int) System.currentTimeMillis(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = System.currentTimeMillis() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification getNotification(Context context, String content, String title) {

        Notification.Builder builder = new Notification.Builder(context.getApplicationContext());
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setSmallIcon(R.drawable.student_icon);
        builder.setDefaults(Notification.DEFAULT_SOUND);
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
        builder.setAutoCancel(true);
        builder.setLights(Color.BLUE, 500, 500);
        return builder.getNotification();
    }


    private void ShowNotification(Context c) {
        Uri uri;
        KeySetting keySetting = new KeySetting(c);

        if (keySetting.getVibration_Notification())
            uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        else
            uri = null;

        if (keySetting.getNew_Version()) {


            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(c.getApplicationContext())
                    .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                    .setSound(uri)
                    .setContentTitle(String.valueOf(R.string.Connect_Internet_Reciver_newVersion)) // title for notification
                    .setContentText(String.valueOf(R.string.Connect_Internet_Reciver_new)) // message for notification
                    .setAutoCancel(true) // clear notification after click
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

            Intent intent = new Intent(c.getApplicationContext(), MainActivity.class);
            PendingIntent pi = PendingIntent.getActivity(c.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pi);
            NotificationManager mNotificationManager =
                    (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(0, mBuilder.build());
        }
    }
}
