package com.hsb.daneshjoo.firebaseanalytics;

/**
 * Created by hsb on 7/30/2017.
 */

public class Book {

    private int bookId;
    private String bookTitle;

    public int getBookId() {
        return bookId;
    }
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
    public String getBookTitle() {
        return bookTitle;
    }
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }
}
