package com.hsb.daneshjoo.phone;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

/**
 * Created by hsb on 9/14/2017.
 */

public   class PhoneInfo {
     Context  c;


    public  PhoneInfo(Context c) {
        this.c = c;
    }


    public String Get_NamePhone(){
            return Build.DEVICE;
    }


    public String Get_ModelPhone(){
            return Build.MODEL;
    }


    public    String Get_VersionAndroid(){
        return Build.VERSION.SDK;
    }


    public String Get_Manufacturer(){
        return Build.MANUFACTURER;
    }


    public String Get_Domain(){
        TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
        return  tm.getNetworkCountryIso();
    }





}
