package com.hsb.daneshjoo.phone;

/**
 * Created by hsb on 9/14/2017.
 */

public class PhoneInfoModel {



    private String Id_Phone_User_Info;
    private String Version_Android;
    private String Name_Phone;
    private String Model_Phone;
    private String Manufacturer;
    private String Domain;


    public PhoneInfoModel( String id_Phone_User_Info, String version_Android, String name_Phone, String model_Phone, String manufacturer, String domain) {

        Id_Phone_User_Info = id_Phone_User_Info;
        Version_Android = version_Android;
        Name_Phone = name_Phone;
        Model_Phone = model_Phone;
        Manufacturer = manufacturer;
        Domain = domain;
    }



    public String getId_Phone_User_Info() {
        return Id_Phone_User_Info;
    }

    public void setId_Phone_User_Info(String id_Phone_User_Info) {
        Id_Phone_User_Info = id_Phone_User_Info;
    }

    public String getVersion_Android() {
        return Version_Android;
    }

    public void setVersion_Android(String version_Android) {
        Version_Android = version_Android;
    }

    public String getName_Phone() {
        return Name_Phone;
    }

    public void setName_Phone(String name_Phone) {
        Name_Phone = name_Phone;
    }

    public String getModel_Phone() {
        return Model_Phone;
    }

    public void setModel_Phone(String model_Phone) {
        Model_Phone = model_Phone;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        Manufacturer = manufacturer;
    }

    public String getDomain() {
        return Domain;
    }

    public void setDomain(String domain) {
        Domain = domain;
    }


    @Override
    public String toString() {
        return "PhoneInfoModel{" +
                "Id_Phone_User_Info='" + Id_Phone_User_Info + '\'' +
                ", Version_Android='" + Version_Android + '\'' +
                ", Name_Phone='" + Name_Phone + '\'' +
                ", Model_Phone='" + Model_Phone + '\'' +
                ", Manufacturer='" + Manufacturer + '\'' +
                ", Domain='" + Domain + '\'' +
                '}';
    }
}
