package com.hsb.daneshjoo.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.fragment.fragCreateStudent;
import com.hsb.daneshjoo.fragment.fragSignin;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.settings.LanguageLocal;
import com.hsb.daneshjoo.utilities.utility;

public class SignUpActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        KeySetting keySetting = new KeySetting(this.getApplicationContext());
        keySetting.setFirst_AppRun(true);



        //Bundle intent=getIntent().getExtras();
        String open = getIntent().getStringExtra("OPEN_FRAGMENT");
        if (open != null) {
            if (open.equals("CREATE_STUDENT")) {
                if (savedInstanceState == null) {
                    utility.setToken_createstudent("token_create");
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentParentViewGroup,new fragCreateStudent())
                            .commit();
                }
            }
        } else {

            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentParentViewGroup, new fragSignin())
                        .commit();
            }

        }

    }
}
