package com.hsb.daneshjoo.activity;

import android.animation.Animator;
import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.settings.LanguageLocal;

public class SplashActivity extends AppCompatActivity {
    KeySetting keySetting;
    TextView text_smartstudent, tv_version;
    private static final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        keySetting = new KeySetting(this.getApplicationContext());
        text_smartstudent = (TextView) findViewById(R.id.text_splsh);
        tv_version = (TextView) findViewById(R.id.tv_version);


        try {
            String versionName = getApplication().getPackageManager().getPackageInfo(getApplication().getPackageName(), 0).versionName;
            tv_version.setText(getResources().getString(R.string.App_Version) + " " + versionName);
        } catch (Exception ex) {

        }
        delay();
    }

    public void delay() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Initial();

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void Initial() {
        SqliteRepo sqliteRepo = new SqliteRepo(this.getApplicationContext());

        if (keySetting.getIntro_App()) {
            // Intro_App true
            InitLanguage();

            if (keySetting.getFirst_AppRun()) {
                // First_AppRun true
                if (sqliteRepo.getIdUser() > 0 && sqliteRepo.getIdUser() != 0) {

                    if (sqliteRepo.getIdStudent() > 0 && sqliteRepo.getIdStudent() != 0) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();

                        /*Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                        intent.putExtra("OPEN_FRAGMENT","CREATE_STUDENT");
                        startActivity(intent);
                        finish();*/
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                        intent.putExtra("OPEN_FRAGMENT", "CREATE_STUDENT");
                        startActivity(intent);
                        finish();
                    }


                } else {
                    // Create_Account false

                    Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else {

                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            // Intro_App false
            InitSetting();

            Intent intent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void InitSetting() {

        LanguageLocal languageLocal = new LanguageLocal(this.getApplicationContext());
        languageLocal.language("fa");

        keySetting.setLanguage("fa");

    }

    private void InitLanguage() {
        LanguageLocal languageLocal = new LanguageLocal(this.getApplicationContext());
        languageLocal.language(keySetting.getLanguage());
    }
}
