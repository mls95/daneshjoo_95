package com.hsb.daneshjoo.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.clickyab.Banner;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hsb.daneshjoo.app.Config;
import com.hsb.daneshjoo.broadcastreceiver.ChangeDateReceiver;
import com.hsb.daneshjoo.broadcastreceiver.ConnectInternetReceiver;
import com.hsb.daneshjoo.cards.setterm.SetTermDialog;
import com.hsb.daneshjoo.db.SqliteRepo;
import com.hsb.daneshjoo.drawer.HomeFragment;
import com.hsb.daneshjoo.R;
import com.hsb.daneshjoo.fragment.fragAboutMe;
import com.hsb.daneshjoo.fragment.fragNote;
import com.hsb.daneshjoo.fragment.fragReminder;
import com.hsb.daneshjoo.fragment.fragReportCard;
import com.hsb.daneshjoo.fragment.fragSetting;
import com.hsb.daneshjoo.fragment.fragTerm;
import com.hsb.daneshjoo.phone.PhoneInfo;
import com.hsb.daneshjoo.settings.KeySetting;
import com.hsb.daneshjoo.util.NotificationUtils;
import com.hsb.daneshjoo.utilities.utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements ActionBar.OnNavigationListener{

    private NavigationView navigationView;
    private DrawerLayout drawer;
    boolean doubleBackToExitPressedOnce = false;
    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    private String[] activityTitles ;
    public static int navItemIndex = 0;
    String title = "Smart Student";

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";

    public static String CURRENT_TAG = TAG_HOME;
    private View navHeader;

    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    android.support.v4.app.FragmentTransaction fragmentTransaction;

    private TextView tv_username;
    private TextView tv_version_app;

    private Toolbar mToolbar;

    ImageView im_setshowterm;
    Banner banner;
    // action bar
    private ActionBar actionBar;



    // KeySetting
    KeySetting keySetting;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        keySetting = new KeySetting(this);
       /* banner=(Banner) findViewById(R.id.bannerclick) ;
        banner.setClickYabAdListener(new ClickYabAdListener() {
            @Override
            public void onLoadFinished() {
                Log.i("click","onLoadFinished");


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restartActivity();

                    }
                }, 5000);
            }

            @Override
            public void onNoAds(int i, String s) {
                restartActivity();
            }

            @Override
            public void onClose() {
                Log.i("click","onClose");
            }
        });*/



        PhoneInfo phoneInfo = new PhoneInfo(getApplicationContext());
        if (Integer.valueOf(phoneInfo.Get_VersionAndroid()) >= 17) {
            if (keySetting.getLanguage().equals("fa"))
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            else
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }


        LocalBroadcastManager.getInstance(this).registerReceiver(getmRegistrationBroadcastReceiver, new IntentFilter("change_Language"));
        keySetting = new KeySetting(this);

        if (keySetting.getActiveSCVMain() == 0) {
            helpMainActivity();
            keySetting.setActiveSCVMain(1);
        }

        Initpage(savedInstanceState);

        InitPushNotification();

        IntentFilter intentFilter = new IntentFilter("android.intent.action.TIME_TICK");
        registerReceiver(new ChangeDateReceiver(), intentFilter);


        IntentFilter intentFilter1 = new IntentFilter("android.intent.action.SCREEN_ON");
        registerReceiver(new ConnectInternetReceiver(), intentFilter1);


        startService(new Intent(this, ChangeDateReceiver.class));

    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void Initpage( Bundle savedInstanceState) {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        activityTitles = getResources().getStringArray(R.array.nav_drawer_labels);
        navHeader = navigationView.getHeaderView(0);

        // load nav menu header data


        // initializing navigation menu
        setUpNavigationView(mToolbar);

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }



        im_setshowterm = (ImageView) findViewById(R.id.im_toolbar_setshowterm);
        im_setshowterm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show dialog set term
                SqliteRepo sqliteRepo = new SqliteRepo(MainActivity.this);
                int count = sqliteRepo.Repo_term().getCount();
                if (count > 0) {
                    SetTermDialog setTermDialog = new SetTermDialog(MainActivity.this);
                    setTermDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    setTermDialog.show();
                } else {
                    utility.dialog(getResources().getString(R.string.Message), getResources().getString(R.string.MainActivity_NoTermRegistered), MainActivity.this);
                }
            }
        });


        tv_username=(TextView) navHeader.findViewById(R.id.tv_username);
        tv_version_app=(TextView) navHeader.findViewById(R.id.tv_version_app);
        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            tv_version_app.setText(getResources().getString(R.string.Navigation_drawer) + " " + versionName);
        } catch (Exception ex) {

        }

        get_UserName();


    }


    private void get_UserName(){
        SqliteRepo sqliteRepo=new SqliteRepo(this);
        Cursor cur=sqliteRepo.Repo_account_UserName();
        if (cur.getCount()>0){
            cur.moveToFirst();
            do{
                tv_username.setText(cur.getString(0));
            }while (cur.moveToNext());
        }
    }
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        // Action to be taken after selecting a spinner item
        return false;
    }




    private void displayView(int position) {
        helpMainActivity();
        android.support.v4.app.Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.title_basic);
                keySetting.setActiveFragment(0);
                break;

            case 1:
                fragment = new fragTerm();
                title = getString(R.string.title_term);
                keySetting.setActiveFragment(1);
                break;

            case 2:
                fragment = new fragReportCard();
                title = getString(R.string.title_reportcard);
                keySetting.setActiveFragment(2);
                break;

            case 3:
                fragment = new fragReminder();
                title = getString(R.string.title_notification);
                keySetting.setActiveFragment(3);
                break;

            case 4:
                fragment = new fragNote();
                title = getString(R.string.title_note);
                keySetting.setActiveFragment(4);
                break;

            case 5:
                fragment = new fragSetting();
                title = getString(R.string.title_setting);
                keySetting.setActiveFragment(5);
                break;

            case 6:
                fragment = new fragAboutMe();
                title = getString(R.string.nav_item_aboutme);
                keySetting.setActiveFragment(6);
                break;

            case 7: {

                title = getString(R.string.nav_item_shareit);

                final String appPackageName = getPackageName();

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));

                }
            }
            break;

            case 8:
                fragment = new fragAboutMe();
                title = getString(R.string.nav_item_aboutme);
                break;
            default:
                break;
        }

        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);

            fragmentTransaction.commit();

            // set the toolbar title

            getSupportActionBar().setTitle(title);


        }

        keySetting.setOnStartFragment(0);
    }



    private void InitPushNotification() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);


                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    //String message = intent.getStringExtra("message");

                    // Toast.makeText(getApplicationContext(), "h Push notification: " + message, Toast.LENGTH_LONG).show();

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                }
            }
        };
    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }


    private void restartActivity() {
        keySetting.setOnStartFragment(5);

        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    private BroadcastReceiver getmRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            restartActivity();
        }
    };


    public void helpMainActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 5000);

        ViewTarget viewTarget = new ViewTarget(findViewById(R.id.im_toolbar_setshowterm));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);


        int margin = ((Number) (MainActivity.this.getResources().getDisplayMetrics().density * 40)).intValue();
        lps.setMargins(margin, 10, margin, margin);

        ShowcaseView sv = new ShowcaseView.Builder(MainActivity.this)
                //.setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setTarget(viewTarget)
                .setContentTitle(this.getResources().getString(R.string.MainActivity_ShowCase_title))
                .setContentText(this.getResources().getString(R.string.MainActivity_ShowCase_desc))
                .setStyle(R.style.BlueShowcaseTheme)
                .singleShot(42)
                .hideOnTouchOutside()
                .build();
        sv.setButtonText(this.getResources().getString(R.string.MainActivity_ShowCase_btn));
        sv.setButtonPosition(lps);
        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });

    }


    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.container_body, fragment, CURRENT_TAG);
                fragmentTransaction.addToBackStack(CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {

        switch (navItemIndex) {
            case 0:
                // home
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                // photos
                fragTerm fragTerm = new fragTerm();
                return fragTerm;
            case 2:
                // movies fragment
                fragReportCard fragReportCard = new fragReportCard();
                return fragReportCard;
            case 3:
                // notifications fragment
                fragReminder fragReminder = new fragReminder();
                return fragReminder;

            case 4:
                // settings fragment
                fragNote fragNote = new fragNote();
                return fragNote;
            case 5:
                // settings fragment

                fragSetting fragSetting = new fragSetting();
                return fragSetting;
            case 6:
                // settings fragment
                fragAboutMe fragAboutMe = new fragAboutMe();
                return fragAboutMe;
            case 7:
                // settings fragment
                fragAboutMe fragAboutMe1 = new fragAboutMe();
                return fragAboutMe1;
            default:
                return new HomeFragment();
        }
    }



    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView(Toolbar toolbar) {

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        title = getString(R.string.title_basic);
                        keySetting.setActiveFragment(0);
                        CURRENT_TAG = getString(R.string.title_basic);
                        break;
                    case R.id.nav_semester:
                        navItemIndex = 1;
                        title = getString(R.string.title_term);
                        keySetting.setActiveFragment(1);
                        CURRENT_TAG = getString(R.string.title_term);
                        break;
                    case R.id.nav_workbook:
                        navItemIndex = 2;
                        title = getString(R.string.title_reportcard);
                        keySetting.setActiveFragment(2);
                        CURRENT_TAG = getString(R.string.title_reportcard);
                        break;
                    case R.id.nav_reminder:
                        navItemIndex = 3;
                        title = getString(R.string.title_notification);
                        keySetting.setActiveFragment(3);
                        CURRENT_TAG = getString(R.string.title_notification);
                        break;
                    case R.id.nav_note:
                        navItemIndex = 4;
                        title = getString(R.string.title_note);
                        keySetting.setActiveFragment(4);
                        CURRENT_TAG = getString(R.string.title_note);
                        break;
                    case R.id.nav_setting:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                        Log.i("tag","nav_setting");
                        navItemIndex = 5;
                        title = getString(R.string.title_setting);
                        keySetting.setActiveFragment(5);
                        CURRENT_TAG = getString(R.string.title_setting);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_about_us:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
                        navItemIndex = 6;
                        title = getString(R.string.title_aboutme);
                        keySetting.setActiveFragment(6);
                        CURRENT_TAG = getString(R.string.title_aboutme);
                       // drawer.closeDrawers();
                        break;
                    case R.id.nav_share:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
                        navItemIndex = 7;

                        final String appPackageName = getPackageName();

                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));

                        }

                        drawer.closeDrawers();
                        return true;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up

            actionBarDrawerToggle.syncState();


    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }



        if (getFragmentManager().getBackStackEntryCount() > 0) {

            getFragmentManager().popBackStack();
        } else {

            if (!keySetting.getTopPageHomeFragmet()){
                super.onBackPressed();
            }else{
                if (doubleBackToExitPressedOnce) {
                    finish();
                    return;
                }
            }

        }




        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

    }


    // show or hide the fab
    private void toggleFab() {

    }

}
